import React from 'react';
import {StyleSheet, View, Text} from 'react-native';

export class RSSFeed extends React.Component{
  constructor(props) {
    super(props);

    this.state = {
      rss: []
    };
    this.setRss = this.setRss.bind(this);
  }
  setRss(json) {
    this.setState({
      rss: json.items
    });
  }
  render () {
    return (
      <View style={styles.container}>
        {this.state.rss.map(item =>
          <View key={item.guid}>
            <Text style={styles.textBold}>{item.title}</Text>
            <Text>{item.content}</Text>
          </View>
        )}
      </View>
    )
  }

  componentDidMount() {
    //Source https://gist.github.com/PhilipFlyvholm/d4171a16900cef6146b097a7ed432515
    const parseUrl = 'https://api.rss2json.com/v1/api.json?rss_url=';
    const rssUrl = 'https://www.dr.dk/nyheder/service/feeds/allenyheder/';
    fetch(parseUrl + rssUrl)
    .then(response => response.json())
    .then((json) => {
      if (json.status === 'ok') {
        this.setRss(json);
      }else {
        console.log("failed");
      }
    });
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10
  },
  textBold:{
    fontWeight: 'bold'
  }
});