import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, ImageBackground, ScrollView,AsyncStorage, TouchableHighlight,TouchableOpacity, Image , FlatList} from 'react-native';
import { createAppContainer, createDrawerNavigator, createSwitchNavigator, SafeAreaView, DrawerItems, createStackNavigator } from "react-navigation";
import Icon  from 'react-native-vector-icons/Ionicons';

import { BASE_URL } from './src/services/Api_constant';
import {TopTabNavigator,BottomTabNavigator} from './src/Navigators';
//Details Page
import Details from './src/Details.js';
import VideoCategoryDetails from './src/VideoCategoryDetails.js'; 
import followingPage from  './src/followingPage.js';
import searchPage from './src/searchPage.js';
import Splash from './src/Splash.js';

import Login from './src/Login.js';
import Register from './src/Register.js';
import Comment from './src/Comment.js';
import Test from './src/Test/Test.js';

import SimpleExample from './src/Test/SimpleExample';
import ScrollableTabsExample from './src/Test/ScrollableTabsExample';
import OverlayExample from './src/Test/OverlayExample';
import FacebookExample from './src/Test/FacebookExample';
import DynamicExample from './src/Test/DynamicExample';
import { Drawer } from 'native-base';


class HamburgerIcon extends Component {

  toggleDrawer = () => {
    this.props.navigationProps.toggleDrawer();
  }

  render() {
    return (
      <View style={{ flexDirection: 'row' }}>
        <TouchableOpacity style={{paddingLeft:15,paddingVertical:10}} onPress={this.toggleDrawer.bind(this)} >
        <Icon name={'ios-menu'} size={30} />
        </TouchableOpacity>
      </View>
    );
  }
}

class Logo extends Component {
  constructor(props){
    super(props);
  }
  static navigationOptions =
  {
   header: null,

  };

  toggleDrawers = () => {

  }

  searchPage = () => {
    this.props.navigationProps.navigate('searchPage');
  }

  /**   
   *  <Image
          source={require('./src/img/icon.jpg')}
          style={{ width: 140, height: 40 }}
        />
        <TextInput placeholder={'Search News, Videos & Mem...'} style={{width:'95%',padding:0,height:30,borderBottomWidth:1,borderBottomColor:'#bdc3c7'}}>
         </TextInput><Icon style={{paddingVertical:4,borderBottomWidth:1,borderBottomColor:'#bdc3c7'}}name={'ios-search'} size={20} />
   */

  render() {
    return (

      <View style={{ flex: 1,alignItems: "center",paddingRight:20}}>
        <TouchableOpacity onPress = {this.searchPage.bind(this)} style={{overflow:'hidden',paddingBottom:0,height:27,borderBottomWidth:1,borderBottomColor:'#bdc3c7'}}>
        <Text style={{paddingTop:5}}>Search News, Videos & Memes...<Icon name={'ios-search'} size={20} /></Text>
        </TouchableOpacity>
      </View>

    );

  }
}

class SearchBarExample extends Component {
  render() {
    return (
      <Container>
        <Header searchBar rounded>
          <Item>
            <Icon name="ios-search" />
            <Input placeholder="Search" />
            <Icon name="ios-people" />
          </Item>
          <Button transparent>
            <Text>Search</Text>
          </Button>
        </Header>
      </Container>
    );
  }
}

const CustomDrawerContentComponent = (props) => {
  let status = props.screenProps
  return(
    <SafeAreaView style={{ flex: 1}}>
    <View style={{ height: 150, backgroundColor: '#fff',borderBottomWidth:1,borderColor: '#ecf0f1',justifyContent:'center', alignItems: "center", }}>
        <Image
          source={require('./src/img/icon.jpg')}
          style={{ width: 140, height: 40 }}
        />
        <TouchableOpacity 
          onPress={()=>(status)? props.navigation.navigate('Home'):props.navigation.navigate('Login')}
          style={{borderWidth:1,borderRadius:3,marginTop:20,paddingVertical:10,paddingHorizontal:20,}}>
          <Text style={{textAlign:'center',fontSize:16}}>{(status)?' Sign Out':' Sign In '}</Text>
        </TouchableOpacity>

      </View>
      <ScrollView>
      <DrawerItems {...props} />
      </ScrollView>
    </SafeAreaView>
  )
}

class SearchIcon extends Component {

  grid = () => {
    global.layoutType = true;
  }

  list = () => {
    global.layoutType = false;
  }

  render() {
    return (
      <View style={{ flexDirection: 'row' }}>
        <TouchableOpacity style={{paddingRight:15,paddingVertical:10}} onPress={this.grid.bind(this)} >
         <Icon name={'md-grid'} size={25} />
        </TouchableOpacity>

        <TouchableOpacity style={{paddingRight:15,paddingVertical:10}} onPress={this.list.bind(this)} >
         <Icon name={'ios-list'} size={25} />
        </TouchableOpacity>
      </View>
    );
  }
}


class App extends Component{

  static navigationOptions =
  {
     drawerLabel: null,
     header : null,
  };
  constructor(props){
    super(props);
    this.state = {
      selectedLanguage:null,
      selectedLangID:null,
      languages:[]
    }
  }

  componentDidMount(){
    this._getLanguageList();
    this.getStorePreferredLang();

  }

  getStorePreferredLang = async () => {
    AsyncStorage.multiGet(['selectedLangID', 'selectedLanguage']).then((data) => {
        let selectedLangID = data[0][1];
        let selectedLanguage = data[1][1];
        
        if (selectedLangID !== null && selectedLanguage !== null){
          this.setState({selectedLanguage:selectedLanguage,selectedLangID:selectedLangID});
        }
    });
  }

  storePreferredLang = async () => {
    try {
      await AsyncStorage.multiSet([['selectedLangID', this.state.selectedLangID],['selectedLanguage', this.state.selectedLanguage]])
    } catch (e) {
      console.warn('Having problem to store preferred language')
    }
  }


  _getLanguageList = () => {
    
    this.setState({isFetching:false,isLoading:true})

    const url = BASE_URL + 'CommonFunc/getAllLanguage';
    fetch(url)
    .then((response) => response.json())
    .then((json) => {
      // alert(JSON.stringify(json.data))
      this.setState({languages: json.data,isFetching: false,isLoading: false});
    })
    .catch((error) => {
      // alert(JSON.stringify(error));
      console.warn(error)
      this.setState({isFetching: false,isLoading: false});
    });
  }

  selectLanguage = (lang,id)=>{
    this.setState({selectedLanguage:lang,selectedLangID:id},()=>this.storePreferredLang())
  }

  _done = () => {
    
    let stateDetail = this.state;
    // alert()
    if(stateDetail.selectedLanguage !== null && stateDetail.selectedLangID !==null){
      this.props.navigation.navigate('Home');
    }else{
      alert('Please Select Any One Language');
    }
  }

  _renderLanguage = (detail,index) => {   
    // alert(JSON.stringify(detail)) 
    let lang = detail.item.name;
    let id = detail.item.id;
    return(
      <View style={{paddingBottom:5}}>
        <TouchableOpacity
          activeOpacity={0.9} 
          onPress={()=>this.selectLanguage(lang,id)}
          style={{ borderWidth:1,borderColor:'teal',backgroundColor:'white',borderRadius:30,width:140,paddingVertical:15}}>
          {this.state.selectedLangID == id && (<Icon name="ios-checkmark" style={{position:'absolute',top:10,left:10}} size={30} color={'green'}/>)}
          <Text style={{textAlign:'center',fontSize:18}}>{lang}</Text>
        </TouchableOpacity>
      </View>
    )
  }
    
  render() {
    // alert(JSON.stringify(this.state.selectedLanguage))
    return (
      <View style={styles.container}>
        <ImageBackground source={require('./src/img/splash_bg.jpg')}  style={{resizeMode: 'cover',width: '100%', height: '100%'}} >

          <Text style={{fontSize: 20,fontWeight:'100',textAlign: 'center',fontFamily: 'Oswald-Regular', margin: 10,padding:10,color: 'black',}}>
    CHOOSE YOUR PREFERRED {'\n'}<Text style={{ fontSize: 23,fontWeight:'bold'}}>LANGUAGE</Text></Text>
        
        <FlatList
          data={this.state.languages}
          renderItem={this._renderLanguage}
          keyExtractor={item => item.id}
          numColumns={2}
          columnWrapperStyle={{justifyContent:'space-around'}}
        />

        <TouchableOpacity onPress = {()=>this._done() }
          style={{width:'40%',height: 40,alignSelf:'center',marginTop:20,marginBottom:20,backgroundColor:'green',color:"#27ae60" }}>
          <Text style={{fontSize: 17,color:'white',textAlign:"center",padding:10}}>SELECT</Text>
          </TouchableOpacity>
        </ImageBackground>   

      </View>
    );
  }
}


class Category extends Component {

  static navigationOptions =
    {
     header: null,
    };

    // Fetch the token from storage then navigate to our appropriate place
    gotoNextActivity = async () => {
    const userToken = await AsyncStorage.getItem('userToken');
    //this.props.navigation.navigate('StackViaTab');
    //this.props.navigation.navigate('Home_Screen');
    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    this.props.navigation.navigate(userToken ? 'MyDrawerNavigator' : 'Home_Screen');
   } 

  render() {

    return (
      <View style={styles.container}>
       <ImageBackground source={require('./src/img/splash_bg.jpg')}  style={{resizeMode: 'cover',width: '100%', height: '100%'}} >
          <Text style={{fontSize: 20,fontWeight:'100',textAlign: 'center',fontFamily: 'Oswald-Regular', marginTop: 30,padding:10,color: 'black',}}>
          SELECT <Text style={{ fontSize: 23,fontWeight:'bold'}}> CATEGORY</Text></Text>

          <View style={{flexDirection:'column',backgroundColor:'white',borderRadius: 10,marginHorizontal:15,marginVertical:0,paddingTop:20}}>
          
          <ScrollView>
            <View style={{flexDirection:'row',padding:10,alignItems:'center',justifyContent:'space-evenly'}}>
          
              <View style={{flexDirection:'column',alignItems:'center'}}>
              <TouchableOpacity onPress={this.gotoNextActivity} style={{}} >
                <Image source={require('./src/img/category/sport.png')} style={{height:60,width:60}} />
              </TouchableOpacity>
              <Text onPress={this.gotoNextActivity} style={{textAlign:'center',fontSize:15}}>Sports</Text>
              </View>

              <View style={{flexDirection:'column',alignItems:'center'}}>
              <TouchableOpacity onPress={this.gotoNextActivity} style={{}}>
                <Image source={require('./src/img/category/entertainment.png')}  style={{height:60,width:60}}/>
              </TouchableOpacity>
              <Text style={{textAlign:'center',fontSize:15}} onPress={this.gotoNextActivity}>Entertainment</Text>
              </View>

              <View style={{flexDirection:'column',alignItems:'center'}}>
              <TouchableOpacity onPress={this.gotoNextActivity} style={{}}>
                <Image source={require('./src/img/category/world.png')} style={{height:60,width:60}} />
              </TouchableOpacity>
              <Text style={{textAlign:'center',fontSize:15}} onPress={this.gotoNextActivity}>World</Text>
              </View>

           </View>

           <View style={{flexDirection:'row',padding:10,alignItems:'center',justifyContent:'space-evenly'}}>
          
              <View style={{flexDirection:'column',alignItems:'center'}}>
              <TouchableOpacity style={{}} onPress={this.gotoNextActivity} >
                <Image source={require('./src/img/category/business.png')} style={{height:60,width:60}} />
              </TouchableOpacity>
              <Text onPress={this.gotoNextActivity} style={{textAlign:'center',fontSize:15}}>Business</Text>
              </View>

              <View style={{flexDirection:'column',alignItems:'center'}}>
              <TouchableOpacity onPress={this.gotoNextActivity} style={{}}>
                <Image source={require('./src/img/category/technology.png')}  style={{height:60,width:60}}/>
              </TouchableOpacity>
              <Text style={{textAlign:'center',fontSize:15}} onPress={this.gotoNextActivity}>Technology</Text>
              </View>

              <View style={{flexDirection:'column',alignItems:'center'}}>
              <TouchableOpacity style={{}} onPress={this.gotoNextActivity}>
                <Image source={require('./src/img/category/cricket.png')} style={{height:60,width:60}} />
              </TouchableOpacity>
              <Text style={{textAlign:'center',fontSize:15}} onPress={this.gotoNextActivity}>Cricket</Text>
              </View>

           </View>

           <View style={{flexDirection:'row',padding:10,alignItems:'center',justifyContent:'space-evenly'}}>
          
              <View style={{flexDirection:'column',alignItems:'center'}}>
              <TouchableOpacity style={{}} onPress={this.gotoNextActivity}>
                <Image source={require('./src/img/category/astro.png')} style={{height:60,width:60}} />
              </TouchableOpacity>
              <Text style={{textAlign:'center',fontSize:15}} onPress={this.gotoNextActivity}>Astro</Text>
              </View>

              <View style={{flexDirection:'column',alignItems:'center'}}>
              <TouchableOpacity style={{}} onPress={this.gotoNextActivity}>
                <Image source={require('./src/img/category/science.png')}  style={{height:60,width:60}}/>
              </TouchableOpacity>
              <Text style={{textAlign:'center',fontSize:15}} onPress={this.gotoNextActivity}>Science</Text>
              </View>

              <View style={{flexDirection:'column',alignItems:'center'}}>
              <TouchableOpacity style={{}} onPress={this.gotoNextActivity}>
                <Image source={require('./src/img/category/education.png')} style={{height:60,width:60}} />
              </TouchableOpacity>
              <Text style={{textAlign:'center',fontSize:15}} onPress={this.gotoNextActivity}>Education</Text>
              </View>

           </View>

           <View style={{flexDirection:'row',padding:15,alignItems:'center',justifyContent:'space-evenly'}}>
          
              <View style={{flexDirection:'column',alignItems:'center'}}>
              <TouchableOpacity style={{}} onPress={this.gotoNextActivity}>
                <Image source={require('./src/img/category/health.png')} style={{height:60,width:60}} />
              </TouchableOpacity>
              <Text style={{textAlign:'center',fontSize:15}} onPress={this.gotoNextActivity}>Fitness</Text>
              </View>

              <View style={{flexDirection:'column',alignItems:'center'}}>
              <TouchableOpacity style={{}} onPress={this.gotoNextActivity}>
                <Image source={require('./src/img/category/trending.png')}  style={{height:60,width:60}}/>
              </TouchableOpacity>
              <Text style={{textAlign:'center',fontSize:15}} onPress={this.gotoNextActivity}>Trending</Text>
              </View>

              <View style={{flexDirection:'column',alignItems:'center'}}>
              <TouchableOpacity style={{}} onPress={this.gotoNextActivity}>
                <Image source={require('./src/img/category/travel.png')} style={{height:60,width:60}} />
              </TouchableOpacity>
              <Text style={{textAlign:'center',fontSize:15}} onPress={this.gotoNextActivity}>Travel</Text>
              </View>
           </View>
           </ScrollView>
         </View>
       </ImageBackground>   
      </View>
    );
  }
}


class BackButton extends React.Component {
  render() {
    return (
      <View>
        <Text>
          <Icon name={'md-arrow-down'} size={30} color={'black'}/>
       {/* 
        <Icon
         source={require('./spiro.png')}
         style={{ width: 30, height: 30 }}
         />
        */}
       </Text>
      </View>
    );
  }
}


const MyStackNavigator = createStackNavigator({
   Home_Screen : {
    screen: BottomTabNavigator,
    navigationOptions: ({ navigation }) => ({
      //header: <SearchBarExample navigationProps={navigation} />,
      headerTitle: <Logo navigationProps={navigation} />,
      //headerTitle: <SearchBarExample navigationProps={navigation} />,
      headerLeft:  <HamburgerIcon   navigationProps={navigation} />,
      headerRight: <SearchIcon   navigationProps={navigation} />,
      headerTitleStyle: {
      },
      headerStyle: {
        backgroundColor: '#fff',
        elevation: 0, // remove shadow on Android
        shadowOpacity: 0, // remove shadow on iOS
        borderBottomWidth: 1,
        borderBottomColor: '#bdc3c7'
      },
      headerTintColor: '#fff',
    })
  },
  Details : {
    screen: Details
  },
  followingPage: {
    screen: followingPage,
  },
  searchPage:{
    screen: searchPage
  },
  Simple :{screen:SimpleExample},
  Scrollable :{screen:ScrollableTabsExample},
  Overlay :{screen:OverlayExample},
  Facebook :{screen:FacebookExample},
  Dynamic :{screen:DynamicExample},
  Comment :{
    screen: Comment,
   
  },
  Test :{
    screen: Test,  
  },
  Login:{screen:Login}

});

const StartStack = createStackNavigator({ 
  Splash: {
    screen: Splash,
    navigationOptions: ({ navigation }) => ({
     drawerLockMode: 'locked-closed'
    })
  },
  Register: {
    screen: Register,
    navigationOptions: ({ navigation }) => ({
     drawerLockMode: 'locked-closed'
    })
  },
  Login: {
    screen: Login,
    navigationOptions: ({ navigation }) => ({
     drawerLockMode: 'locked-closed'
    })
  },
  App: {
  screen: App,
  navigationOptions: ({ navigation }) => ({
   drawerLockMode: 'locked-closed'
  })
  },
  Category: {
    screen: Category
  }
});

const TestStack = createStackNavigator({ 
VideoCategoryDetails : {
  screen: VideoCategoryDetails,
  navigationOptions: ({ navigation }) => ({
    drawerLockMode: 'locked-closed'
   })
}
});

const MyDrawerNavigator = createDrawerNavigator({
      Home : {
        screen: MyStackNavigator,
      },
      Videos : {
        screen: App,
      },
      Follow : {
        screen: App,
      },
      MyFavourites : {
        screen: App,
      },
      Language : {
        screen: App,
      },
      Advertising : {
        screen: App,
      },
      Settings : {
        screen: App,
      },
      Help : {
        screen: App,
      }
    },
    {
      contentComponent: CustomDrawerContentComponent,
      defaultNavigationOptions: ({ navigation }) => ({
       drawerIcon: ({ focused, horizontal, tintColor }) => {      
          const { routeName } = navigation.state;
          let IconComponent = Icon;
          let iconName;
          if  (routeName === 'Home') {
            return <IconComponent  name={'md-home'} size={25} color={'#2c3e50'} />;

          } else if (routeName === 'Videos') {
            return <IconComponent  name={'logo-youtube'} size={25} color={'#2c3e50'} />; 
    
          } else if (routeName === 'Follow') {
            return <IconComponent  name={'ios-star'} size={25} color={'#2c3e50'} />;
    
          } else if (routeName === 'Location') {
            return <IconComponent  name={'md-locate'} size={25} color={'#2c3e50'} />;
    
          } else if (routeName === 'PROFILE') {
            return <IconComponent  name={'ios-person'} size={25} color={'#f22613'} />;
    
          }
        },
      }),
       contentOptions: {
         activeTintColor: '#2c3e50',
         inactiveTintColor: '#2c3e50',
         itemsContainerStyle: {
           marginVertical: 0,
         },
         iconContainerStyle: {
           opacity: 1
         }
       }
});
const AppContainer = createAppContainer(createSwitchNavigator(
  {
    StartStack: StartStack,
    MyDrawerNavigator: MyDrawerNavigator,
    TestStack:TestStack,
    MyStackNavigator: MyStackNavigator,
  },
  {
    initialRouteName: 'StartStack',
  }
));

class MainView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loggedInUser:false
    };
  }

  componentWillMount(){
    this._getStoreUserDetail()
  }

  _getStoreUserDetail = async () => {
    try {
      const value = await AsyncStorage.getItem('user_id');
      if (value !== null) {
        this.setState({loggedInUser:true})
      }
    } catch (error) {
      alert('Some error occured while fetch login status')
    }
  }

  render() {
    return(
      <AppContainer screenProps={this.state.loggedInUser} />
    );
  }
}

export default MainView;
//export default createAppContainer(MyDrawerNavigator);

const styles = StyleSheet.create({

  MainContainer: {

    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#f5fcff',
    padding: 11

  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#4F6D7A',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    fontFamily: 'Oswald-Regular',
    margin: 10,
    padding:10,
    color: 'black',
  },

  instructions: {
    textAlign: 'center',
    color: '#F5FCFF',
    marginBottom: 5,
  },

  text:
  {
    fontSize: 22,
    color: '#000',
    textAlign: 'center',
    marginBottom: 10
  },
  langView:
  {
    flexDirection:'row',
    justifyContent:'space-evenly',
    alignItems:'center',
    paddingBottom:15
  },
  langButton:
  {
    borderWidth:1,
    backgroundColor:'white',
    borderRadius:30,
    paddingHorizontal:40,
    paddingVertical:10
  }

});