import React, { Component } from 'react';
import {
  View,
  Text,
  Dimensions,ViewPagerAndroid,
  ScrollView,FlatList,TouchableOpacity,StyleSheet,AsyncStorage
} from 'react-native';

import Ionicons  from 'react-native-vector-icons/Ionicons';



 const data = new Array(5).fill(0);
 let scrollXPos = 0;

 class Videos extends React.Component {
   constructor(props){
     super(props);
     this.state = {
       data:data,
       dragBeg:0,
       selected: (new Map(): Map<string, boolean>),
       toggle: true,
       fselected: null,
       screenHeight: Dimensions.get('window').height,      
       screenWidth: Dimensions.get('window').width,
       languages: [
        {"id":1,  "Name" : "Hindi"     ,"Status": "Unchecked", "color": "#1abc9c", "initial":"हि" , "title" : "हिंदी"},       
        {"id":2,  "Name" : "English"   ,"Status": "Unchecked", "color": "#9b59b6", "initial":"E" ,  "title" : "English"},       
        {"id":3,  "Name" : "Tamil"     ,"Status": "Unchecked", "color": "#3498db", "initial":"த" , "title" : "தமிழ்"},       
        {"id":4,  "Name" : "Marathi"   ,"Status": "Unchecked", "color": "#18dcff", "initial":"म" , "title" : "मराठी"},       
        {"id":5,  "Name" : "Malayalam" ,"Status": "Unchecked", "color": "#b71540", "initial":"മ" , "title" : "മലയാളം"},       
        {"id":6,  "Name" : "Kannada"   ,"Status": "Unchecked", "color": "#e58e26", "initial":"ಕ" , "title" : "ಕನ್ನಡ್"},       
        {"id":7,  "Name" : "Telugu"    ,"Status": "Unchecked", "color": "#1e3799", "initial":"తె" , "title" : "తెలుగు"},       
        {"id":8,  "Name" : "Punjabi"   ,"Status": "Unchecked", "color": "#e74c3c", "initial":"ਪੰ" , "title" : "ਪੰਜਾਬੀ"},       
        {"id":9,  "Name" : "Gujarati"  ,"Status": "Unchecked", "color": "#78e08f", "initial":"ગુ" , "title" : "ગુજરાતી"},       
        {"id":10, "Name" : "Urdu"      ,"Status": "Unchecked", "color": "#34495e", "initial":"ار" , "title" : "اردو"},       
        {"id":11, "Name" : "Nepali"    ,"Status": "Unchecked", "color": "#a55eea", "initial":"ने" ,  "title" : "नेपाली"},       
        {"id":12, "Name" : "Bhojpuri"  ,"Status": "Unchecked", "color": "#fa983a", "initial":"भो" , "title" : "भोजपुरी"},       
        {"id":13, "Name" : "Odisha"    ,"Status": "Unchecked", "color": "#e67e22", "initial":"ଓ" , "title" : "ଓଡ଼ିଆ"},       
        {"id":14, "Name" : "Bangla"    ,"Status": "Unchecked", "color": "#2ecc71", "initial":"বা" , "title" : "বাংলা"},       
       ],
       newlanguages: [],
      
     }
   }

   componentDidMount(){
    this.db();
   }

   db(){
           
     const test=[];

     
     this.state.languages.map((item)=>{
       const Lid = ""+item.id+"";

       AsyncStorage.getItem(Lid, (err, result) => {
         // if the user is login first time them it will load from default database Status which is stored in this.state.
          const Status = result ? result : item.Status;
          test.push({
           "id"     : item.id,
           "uid"    : Lid,
           "Name"   : item.name,
           "Status" : Status,
           "color"  : item.color, 
           "initial": item.initial, 
           "title"  : item.title       
          })
          this.setState({
            newlanguages: test
          })


          console.warn(test);
       });
       
      })
        
      //Testing



   }


   renderChecklistItem = ({item}) => (
    <CheckListItem
      id={item.id}
      uid={item.uid}
      onPressItem={this.onPressItem}
      selected={!!this.state.selected.get(item.id)}
      Name={item.Name}
      Status={item.Status}
      title={item.title}
      color={item.color}
      initial={item.initial}

    />
  );
   keyExtractor = (item, index) => item.id;
 
   onPressItem = (id: string) => {
       // updater functions are preferred for transactional updates

       
     this.setState((state) => {
       // copy the map rather than modifying state.
       const selected = new Map(state.selected);
       

       selected.set(id, !selected.get(id)); // toggle

       const checkStatus = !selected.get(id) === true ? "Checked" : "Unchecked";
       const checkId = ""+id+"";

       console.warn("status"+this.state.newlanguages[0]['Status']);

       

       AsyncStorage.setItem(checkId,checkStatus);
       console.warn("main "+id+!selected.get(id));
       console.warn("test "+!!this.state.selected.get(id));
       console.warn("status"+id+checkStatus);

       return {selected};
      });
   };


  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <FlatList
            data={this.state.newlanguages}
            numColumns={2}
            extraData={this.state}
            renderItem={this.renderChecklistItem}
            keyExtractor={this.keyExtractor}
            />
          </View>
    );
  }
}

class CheckListItem extends React.PureComponent {
  constructor(props){
      super(props);

      this.state = {
         pStatus: this.props.Status === "Checked" ? "Checked" : "Unchecked",
         test:[],
         FTest: " ",
         press: true,
         newtest: []

      }
    }

    componentDidMount(){
      //this.newtest();
    }


    // newtest = () => {
    //   const test = [];

    //   //Use React Native Debugger by jhen0409/react-native-debugger
    //   AsyncStorage.getAllKeys((err, name) => {
    //     AsyncStorage.multiGet(name, (error, stores) => {
    //       stores.map((result, i, store) => {

    //         //test.push({ [store[i][0]]: store[i][1] });

    //         return true;
    //        // console.warn('hekko'+test);

    //         // this.setState({
    //         //   newtest: test,
    //         //   isLoading: false
    //         // })
    //       });
    //     });
    //   });
    //   console.warn(this.state.newtest);

    // }



  // checkList = () => {

  //   this.setState({
  //     press: !this.state.press,      
  //     pStatus: this.state.Status === "Checked" ? "Checked" : "Unchecked",
  //   })

  //   console.warn("MAIN SELECTED"+this.props.selected);
  //   //console.warn("Press: "+this.state.press);
  //   //pStatus: !this.state.pStatus

     

  //   //  AsyncStorage.getItem('UID123', (err, result) => {
  //   //    const test = JSON.parse(result);
  //   //     console.warn(test.languages);
  //   //   });

  
  //   this.props.onPressItem(this.props.id);
  //   //const toggle = this.props.selected ? false : true;
  //   //const checkStatus = this.props.selected || this.props.Status  == 'Unchecked' ? 'Unchecked' : 'Checked';

  //   //console.warn("child "+this.props.id+this.props.selected+this.props.uid+this.props.Status);

  //   //const checkStatus = this.props.selected || this.props.Status == 'Checked' ? 'Checked' : 'Unchecked';
  //   //AsyncStorage.setItem(this.props.Name,checkStatus);

  //   //console.warn(AsyncStorage.setItem(this.props.Name,checkStatus));

  //   const checkId = ""+this.props.id+"";

  //   AsyncStorage.getItem(checkId, (err, result) => {
  //     this.setState({
  //       FTest: result
  //     })
  //    //console.warn("Single One "+checkId+result);
  //    //Based on this does not matters the values.
  //   });
    



 

  //     const name = this.props.id;
  //      //Use React Native Debugger by jhen0409/react-native-debugger
  //      AsyncStorage.getAllKeys((err, name) => {
  //        AsyncStorage.multiGet(name, (error, stores) => {
  //          stores.map((result, i, store) => {
  //           const a ={[store[i][0]] : store[i][1]};
  //           const test = [];
  //           test.push(a);
            
  //           //console.warn(test);
  //           this.setState({
  //             newtest: test
  //           })
  //           return true;
  //          });
  //        });
  //       });

  //       console.warn(this.state.newtest);



        
  //     // //Use React Native Debugger by jhen0409/react-native-debugger
  //     // AsyncStorage.getAllKeys((err, keys) => {
  //     //   AsyncStorage.multiGet(keys, (error, stores) => {
  //     //     stores.map((result, i, store) => {
  //     //       console.warn({ [store[i][0]]: store[i][1] });
  //     //       return true;
  //     //     });
  //     //   });
  //     // });

  // };

  // rLang = () =>{
  //   let IconComponent = Ionicons;

  //   return <TouchableOpacity onPress={this.checkList} activeOpacity={1} style={{flexDirection:'row',alignItems:'baseline',justifyContent:'space-between',borderWidth:1,borderColor:this.props.color,backgroundColor: this.props.selected || this.props.Status == 'true'?this.props.color:'white',borderRadius:30,width:140,height:45,paddingTop:3}} >
  //       {this.props.selected == 'true' && this.props.Status == 'false'?<View><Text style={{borderRadius:30,width:30,height:30,marginVertical:3,marginLeft:5,backgroundColor:this.props.color,paddingHorizontal:6,color:'white',fontSize:21}}>{this.props.initial}</Text></View> : this.props.selected || this.props.Status == 'true' ?null :<View><Text style={{borderRadius:30,width:30,height:30,marginVertical:3,marginLeft:5,backgroundColor:this.props.color,paddingHorizontal:6,color:'white',fontSize:21}}>{this.props.initial}</Text></View> }
  //       <Text style={{fontSize:18,paddingVertical:3,paddingHorizontal:15,color:this.props.selected || this.props.Status == 'true'?'white':'black'}}>{this.props.title}</Text>
  //       {this.props.selected == 'true' && this.props.Status == 'false'? null  : this.props.selected || this.props.Status == 'true' ? <IconComponent style={{borderRadius:30,width:30,height:30,marginVertical:3,marginRight:5,backgroundColor:'white',paddingVertical:4,paddingHorizontal:8,color:'grey',fontSize:21}} name={"md-checkmark"} />:null  }
  //     </TouchableOpacity>
  // }

  render() {
    let IconComponent = Ionicons;
    //let Condition =  this.props.Status === "Checked" && this.props.selected === "true" ? this.props.color : 'white' ;
    //let Condition =  this.props.Status === "Checked" && this.props.selected === "false" ? this.props.color : 'white' ;
    //let Condition =  this.props.Status === "Unchecked" && this.props.selected === "true" ? this.props.color : 'white' ;
    //let Condition =  this.props.Status === "Unchecked" && this.props.selected === "false" ? this.props.color : 'white' ;

    return (
      <View style={{padding:10}}>
      <TouchableOpacity onPress={this.checkList} activeOpacity={1} style={{flexDirection:'row',alignItems:'baseline',justifyContent:'space-between',borderWidth:1,borderColor:this.props.color,
       backgroundColor: this.props.selected === true || this.props.Status == "Checked" ? this.props.color : 'white' ,borderRadius:30,width:140,height:45,paddingTop:3}} >
       

       </TouchableOpacity>
      </View>
    );
  }
}



const styles = StyleSheet.create({
  selected: {
    color:'red',
  },
  notselected: {
  },
  btselected: {
    paddingVertical:20,
    width:130,
    borderBottomWidth:2,
    borderBottomColor:'red'
  },
  btnotselected: {
    paddingVertical:20,
    width:130,
    borderBottomWidth:2,
    borderBottomColor:'white'
  },
  iconStyle:{
    borderRadius:30,
    width:30,
    height:30,
    marginVertical:3,
    marginRight:5,
    backgroundColor:'white',
    paddingVertical:4,
    paddingHorizontal:8,
    color:'grey',
    fontSize:21
  }

})

export default Videos;       



/**
 *   
 *  backgroundColor: (this.state.pStatus === "Checked" && (this.props.selected === true || this.props.selected === false)) ||
       (this.state.pStatus === "Unchecked" && this.props.selected === true) ? this.props.color : 'white' ,borderRadius:30,width:140,height:45,paddingTop:3}} >
       
 * 
 * 
 * 
 * 
 *   Latest 1
 *    <TouchableOpacity onPress={this.checkList} activeOpacity={1} style={{flexDirection:'row',alignItems:'baseline',justifyContent:'space-between',borderWidth:1,borderColor:this.props.color,backgroundColor: this.props.Status === "Checked" ? this.props.color : this.props.selected ?this.props.color:'white',borderRadius:30,width:140,height:45,paddingTop:3}} >
       
        {this.props.Status === "Checked" && this.state.press === true ? null : this.props.selected ? null :<View><Text style={{borderRadius:30,width:30,height:30,marginVertical:3,marginLeft:5,backgroundColor:this.props.color,paddingHorizontal:6,color:'white',fontSize:21}}>{this.props.initial}</Text></View> }
        <Text style={{fontSize:18,paddingVertical:3,paddingHorizontal:15,color: this.props.Status === "Checked" && this.state.press === true ? 'white' : this.props.selected ?'white':'black'}}>{this.props.title}</Text>
        { this.props.Status === "Checked" && this.state.press === true ? <IconComponent style={styles.iconStyle} name={"md-checkmark"} /> : this.props.selected ?<IconComponent style={styles.iconStyle} name={"md-checkmark"} />:null}

      </TouchableOpacity>

     Latest 2 
        {this.props.selected ? this.props.Status === "Unchecked" && this.props.selected === true? null :<View><Text style={{borderRadius:30,width:30,height:30,marginVertical:3,marginLeft:5,backgroundColor:this.props.color,paddingHorizontal:6,color:'white',fontSize:21}}>{this.props.initial}</Text></View>:null }
        <Text style={{fontSize:18,paddingVertical:3,paddingHorizontal:15,color: this.props.selected ? this.props.Status === "Unchecked" && this.props.selected === true?'white':'black':'white'}}>{this.props.title}</Text>
        {this.props.selected ? this.props.Status === "Unchecked" && this.props.selected === true?<IconComponent style={styles.iconStyle} name={"md-checkmark"} />:null:<IconComponent style={styles.iconStyle} name={"md-checkmark"} />}
 * 
 * 
 * 
 * 
 * 
 */

   /* Version 1
     <TouchableOpacity onPress={this.checkList} activeOpacity={1} style={{flexDirection:'row',alignItems:'baseline',justifyContent:'space-between',borderWidth:1,borderColor:this.props.color,backgroundColor: this.props.selected || this.props.Status == 'true'?this.props.color:'white',borderRadius:30,width:140,height:45,paddingTop:3}} >
        {this.props.selected == 'true' && this.props.Status == 'false'?<View><Text style={{borderRadius:30,width:30,height:30,marginVertical:3,marginLeft:5,backgroundColor:this.props.color,paddingHorizontal:6,color:'white',fontSize:21}}>{this.props.initial}</Text></View> : this.props.selected || this.props.Status == 'true' ?null :<View><Text style={{borderRadius:30,width:30,height:30,marginVertical:3,marginLeft:5,backgroundColor:this.props.color,paddingHorizontal:6,color:'white',fontSize:21}}>{this.props.initial}</Text></View> }
        <Text style={{fontSize:18,paddingVertical:3,paddingHorizontal:15,color:this.props.selected || this.props.Status == 'true'?'white':'black'}}>{this.props.title}</Text>
        
        {this.props.selected == 'true' && this.props.Status == 'false'? 
          null  
          :  this.props.selected == 'false' && this.props.Status == 'true' ? <IconComponent style={styles.iconStyle} name={"md-checkmark"} />
{this.props.selected ? this.props.Status === "Unchecked" && this.props.selected === true? null :<View><Text style={{borderRadius:30,width:30,height:30,marginVertical:3,marginLeft:5,backgroundColor:this.props.color,paddingHorizontal:6,color:'white',fontSize:21}}>{this.props.initial}</Text></View>:null }
        <Text style={{fontSize:18,paddingVertical:3,paddingHorizontal:15,color: this.props.selected ? this.props.Status === "Unchecked" && this.props.selected === true?'white':'black':'white'}}>{this.props.title}</Text>
        {this.props.selected ? this.props.Status === "Unchecked" && this.props.selected === true?<IconComponent style={styles.iconStyle} name={"md-checkmark"} />:null:<IconComponent style={styles.iconStyle} name={"md-checkmark"} />}
 
          :  this.props.selected == 'false' && this.props.Status == 'false' ? null

          :  this.props.selected == 'true'  && this.props.Status == 'true' ? <IconComponent style={styles.iconStyle} name={"md-checkmark"} />:null
        }
     </TouchableOpacity> 
     */

     /** Version 2
      * <TouchableOpacity onPress={this.checkList} activeOpacity={1} style={{flexDirection:'row',alignItems:'baseline',justifyContent:'space-between',borderWidth:1,borderColor:this.props.color,backgroundColor: this.props.selected || this.props.Status == 'true'?this.props.color:'white',borderRadius:30,width:140,height:45,paddingTop:3}} >
        {this.props.selected == 'true' && this.props.Status == 'false'?<View><Text style={{borderRadius:30,width:30,height:30,marginVertical:3,marginLeft:5,backgroundColor:this.props.color,paddingHorizontal:6,color:'white',fontSize:21}}>{this.props.initial}</Text></View> : this.props.selected || this.props.Status == 'true' ?null :<View><Text style={{borderRadius:30,width:30,height:30,marginVertical:3,marginLeft:5,backgroundColor:this.props.color,paddingHorizontal:6,color:'white',fontSize:21}}>{this.props.initial}</Text></View> }
        <Text style={{fontSize:18,paddingVertical:3,paddingHorizontal:15,color:this.props.selected || this.props.Status == 'true'?'white':'black'}}>{this.props.title}</Text>
        
        { this.props.selected ?
           this.props.Status == true && this.props.selected ?  <IconComponent style={styles.iconStyle} name={"md-checkmark"}/> :null 
           :this.props.selected ?  <IconComponent style={styles.iconStyle} name={"md-checkmark"} />:null}

       </TouchableOpacity>
*/



/* 
            <View style={{flexDirection:'row',justifyContent:'space-evenly'}}>

              <TouchableOpacity onPress={this.check.bind(this,this.state.languages)} activeOpacity={1} style={{flexDirection:'row',alignItems:'baseline',justifyContent:'space-between',borderWidth:1,borderColor:'#1abc9c',backgroundColor:this.state.toggle?'white':'#1abc9c',borderRadius:30,width:140,height:45,paddingTop:3}} >
                {this.state.toggle?<View ><Text style={{borderRadius:30,width:30,height:30,marginVertical:3,marginLeft:5,backgroundColor:'#1abc9c',paddingHorizontal:6,color:'white',fontSize:21}}>हि</Text></View>:null}
                <Text style={{fontSize:18,paddingVertical:3,paddingHorizontal:15,color:this.state.toggle?'black':'white'}}>हिंदी</Text>
                {this.state.toggle?null:<Icon style={{borderRadius:30,width:30,height:30,marginVertical:3,marginRight:5,backgroundColor:'white',paddingVertical:4,paddingHorizontal:8,color:'grey',fontSize:21}} name={"checkmark"} />}
              </TouchableOpacity>

              <TouchableOpacity onPress={this.check.bind(this,this.state.languages)} activeOpacity={1} style={{flexDirection:'row',alignItems:'baseline',justifyContent:'space-between',borderWidth:1,borderColor:'#e74c3c',backgroundColor:this.state.toggle?'white':'#e74c3c',borderRadius:30,width:140,height:45,paddingTop:3}} >
                {this.state.toggle?<View ><Text style={{borderRadius:30,width:30,height:30,marginVertical:3,marginLeft:5,backgroundColor:'#e74c3c',paddingHorizontal:6,color:'white',fontSize:23}}>తె</Text></View>:null}
                <Text style={{fontSize:18,paddingVertical:3,paddingHorizontal:15,color:this.state.toggle?'black':'white'}}>తెలుగు</Text>
                {this.state.toggle?null:<Icon style={{borderRadius:30,width:30,height:30,marginVertical:3,marginRight:5,backgroundColor:'white',paddingVertical:4,paddingHorizontal:8,color:'grey',fontSize:21}} name={"checkmark"}  />}
              </TouchableOpacity>
             </View> 

              Flatlist
               <View style={{padding:10}}>
                <TouchableOpacity onPress={this.check.bind(this,item)} activeOpacity={1} style={{flexDirection:'row',alignItems:'baseline',justifyContent:'space-between',borderWidth:1,borderColor:item.color,backgroundColor:this.state.toggle?'white':item.color,borderRadius:30,width:140,height:45,paddingTop:3}} >
                  {this.state.toggle?<View ><Text style={{borderRadius:30,width:30,height:30,marginVertical:3,marginLeft:5,backgroundColor:item.color,paddingHorizontal:6,color:'white',fontSize:21}}>{item.initial}</Text></View>:null}
                  <Text style={{fontSize:18,paddingVertical:3,paddingHorizontal:15,color:this.state.toggle?'black':'white'}}>{item.title}</Text>
                  {this.state.toggle?null:<Icon style={{borderRadius:30,width:30,height:30,marginVertical:3,marginRight:5,backgroundColor:'white',paddingVertical:4,paddingHorizontal:8,color:'grey',fontSize:21}} name={"checkmark"} />}
                </TouchableOpacity>
               </View>
*/



    //Testing
    // let UID123_object = {
    //   languages: [
    //     {"id":1,  "Name" : "Hindi"     , "Checked" : false, "color": "#1abc9c", "initial":"हि" , "title" : "हिंदी"},       
    //     {"id":2,  "Name" : "English"   , "Checked" : false, "color": "#9b59b6", "initial":"E" ,  "title" : "English"},       
    //     {"id":3,  "Name" : "Tamil"     , "Checked" : false, "color": "#3498db", "initial":"த" , "title" : "தமிழ்"},       
    //     {"id":4,  "Name" : "Marathi"   , "Checked" : false, "color": "#18dcff", "initial":"म" , "title" : "मराठी"},       
    //     {"id":5,  "Name" : "Malayalam" , "Checked" : false, "color": "#b71540", "initial":"മ" , "title" : "മലയാളം"},       
    //     {"id":6,  "Name" : "Kannada"   , "Checked" : false, "color": "#e58e26", "initial":"ಕ" , "title" : "ಕನ್ನಡ್"},       
    //     {"id":7,  "Name" : "Telugu"    , "Checked" : false, "color": "#1e3799", "initial":"తె" , "title" : "తెలుగు"},       
    //     {"id":8,  "Name" : "Punjabi"   , "Checked" : false, "color": "#e74c3c", "initial":"ਪੰ" , "title" : "ਪੰਜਾਬੀ"},       
    //     {"id":9,  "Name" : "Gujarati"  , "Checked" : false, "color": "#78e08f", "initial":"ગુ" , "title" : "ગુજરાતી"},       
    //     {"id":10, "Name" : "Urdu"      , "Checked" : false, "color": "#34495e", "initial":"ار" , "title" : "اردو"},       
    //     {"id":11, "Name" : "Nepali"    , "Checked" : false, "color": "#a55eea", "initial":"ने" ,  "title" : "नेपाली"},       
    //     {"id":12, "Name" : "Bhojpuri"  , "Checked" : false, "color": "#fa983a", "initial":"भो" , "title" : "भोजपुरी"},       
    //     {"id":13, "Name" : "Odisha"    , "Checked" : false, "color": "#e67e22", "initial":"ଓ" , "title" : "ଓଡ଼ିଆ"},       
    //     {"id":14, "Name" : "Bangla"    , "Checked" : false, "color": "#2ecc71", "initial":"বা" , "title" : "বাংলা"},       
    //    ]
    // };
 
/**
 * 
 * languages: [
        {"id":1,  "Name" : "Hindi"     ,"Status": "Unchecked", "color": "#1abc9c", "initial":"हि" , "title" : "हिंदी"},       
        {"id":2,  "Name" : "English"   ,"Status": "Unchecked", "color": "#9b59b6", "initial":"E" ,  "title" : "English"},       
        {"id":3,  "Name" : "Tamil"     ,"Status": "Unchecked", "color": "#3498db", "initial":"த" , "title" : "தமிழ்"},       
        {"id":4,  "Name" : "Marathi"   ,"Status": "Unchecked", "color": "#18dcff", "initial":"म" , "title" : "मराठी"},       
        {"id":5,  "Name" : "Malayalam" ,"Status": "Unchecked", "color": "#b71540", "initial":"മ" , "title" : "മലയാളം"},       
        {"id":6,  "Name" : "Kannada"   ,"Status": "Unchecked", "color": "#e58e26", "initial":"ಕ" , "title" : "ಕನ್ನಡ್"},       
        {"id":7,  "Name" : "Telugu"    ,"Status": "Unchecked", "color": "#1e3799", "initial":"తె" , "title" : "తెలుగు"},       
        {"id":8,  "Name" : "Punjabi"   ,"Status": "Unchecked", "color": "#e74c3c", "initial":"ਪੰ" , "title" : "ਪੰਜਾਬੀ"},       
        {"id":9,  "Name" : "Gujarati"  ,"Status": "Unchecked", "color": "#78e08f", "initial":"ગુ" , "title" : "ગુજરાતી"},       
        {"id":10, "Name" : "Urdu"      ,"Status": "Unchecked", "color": "#34495e", "initial":"ار" , "title" : "اردو"},       
        {"id":11, "Name" : "Nepali"    ,"Status": "Unchecked", "color": "#a55eea", "initial":"ने" ,  "title" : "नेपाली"},       
        {"id":12, "Name" : "Bhojpuri"  ,"Status": "Unchecked", "color": "#fa983a", "initial":"भो" , "title" : "भोजपुरी"},       
        {"id":13, "Name" : "Odisha"    ,"Status": "Unchecked", "color": "#e67e22", "initial":"ଓ" , "title" : "ଓଡ଼ିଆ"},       
        {"id":14, "Name" : "Bangla"    ,"Status": "Unchecked", "color": "#2ecc71", "initial":"বা" , "title" : "বাংলা"},       
       ],
 */

    //  let UID123_delta = {
    //   languages: [
    //     {"id":1,  "Name" : "Hindi"     , "Checked" : false, "color": "#1abc9c", "initial":"हि" , "title" : "हिंदी"},       
    //     {"id":2,  "Name" : "English"   , "Checked" : false, "color": "#9b59b6", "initial":"E" ,  "title" : "English"},       
    //     {"id":3,  "Name" : "Tamil"     , "Checked" : false, "color": "#3498db", "initial":"த" , "title" : "தமிழ்"},       
    //     {"id":4,  "Name" : "Marathi"   , "Checked" : false, "color": "#18dcff", "initial":"म" , "title" : "मराठी"},       
    //     {"id":5,  "Name" : "Malayalam" , "Checked" : false, "color": "#b71540", "initial":"മ" , "title" : "മലയാളം"},       
    //     {"id":6,  "Name" : "Kannada"   , "Checked" : false, "color": "#e58e26", "initial":"ಕ" , "title" : "ಕನ್ನಡ್"},       
    //     {"id":7,  "Name" : "Telugu"    , "Checked" : false, "color": "#1e3799", "initial":"తె" , "title" : "తెలుగు"},       
    //     {"id":8,  "Name" : "Punjabi"   , "Checked" : false, "color": "#e74c3c", "initial":"ਪੰ" , "title" : "ਪੰਜਾਬੀ"},       
    //     {"id":9,  "Name" : "Gujarati"  , "Checked" : false, "color": "#78e08f", "initial":"ગુ" , "title" : "ગુજરાતી"},       
    //     {"id":10, "Name" : "Urdu"      , "Checked" : false, "color": "#34495e", "initial":"ار" , "title" : "اردو"},       
    //     {"id":11, "Name" : "Nepali"    , "Checked" : false, "color": "#a55eea", "initial":"ने" ,  "title" : "नेपाली"},       
    //     {"id":12, "Name" : "Bhojpuri"  , "Checked" : false, "color": "#fa983a", "initial":"भो" , "title" : "भोजपुरी"},       
    //     {"id":13, "Name" : "Odisha"    , "Checked" : false, "color": "#e67e22", "initial":"ଓ" , "title" : "ଓଡ଼ିଆ"},       
    //     {"id":14, "Name" : "Bangla"    , "Checked" : false, "color": "#2ecc71", "initial":"বা" , "title" : "বাংলা"},       
    //    ]
    //  };
    
    
      // AsyncStorage.setItem('UID123', JSON.stringify(UID123_object), () => {
      //   AsyncStorage.mergeItem('UID123', JSON.stringify(UID123_delta), () => {
      //     AsyncStorage.getItem('UID123', (err, result) => {
      //       console.warn(result);
      //     });
      //   });
      //  });





    //  AsyncStorage.getItem('UID123', (err, result) => {
    //   const test = JSON.parse(result);
    //   this.setState({
    //     test: test.languages
    //   })

    // });
     // console.warn(this.state.test.languages);
