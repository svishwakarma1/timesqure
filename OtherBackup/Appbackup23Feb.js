import React, { Component } from 'react';

import { StyleSheet, Text, View, Button,TextInput, ImageBackground, TouchableOpacity, Image } from 'react-native';

import { createAppContainer, createMaterialTopTabNavigator, createDrawerNavigator, createStackNavigator } from "react-navigation";

import { createIconSet } from 'react-native-vector-icons';

import {TopTabNavigator,BottomTabNavigator} from './src/Navigators';


class HamburgerIcon extends Component {

  toggleDrawer = () => {

    this.props.navigationProps.toggleDrawer();

  }

  render() {

    return (

      <View style={{ flexDirection: 'row' }}>

        <TouchableOpacity onPress={this.toggleDrawer.bind(this)} >

          <Image
            source={{ uri: 'https://reactnativecode.com/wp-content/uploads/2018/04/hamburger_icon.png' }}
            style={{ width: 25, height: 25, marginLeft: 5 }}
          />

        </TouchableOpacity>

      </View>

    );


  }
}


class App extends Component<Props> {

  static navigationOptions =
  {
     title: 'App',
     header : null
  };

  gotoNextActivity = () =>
  {
     this.props.navigation.navigate('Second');
     
  }

  render() {
    return (
      <View style={styles.container}>
        <ImageBackground source={require('./src/img/choose_city.jpg')}  style={{resizeMode: 'cover',width: '100%', height: '100%'}} >

        <Text style={styles.welcome}>CHOOSE COUNTRY</Text>
        <TextInput
          style={{height: 40,borderWidth:1,borderColor:'grey',borderRadius:5,backgroundColor:'white',marginLeft:20,marginRight:20}}
          placeholder=""
          onChangeText={(text) => this.setState({text})}
        />
        <Text style={styles.welcome}>CHOOSE CITY</Text>
        <TextInput
          style={{height: 40,borderWidth:1,borderColor:'grey',borderRadius:5,backgroundColor:'white',marginLeft:20,marginRight:20}}
          placeholder=""
          onChangeText={(text) => this.setState({text})}
        />
        <Text style={styles.welcome}>CHOOSE LANGUAGE</Text>
        <TextInput
          style={{height: 40,borderWidth:1,borderColor:'grey',borderRadius:5,backgroundColor:'white',marginLeft:20,marginRight:20,marginBottom:40 }}
          placeholder=""
          onChangeText={(text) => this.setState({text})}
        />

        <TouchableOpacity onPress = { this.gotoNextActivity }
         style={{width:'40%',height: 40,alignSelf:'center',backgroundColor:'green',color:"#27ae60" }}>
        <Text style={{fontSize: 17,color:'white',textAlign:"center",padding:10}}>NEXT</Text>
        </TouchableOpacity>


        </ImageBackground>   
      </View>
    );
  }
}

class Home_Screen extends Component {

  static navigationOptions =
    {
      title: 'Home',

    };

  gotoNextActivity = () => {
    this.props.navigation.navigate('Second');

  }

  render() {

    return (

      <View style={styles.container}>
      <Text>Home_Screen</Text>
      </View>
    );
  }
}

class Settings_Screen extends Component {

  static navigationOptions =
    {
      title: 'Settings',
    };

  render() {

    return (

      <View style={styles.MainContainer}>

        <Text style={styles.text}>This is Settings Screen Activity.</Text>

      </View>
    );
  }
}

class Student_Screen extends Component {

  static navigationOptions =
    {
      title: 'Student',

    };

  gotoNextActivity = () => {
    this.props.navigation.navigate('Forth');

  }

  render() {

    return (

      <View style={styles.MainContainer}>

        <Text style={styles.text}>This is Student Screen Activity.</Text>

        <Button onPress={this.gotoNextActivity} title='Open Details Activity' />

      </View>
    );
  }
}

class Details_Screen extends Component {

  static navigationOptions =
    {
      title: 'Details Screen',

    };

  gotoNextActivity = () => {
    this.props.navigation.navigate('Second');

  }

  render() {

    return (

      <View style={styles.MainContainer}>

        <Text style={styles.text}>This is Details Screen Activity.</Text>

      </View>
    );
  }
}

export const Tab_1 = createMaterialTopTabNavigator({
  First: {
    screen: Home_Screen,
  },
  Second: {
    screen: Settings_Screen,
  }
}, {
    tabBarPosition: 'top',

    swipeEnabled: true,

    tabBarOptions: {

      activeTintColor: '#fff',
      pressColor: '#004D40',
      inactiveTintColor: '#fff',
      style: {

        backgroundColor: '#00B8D4'

      },

      labelStyle: {
        fontSize: 16,
        fontWeight: '200'
      }
    }

  });

export const Tab_2 = createMaterialTopTabNavigator({
  Third: {
    screen: Student_Screen,
  },
  Forth: {
    screen: Details_Screen,
  }
}, {
    tabBarPosition: 'top',

    swipeEnabled: true,

    tabBarOptions: {

      activeTintColor: '#fff',
      pressColor: '#004D40',
      inactiveTintColor: '#fff',
      style: {

        backgroundColor: '#00B8D4'

      },

      labelStyle: {
        fontSize: 16,
        fontWeight: '200'
      }
    }

  });

const First_2_Tabs = createStackNavigator({
  First: {
    screen: Tab_1,
    navigationOptions: ({ navigation }) => ({
      title: 'First Screen',
      headerLeft: <HamburgerIcon navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#00B8D4',
        elevation: 0, // remove shadow on Android
        shadowOpacity: 0, // remove shadow on iOS
      },
      headerTintColor: '#fff',
    })
  },
});

const Second_2_Tabs = createStackNavigator({
  First: {
    screen: Tab_2,
    navigationOptions: ({ navigation }) => ({
      title: 'Second Screen',
      headerLeft: <HamburgerIcon navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#00B8D4',
        elevation: 0, // remove shadow on Android
        shadowOpacity: 0, // remove shadow on iOS
      },
      headerTintColor: '#fff',
    })
  },
});

const MyDrawerNavigator = createDrawerNavigator({
  App: {
    screen: App,
  },

  Home_Menu_Label: {

    screen: First_2_Tabs,

  },

  Student_Menu_Label: {

    screen: Second_2_Tabs,

  },
  BottomTab : {
    screen: BottomTabNavigator,
  }

});

export default createAppContainer(MyDrawerNavigator);

const styles = StyleSheet.create({

  MainContainer: {

    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#f5fcff',
    padding: 11

  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#4F6D7A',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    fontFamily: 'Oswald-Regular',
    margin: 10,
    padding:10,
    color: 'black',
  },
  instructions: {
    textAlign: 'center',
    color: '#F5FCFF',
    marginBottom: 5,
  },

  text:
  {
    fontSize: 22,
    color: '#000',
    textAlign: 'center',
    marginBottom: 10
  },

});