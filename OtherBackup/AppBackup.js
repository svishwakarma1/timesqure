/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, {Component} from 'react';
import { FlatList, Platform, ActivityIndicator, StyleSheet, Text, View  } from 'react-native';
import * as rssParser from 'react-native-rss-parser';
import Icon  from 'react-native-vector-icons/Ionicons';
import { createMaterialTopTabNavigator , createBottomTabNavigator, createDrawerNavigator, createAppContainer } from 'react-navigation';




type Props = {};

 
// return fetch('http://www.nasa.gov/rss/dyn/breaking_news.rss')
//   .then((response) => response.text())
//   .then((responseData) => rssParser.parse(responseData))
//   .then((rss) => {
//     console.log(rss.title);
//     console.log(rss.items.length);
//   });


class App extends Component<Props> {

  constructor(props){
    super(props);
    this.state ={ isLoading: true}
  }

  componentDidMount(){
    return fetch('http://www.nasa.gov/rss/dyn/breaking_news.rss')
      .then((response) => response.text())
      .then((responseData) => rssParser.parse(responseData))
      .then((rss) => {
        console.warn(rss);
        console.warn(rss.title);
        console.warn(rss.items.length);

        this.setState({
          isLoading: false,
          dataSource: rss,
        });

      })
      .catch((error) =>{
        console.error(error);
      });
  }

  
  render() {
    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }
    return(
      <View style={{flex: 1, paddingTop:20}}>
       <Text>Hello and Welcome</Text>
      </View>
    );
  }

}

class TVShow extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>TVShow!</Text>
      </View>
    );
  }
}

class Videos extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Videos!</Text>
      </View>
    );
  }
}

class Photos extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Icon name="ios-person" size={30} color="#4F8EF7" />
        <Text>Photos!</Text>
      </View>
    );
  }
}

class Profile extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Profile !</Text>
      </View>
    );
  }
}


const MyDrawerNavigator = createDrawerNavigator({
  VIDEOS: {
    screen: Videos
  },
  TVSHOW: {
   screen : TVShow,
  },
  HOME: {
   screen : App,
  },
  PHOTOS: {
   screen : Photos,
  },
  PROFILE: {
   screen : Profile,
  } 
});

const TopTabNavigator = createMaterialTopTabNavigator(
  {
    Videos: Videos,
    TVSHOW: TVShow,
    HOME: App,
    PHOTOS: Photos,
    PROFILE: Profile,
  },
  
);


const BottomTabNavigator = createBottomTabNavigator(
{
  VIDEOS: Videos,
  TVSHOW: TVShow,
  HOME: App,
  PHOTOS: Photos,
  PROFILE: Profile,
},
{
  defaultNavigationOptions: ({ navigation }) => ({
    tabBarIcon: ({ focused, horizontal, tintColor }) => {
      const { routeName } = navigation.state;
      let IconComponent = Icon;
      let iconName;
      if (routeName === 'VIDEOS') {
        return <IconComponent  name={'logo-youtube'} size={25} color={focused ? '#f22613' :'#fff'} />;
        //iconName = `ios-information-circle${focused ? '' : '-outline'}`;
        // Sometimes we want to add badges to some icons. 
        // You can check the implementation below.
        //IconComponent = HomeIconWithBadge; 
      } else if (routeName === 'TVSHOW') {
        return <IconComponent  name={'ios-tv'} size={25} color={focused ? '#f22613' :'#fff'} />;

      } else if (routeName === 'HOME') {
        return <IconComponent  name={'ios-home'} size={25} color={focused ? '#f22613' :'#fff'} />;

      } else if (routeName === 'PHOTOS') {
        return <IconComponent  name={'ios-camera'} size={25} color={focused ? '#f22613' :'#fff'} />;

      } else if (routeName === 'PROFILE') {
        return <IconComponent  name={'ios-person'} size={25} color={focused ? '#f22613' :'#fff'} />;

      }


    },
  }),
  tabBarOptions: {
    activeTintColor: '#f22613',
    inactiveTintColor: 'white',
    style: {
      backgroundColor: 'black'
    }
  },

}
);

export default createAppContainer(TopTabNavigator);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
