import React, { Component } from 'react';
import {
  View,
  Text,Alert,AsyncStorage,Image,StyleSheet,ImageBackground,Button,TextInput,TouchableOpacity,KeyboardAvoidingView,
} from 'react-native';

import { BASE_URL } from './services/Api_constant';

 export default class Register extends React.Component {
  static navigationOptions =
  {
     drawerLabel: null,
     header : null,
  };
  
  constructor(props){
     super(props);
     //const userToken = AsyncStorage.getItem('userToken');
     //this.gotoNextActivity();
     this.state = {
       name: '',
       mobile: '',
       email: '',
       password: '',
       confirmpassword: '',
       errors: '',
       success: 'success',
       error: 'error'
     }
     //this.props.navigation.navigate('Home');
   }

   SignUp = () =>{
     // validation pending
     console.warn(this.state.name+" "+this.state.mobile+" "+this.state.email+" "+this.state.password);
 
 
    // const {name}  = this.state ;
    // const {mobile} = this.state ;
    // const {email} = this.state ;
    // const {password} = this.state ;
    // const {confirmpassword} = this.state ;

    //if( this.state.password === this.state.confirmpassword )
    //{   

      if (this.state.name === undefined &&  this.state.mobile === undefined && this.state.email === undefined && this.state.password  === undefined && this.state.confirmpassword === undefined)  {
      
        this.setState({
          errors: 'Required'
        });
  
      } else if (this.state.email.trim() === '' && this.state.mobile.trim() === '' && this.state.password.trim() === '' && this.state.confirmpassword.trim() === '') {
        this.setState({
          errors: 'Must not be blank'
        });
        
      } 
      else if (this.state.mobile.length !== 10 && this.state.mobile ) {
        this.setState({
          errors: 'Invalid Mobile Number'
        });
        
      } 
      else if (this.state.password !== this.state.confirmpassword) {
        this.setState({
          errors: 'Password and Confirm Password does not Match'
        });
        
      } 
      else if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.state.email) && (this.state.password === this.state.confirmpassword) && /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(this.state.mobile)) {

        fetch(BASE_URL+'Rss/do_register', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({

    
            name: this.state.name,

            mobile: this.state.mobile,
    
            email: this.state.email,
    
            password: this.state.password

          
            })
          
        }).then((response) => response.json())
        .then((responseJson) => {
        
          // Showing response message coming from server after inserting records.

          this.setState({
            success: responseJson,
            errors : responseJson.message
          })
          if ( responseJson.status === 1){
            this.props.navigation.navigate('App');
           }
          
        console.warn(this.state.success);
        //this.props.navigation.navigate('Login');
        
          }).catch((error) => {
          this.setState({
            error: error
            
          })
            console.warn(this.state.error);
          });


      } else {
        this.setState({
          errors: 'Invalid Email Address or Password'
        });

      }

    
     }

   
  // Fetch the token from storage then navigate to our appropriate place
 // gotoNextActivity = () =>
 // {

    //console.warn(userToken);s

     //this.props.navigation.navigate('StackViaTab');
     //this.props.navigation.navigate('Category');

    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
  // this.props.navigation.navigate('Login');
  //}

  // componentWillMount(){
  //   setTimeout(()=> {
  //     this.props.navigation.navigate('App')
  //   },1000);
  // }
  render() {
    return (
      <View style={styles.container}>
      <ImageBackground style={styles.bg}  source={require('./img/splash_bg.jpg')}>
      <Image source={require('./img/logo.png')}/>
      
      <KeyboardAvoidingView behavior="padding " enabled>

      <Text style={styles.title}>SIGN <Text style={{color:"red"}}>UP</Text></Text>      
      <Text style={{color:"red"}}>{this.state.errors}</Text>     
      
      <View style={styles.w100container}>
       <View style={styles.inputStyle}><Image style={styles.w10} source={require('./img/signup/user.png')}/><TextInput style={styles.inputText}   onChangeText={name => this.setState({name})} placeholder={'Full Name'} /></View>
       <View style={styles.inputStyle}><Image style={styles.w10} source={require('./img/signup/mobile.png')}/><TextInput style={styles.inputText} onChangeText={mobile => this.setState({mobile})} placeholder={'Mobile No'} /></View>
       <View style={styles.inputStyle}><Image style={styles.w10} source={require('./img/signup/mail.png')}/><TextInput style={styles.inputText}   onChangeText={email => this.setState({email})} placeholder={'Email'} /></View>
       <View style={styles.inputStyle}><Image style={styles.w10} source={require('./img/signup/key.png')}/><TextInput style={styles.inputText}    onChangeText={password => this.setState({password})} secureTextEntry={true} placeholder={'Password'} /></View>
       <View style={styles.inputStyle}><Image style={styles.w10} source={require('./img/signup/mobile.png')}/><TextInput style={styles.inputText} onChangeText={confirmpassword => this.setState({confirmpassword})} secureTextEntry={true} placeholder={'Confirm Password'} /></View>
       <TouchableOpacity onPress={this.SignUp} style={styles.submitbtn} >
          <Text style={styles.submit}>SIGN UP</Text>
       </TouchableOpacity>
       </View>

       </KeyboardAvoidingView>
      
      </ImageBackground>
      </View>
    );
  }
}


const styles = StyleSheet.create({
 container: {flex: 1,flexDirection:'column',justifyContent:'center',alignItems:'center'},
 bg: {height:'100%',width:'100%',position:'absolute',resizeMode:'contain',justifyContent:'center',alignItems:'center',},
 title: {fontFamily: 'Oswald-Regular',fontSize:24,color:'black',textAlign:'center',marginVertical:10},
 w100container: {width:'100%',justifyContent:'center',alignItems:'center'},
 inputStyle: {flexDirection:'row',alignItems:'center',borderWidth:1,borderRadius:5,borderColor:'#7f8c8d',backgroundColor:'white',marginVertical:8,paddingLeft:5},
 inputText: {height:50,width: "80%",fontSize:18},
 w10: {width:"10%"},
 submitbtn: {width:'90%',flexDirection:'row',borderRadius:5,marginTop:15,paddingVertical:10,backgroundColor:'red'},
 submit: {width: "100%",fontSize:19,color:'white',textAlign:'center'},

});


// import React, { Component } from 'react';
// import {
//   View,
//   Text,AsyncStorage,Image,ImageBackground,Button,TextInput,TouchableOpacity,KeyboardAvoidingView,
// } from 'react-native';

//  export default class Register extends React.Component {
//   static navigationOptions =
//   {
//      drawerLabel: null,
//      header : null,
//   };
  
//   constructor(props){
//      super(props);
//      //const userToken = AsyncStorage.getItem('userToken');
//      //this.gotoNextActivity();
//    }

   

   
//   // Fetch the token from storage then navigate to our appropriate place
//   gotoNextActivity = () =>
//   {

//     //console.warn(userToken);s

//      //this.props.navigation.navigate('StackViaTab');
//      //this.props.navigation.navigate('Category');

//     // This will switch to the App screen or Auth screen and this loading
//     // screen will be unmounted and thrown away.
//     this.props.navigation.navigate('Login');
//   }

//   // componentWillMount(){
//   //   setTimeout(()=> {
//   //     this.props.navigation.navigate('App')
//   //   },1000);
//   // }
//   render() {
//     return (
//       <View style={{ flex: 1,}}>
//       <ImageBackground style={{flex:1,paddingLeft:10,paddingRight:10,}} source={require('./img/splash_bg.jpg')}>
//       <Image style={{width:'80%',alignSelf:'center',marginTop:"25%"}} source={require('./img/logo.png')}/>
//       <Text style={{fontFamily: 'Oswald-Regular',marginTop:20,fontSize:24,color:'black',textAlign:'center',marginVertical:10}}>SIGN <Text style={{color:"red"}}>UP</Text></Text>
//       <KeyboardAvoidingView style={{ flex: 1,}} behavior="padding" enabled>
      
//        <View style={{justifyContent:'center',alignItems:'center'}}>
//        <View style={{flexDirection:'row',alignItems:'center',borderWidth:1,borderRadius:5,borderColor:'#7f8c8d',backgroundColor:'white',marginVertical:8,paddingLeft:5}}><Image source={require('./img/signup/user.png')}/><TextInput style={{width: "80%",fontSize:18}} placeholder={'Full Name'} /></View>
//        <View style={{flexDirection:'row',alignItems:'center',borderWidth:1,borderRadius:5,borderColor:'#7f8c8d',backgroundColor:'white',marginVertical:8,paddingLeft:5}}><Image source={require('./img/signup/mobile.png')}/><TextInput style={{width: "80%",fontSize:18}} placeholder={'Mobile No'} /></View>
//        <View style={{flexDirection:'row',alignItems:'center',borderWidth:1,borderRadius:5,borderColor:'#7f8c8d',backgroundColor:'white',marginVertical:8,paddingLeft:5}}><Image source={require('./img/signup/mail.png')}/><TextInput style={{width: "80%",fontSize:18}} placeholder={'Email'} /></View>
//        <View style={{flexDirection:'row',alignItems:'center',borderWidth:1,borderRadius:5,borderColor:'#7f8c8d',backgroundColor:'white',marginVertical:8,paddingLeft:5}}><Image source={require('./img/signup/key.png')}/><TextInput style={{width: "80%",fontSize:18}} secureTextEntry={true} placeholder={'Password'} /></View>
//        <View style={{flexDirection:'row',alignItems:'center',borderWidth:1,borderRadius:5,borderColor:'#7f8c8d',backgroundColor:'white',marginVertical:8,paddingLeft:5}}><Image source={require('./img/signup/password.png')}/><TextInput style={{width: "80%",fontSize:18}} secureTextEntry={true} placeholder={'Confirm Password'} /></View>
//        </View>
//        <TouchableOpacity onPress={this.gotoNextActivity} style={{width: "90%",borderRadius:5,alignSelf:'center',marginTop:15,paddingVertical:10,backgroundColor:'red'}} >
//          <Text style={{fontSize:19,color:'white',textAlign:'center'}}>SUBMIT</Text>
//        </TouchableOpacity>
//        </KeyboardAvoidingView>

//        </ImageBackground>
//       </View>
//     );
//   }
// }


