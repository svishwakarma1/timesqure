import React, { Component } from 'react';
import {
  View,Text,Image,AsyncStorage,
  ActivityIndicator,Dimensions,BackHandler,
  StyleSheet,FlatList,WebView,TextInput,Modal,KeyboardAvoidingView,ToastAndroid,Share,Linking,Platform,ScrollView,TouchableHighlight,TouchableOpacity,SafeAreaView
} from 'react-native';
import _ from "lodash";
import {Container,Footer,Header,Left,Right,Button, Content, Body} from "native-base";
import Icon  from 'react-native-vector-icons/Ionicons';
// import YouTube from 'react-native-youtube';

import { BASE_URL } from './services/Api_constant';

const YoutubeApiKey = "AIzaSyCwPyeDgIp4jIeHecewfPDBW5NBdBoVRW4";

const baseUrl= 'http://limrademo.com/timesqure/';
const test=[{"channel_name":"Times of India"},{"channel_name":"Zee News"}];
const testWorkable=[{"45":"Times of India","51":"Zee News"}];



//item.channel_name == test
var {height, width} = Dimensions.get('window');
// a component that calls the imperative ToastAndroid API
const Toast = (props) => {
  if (props.visible) {
    ToastAndroid.showWithGravity(
      props.message,
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
      25,
      50,
    );
    return null;
  }
  return null;
};

const data = new Array(5).fill(0);


 export default class VideoCategoryDetails extends React.Component {

  static navigationOptions ={
    header:null
  }

  constructor(props) {
    super(props);

    this.state = {
      rss: [],
      headerrss:[],
      fullscreen: "fullscreen",
      selectedVideoItem: "",
      selected:'',
      newsLikedStatus: "",
      channel_id: 46,
      category_id: this.props.navigation.getParam('category_id', 'Default'),
      category_name: this.props.navigation.getParam('category_name', 'Default'),
      isFetching: false,
      selectedVideo: "",
      selectedVideoId: this.props.navigation.getParam('VideoId','DefaultVideoId'),
      selectedVideoTitle: "Test",
      height: 200,
      updateRss:[],
      newsLiked:null,
      comment: false,
      selectedChannel: null,
      selectedStatus: null,
      previousSelectedStatus: null,
      followingMessage: " ",
      followingStatus: " ",
      follow: "Follow",
      following: "Following",
      modalVisible: false,
      modalShare: "",
      status: "",
      layoutType: true,
      isFetching: false,
      isLoading: true,
      backgroundColor: '',
      user_id:null,
      news_id:null,
      selectedDetail:{}
    };
    this.setRss = this.setRss.bind(this);
  }

  followingPage = (category_id,channel_id) => {
    let catid = category_id;
    let chanid = channel_id;
    
    this.props.navigation.navigate('followingPage',{'category_id': catid,'channel_id': chanid});
  }

  

  componentDidMount(){
    // alert('reached')
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    this._getStoreUserDetail()
    this.loadRss();
  }

  _getStoreUserDetail = async () => {
    try {
      const value = await AsyncStorage.getItem('user_id');
      if (value !== null) {
        this.setState({user_id:value})
      }
    } catch (error) {
      alert('have issue while fetching user detail');
    }
  }

  componentWillUnmount() {
    //Test Basis
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }
  componentDidUpdate(prevProps, prevState, snapshot){
    // if (this.state.selectedChannel !== prevState.selectedChannel ) {
    //     //console.warn("componentDidUpdate "+this.state.selectedChannel+"prevState "+prevState.selectedChannel);
    //     //console.warn("if prevState "+prevState.selectedChannel+" selectedChannel "+this.state.selectedChannel+" prevState "+prevState.selectedStatus+" selectedStatus "+this.state.selectedStatus);
    //   this.loadRss();
    // }
    // if (this.state.newsLiked !== prevState.newsLiked){
    //   this.loadRss();
    // }
  
  }

  handleBackPress = () => {
    console.warn('BackPress');
    this.setState({
      comment: false
    })
    this.props.navigation.navigate('Videos'); // works best when the goBack is async
    return true;
  }

  

  handleButtonPress = () => {
    this.setState(
      {
        visible: true,
      },
      () => {
        this.hideToast();
      },
    );
  };

  onRefresh() {
    this.setState({ isFetching: true }, function() { this.loadRss() });
  }
  

  hideToast = () => {
    this.setState({
      visible: false,
    });
  };

  loadRss = _.debounce(() => {
    //this.loadUpdateRss();
      // alert('reached');
      fetch(BASE_URL+'Video/homeNewsVideos', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          "user_id": this.state.user_id,
          "category_id": this.state.category_id
        }),
      })
      .then(response => response.json())
      .then((json) => {
        // alert(JSON.stringify(json));
        if(json.status){
          this.setState({
            rss: json.newslist,
            isFetching: false,
            isLoading: false 
          },()=>this._setInitialPlay(this.state.rss[0]));
        }else{
          alert('Problem in fetching detail')
        }
      })
      .catch((error) => {
        // alert(JSON.stringify(error))
      });
  },250);

  _setInitialPlay(item){
    // alert(JSON.stringify(item))
    let detail = this.state.rss[0]
    this.setState({selectedDetail:detail})
  }


  toggleFollow = (item) => {

    if(this.state.user_id == null){
      alert('please login')
    }else{
      this.setState({
        selectedChannel: item.channel_name+item.following,
        selectedStatus: item.following,
        isLoading: false 
      });

      //console.warn("channelName "+this.state.selectedChannel);

    
        fetch(BASE_URL+'Rss/channelFollow', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            user_id: this.state.user_id,
            channel_id: item.channel_id,
          }),
        })
        .then(response => response.json())
        .then((json) => {
          console.warn(json);

        })
        .catch(function (error) {
          console.warn(error);
        });
    }
    
  }

  toggleLike = () => {
    
    if(this.state.user_id == null){
      alert('please login')
    }else{
      //To Follow and Unfollow Channel
      fetch(BASE_URL+'Video/newsLike', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          news_id: this.state.selectedVideoItem.news_id,
          user_id: this.state.user_id,
        }),
      })
      .then(response => response.json())
      .then((json) => {
        const fStatus = json.Status ;
        this.setState({
          newsLikedStatus: json.Status
        })
        console.warn(json);
        
      })
      .catch(function (error) {
        console.warn(error);
      });
    }

  }

  navCommentPage = (item) => {
    this.props.navigation.navigate('Comment',{item});
    //To Follow and Unfollow Channel  
    // fetch('http://limrademo.com/timesqure/ApiV1/Rss/channelFollow', {
    //   method: 'POST',
    //   headers: {
    //     'Accept': 'application/json',
    //     'Content-Type': 'application/json',
    //   },
    //   body: JSON.stringify({
    //     channel_id: 49,
    //     user_id: 8,
    //   }),
    // })
    // .then(response => response.json())
    // .then((json) => {
    //   const fStatus = json.Status ; 
    //   console.warn(fStatus);
    //   this.setState({
    //     visible: !this.state.visible,
    //   });
      
    // })
    // .catch(function (error) {
    //   console.warn(error);
    // });

  }

  setRss(json) {
    console.warn(json.items);
    this.setState({
      rss: json.items,
      isFetching: false 
    });
  }

  loadModal(){
    //Here item is taken as way of optimisation of Modals. Since modal is not looped so the value is passed in this.state.modalShare: item
    // this.setState({
    //   modalShare: item
    // })
    this.setModalVisible(true);
  }

  whatsappLink(){
    // Whatsapp Opening external link. Done!
    const newsTitle = this.state.selectedVideoTitle;
    // encodeURI () function returns the Whatsapp supported encode format of link which also contains image sends link as well images
    const link = "https://youtu.be/"+this.state.selectedVideoId;
    const newsLink = encodeURI(link);
    const appSource = "Source%3A+TimeSqure%0D%0A%0D%0ADownload+Now%0D%0Ahttps%3A%2F%2Fwww.limratechnosys.com%2F";

    const url= "whatsapp://send?text="+newsTitle+"%0D%0A"+newsLink+"%0D%0A"+appSource ;
    Linking.canOpenURL(url)
    .then((supported) => {
      if (!supported) {
        console.log("Can't handle url: " + url);
        console.warn("Can't handle url: " + url);
  
      } else {
        return Linking.openURL(url);
      }
    })
    .catch((err) => {
    console.error('An error occurred', err); 
    console.warn('An error occurred', err)});
    }

  async onShare(){
    //Sharing Content with all apps Done!
  console.warn(this.state.selectedVideoTitle);
  // Whatsapp Opening external link. Done!
  const newsTitle = this.state.selectedVideoTitle;
  // OnShare method does not require encodeURI () function 
  const newsLink = "https://youtu.be/"+this.state.selectedVideoId;
  const appSource = "Source:"+" TimeSqure"+"\n\n"+"Download Now "+"https://www.limratechnosys.com/";

  const url= newsTitle +"\n" + newsLink +"\n"+appSource ;

    try {
      const result = await Share.share({
        message: url,
      })

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
    console.warn(visible);
  }
  


  selectedVideo = (item) =>{
   this.setState({
     selectedVideoItem: item,
     selectedVideo: item.news_id,
     selectedVideoId: item.link,
     selectedVideoTitle: item.title,
     fullscreen: "NO FullScreen",
     height: 199+1,
     PlaySelected: true
   })
   console.warn(this.state.status);
   //console.warn(this.state.selectedVideo+item.news_id);
  }

  Ready= (e,item) => {
    //e => this.setState({ isReady: this.state.selectedVideo ===  item.news_id ? true : false })
    // if (this.state.selectedVideo ===  item.news_id )
    // {
    //   this.setState({ 
    //     isReady: true ,
    //     PlaySelected: true
    //   });
    // }else{
    //   this.setState({ 
    //     isReady: false ,
    //     PlaySelected: false
    //   });
    // }


    //console.warn(this.state.isReady);
    //console.warn(e);

  }

  ChangeState = (e,item) => {
    console.warn("status");
    console.warn(e.state);

  }

  render() {
    // alert(JSON.stringify(this.state.selectedDetail))
    let IconComponent = Icon;
    const { navigate } = this.props.navigation;

    return (

      this.state.isLoading ?
      <View style={styles.MainContainer}>
      <ActivityIndicator size="large" />
      </View>:
      <View style={styles.MainContainer}>

       <View style={styles.container}>     
              <Modal
              transparent={true}
              visible={this.state.modalVisible}
              onRequestClose={() =>{ }}>
              <View style={{flex: 1,flexDirection: 'column',justifyContent:'flex-end'}}>
                <TouchableOpacity style={{flex: 1,backgroundColor:'black',opacity:this.state.modalVisible?0.3:0}}
                    onPress={() => {this.setModalVisible(!this.state.modalVisible);}}>
                </TouchableOpacity>

                <View style={{backgroundColor:'white'}}>
                    <View style={{paddingHorizontal:15}}>
                    <Text onPress={()=>this.onShare()} style={{textAlignVertical:'center',fontSize:16,paddingVertical:10}}> <Icon name={'md-share'} size={21}/>  Share...  </Text>
                    <Text style={{textAlignVertical:'center',fontSize:16,paddingVertical:10}}> <Icon name={'md-open'} size={21}/>  Browse </Text>
                    <Text style={{textAlignVertical:'center',fontSize:16,paddingVertical:10}}> <Icon name={'md-hand'} size={21}/>  Block </Text>
                    <Text style={{textAlignVertical:'center',fontSize:16,paddingVertical:10}}> <Icon name={'md-remove-circle-outline'} size={21}/>  Hide this news</Text>
                    <Text style={{textAlignVertical:'center',fontSize:16,paddingVertical:10}}> <Icon name={'md-thumbs-up'} size={21}/>  Show less of such content</Text>
                    <Text style={{textAlignVertical:'center',fontSize:16,paddingVertical:10}}> <Icon name={'md-thumbs-down'} size={21}/>  Show more of such content</Text>
                  </View>
                </View>
              </View>
            </Modal>
      
        </View>

      <View style={{flexDirection:'row',justifyContent:"space-between",alignItems:"center",borderBottomWidth:1,borderBottomColor:'#d9e1e3',height:60,backgroundColor:'white'}}>
            <TouchableOpacity style={{paddingVertical:10,paddingHorizontal:15}} onPress={() => this.props.navigation.navigate('Videos')}><Text ><IconComponent name={'md-arrow-back'} size={25} color={'grey'}/></Text></TouchableOpacity>
            <Text style={{fontSize:18,fontWeight:'bold',color:'black'}}>{this.state.category_name}</Text>
            <TouchableOpacity style={{paddingVertical:10,paddingHorizontal:15}} onPress={() => {this.loadModal()}}><Text ><IconComponent name={'md-more'} size={25} color={'white'}/></Text></TouchableOpacity>
      </View>
      {/*<YouTube
        apiKey={YoutubeApiKey}
        videoId={this.state.selectedVideoId} // The YouTube video ID
        play={true} // control playback of video with true/false
        fullscreen={this.state.fullscreen == "fullscreen"?true:false} // control whether the video should play in fullscreen or inline
        loop={false} // control whether the video should loop when ended
        controls={1}
        resumePlayAndroid={true} // tab switching crashes without this
        onReady={e => this.setState({ isReady: true })}
        onChangeState={e => this.setState({ status: e.state })}
        onChangeQuality={e => this.setState({ quality: e.quality })}
        onError={e => console.warn(e.error)}
        style={[styles.YouTubePlayer,{height:this.state.height}]}
        />*/}
        <View style={{marginHorizontal:10,alignItems:'center',height:45,flexDirection:'row',}}>
        <Text style={{fontSize:16,color:'black',textAlignVertical:'center',paddingTop:10}} numberOfLines={2}>{this.state.selectedDetail.title}</Text>
        </View>
        <View style={{marginHorizontal:10,alignItems:'center',height:45,flexDirection:'row',justifyContent:'space-between'}}>
        <Text style={{fontSize:13}}>68 views</Text>
        <TouchableOpacity style={{paddingVertical:10,paddingHorizontal:15}} onPress={() => {this.setModalVisible(!this.state.modalVisible);}}><Text ><IconComponent name={'md-more'} size={25} color={'grey'}/></Text></TouchableOpacity>
        </View>

        <View style={{marginHorizontal:10,alignItems:'center',height:45,borderTopWidth:1,borderBottomWidth:1,borderColor:'#ecf0f1',flexDirection:'row',justifyContent:'space-between'}}>
        <TouchableOpacity onPress={()=>this.navCommentPage(this.state.selectedDetail)} style={{paddingVertical:5,}}>
        <Icon name={'md-chatboxes'} size={21} color={'#2980b9'} />
        {/* <Text style={{fontSize:12,textAlign:'center'}}>1</Text> */}
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>this.toggleLike(this.state.selectedDetail)} style={{paddingVertical:5,}}>
        <Icon name={this.state.newsLikedStatus === 1 ? 'md-thumbs-up' : 'md-thumbs-down'} size={21} color={'#c0392b'} />
        {/* <Text style={{fontSize:12,textAlign:'center'}}>1</Text> */}
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>this.whatsappLink(this.state.selectedDetail)} style={{paddingVertical:5,}}>
        <Icon name={'logo-whatsapp'} size={21} color={'#25d366'} />
        {/* <Text style={{fontSize:12,textAlign:'center'}}>1</Text> */}
        </TouchableOpacity> 
        </View>
        
        <FlatList
        data={ this.state.rss }
        // extraData={this.state.selectedVideo}
        keyExtractor={(item, index) => index.toString()}
        // stickyHeaderIndices={this.state.stickyHeaderIndices}
        renderItem={({item}) => 
        <View>

         <TouchableOpacity onPress={()=>this.setState({selectedDetail:item})} style={{flex:1,flexDirection:'row',paddingHorizontal:5}}>
            <Image style={styles.imageView} source={{uri:"https://i.ytimg.com/vi/"+item.link+"/0.jpg"}} />
            <Text style={styles.textView}>{item.title}</Text>
         </TouchableOpacity>         

         </View>  

         }
        />

        
    </View>

      );
    }
  }
    


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems:'center'
  },
  YouTubePlayer:{
    alignSelf:'stretch',
    marginVertical:10
  },
  subcontainer:{
    paddingHorizontal:15
  },
  imageView:{
    height: 90,
    width: "30%",
    margin:5
  },
  textView: {
    width: "60%",
    fontSize: 14,
    color: 'black',
    fontWeight: 'bold',
    textAlignVertical:'center'
  },
  contentContainer: {
    paddingVertical: 20
  },
  textBold:{
    fontWeight: 'bold'
  }
});