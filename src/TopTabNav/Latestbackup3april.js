import React, { Component } from 'react';
import {
  View,
  Text,Modal,Alert,
  ActivityIndicator,AsyncStorage,
  StyleSheet,NetInfo,FlatList,Image,Linking,Share,Platform,ScrollView,TouchableHighlight,TouchableOpacity,Dimensions,
} from 'react-native';
import ScrollableTabView, { ScrollableTabBar } from 'react-native-scrollable-tab-view';

import Icon  from 'react-native-vector-icons/Ionicons';

//import * as rssParser from 'react-native-rss-parser';


class Latest extends React.Component{
  constructor(props) {
    super(props);

    this.state = {
      rss: [],
      headerrss:[],
      modalVisible: false,
      modalShare: "",
      layoutType: true,
      isFetching: false,
      isLoading:true
    };
    //this.setRss = this.setRss.bind(this);
    AsyncStorage.setItem('userToken','abc');
  }



  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  componentDidMount() {
    NetInfo.getConnectionInfo().then((connectionInfo) => {
      console.warn(
        'Initial, type: ' +
          connectionInfo.type +
          ', effectiveType: ' +
          connectionInfo.effectiveType,
      );
    });
    function handleFirstConnectivityChange(connectionInfo) {
      console.warn(
        'First change, type: ' +
          connectionInfo.type +
          ', effectiveType: ' +
          connectionInfo.effectiveType,
      );
      NetInfo.removeEventListener(
        'connectionChange',
        handleFirstConnectivityChange,
      );
    }
    NetInfo.addEventListener('connectionChange', handleFirstConnectivityChange);
   this.loadHeaderRss();
   this.loadRss();
  }


    loadModal(item){
      //Here item is taken as way of optimisation of Modals. Since modal is not looped so the value is passed in this.state.modalShare: item
      this.setState({
        modalShare: item
      })
      this.setModalVisible(true);
    }
  
    link(item){
      // Whatsapp Opening external link. Done!
      const newsTitle = item.title;
      // encodeURI () function returns the Whatsapp supported encode format of link which also contains image sends link as well images
      const newsLink = encodeURI(item.link);
      const appSource = "Source%3A+TimeSqure%0D%0A%0D%0ADownload+Now%0D%0Ahttps%3A%2F%2Fwww.limratechnosys.com%2F";

      const url= "whatsapp://send?text="+newsTitle+"%0D%0A"+newsLink+"%0D%0A"+appSource ;
      Linking.canOpenURL(url)
      .then((supported) => {
        if (!supported) {
          console.log("Can't handle url: " + url);
          console.warn("Can't handle url: " + url);
    
        } else {
          return Linking.openURL(url);
        }
      })
      .catch((err) => {
      console.error('An error occurred', err); 
      console.warn('An error occurred', err)});
      }
  
    async onShare(modalShare){
      //Sharing Content with all apps Done!
    console.warn(modalShare);
    // Whatsapp Opening external link. Done!
    const newsTitle = modalShare.title;
    // OnShare method does not require encodeURI () function 
    const newsLink = modalShare.link;
    const appSource = "Source:"+" TimeSqure"+"\n\n"+"Download Now "+"https://www.limratechnosys.com/";

    const url= newsTitle +"\n" + newsLink +"\n"+appSource ;

      try {
        const result = await Share.share({
          message: url,
        })
  
        if (result.action === Share.sharedAction) {
          if (result.activityType) {
            // shared with activity type of result.activityType
          } else {
            // shared
          }
        } else if (result.action === Share.dismissedAction) {
          // dismissed
        }
      } catch (error) {
        alert(error.message);
      }
    };

    loadHeaderRss(){
      fetch('http://limrademo.com/timesqure/ApiV1/Rss/categoriesList')
      .then(response => response.json())
      .then((json) => {
        
        console.warn(json);
        this.setState({
          headerrss: json.categoriesList[0],
          isFetching: false,
          isLoading:false 
        });
      });
    }

    loadRss(){

    //   const parseUrl = 'https://api.rss2json.com/v1/api.json?rss_url=';
    //   const rssUrl = 'https://timesofindia.indiatimes.com/rssfeeds/-2128838597.cms';
    //   //fetch('http://limrademo.com/timesqure/timesqure_api_ver_01/RssSample/SampleApi/oneIndia')
    //   fetch(parseUrl+rssUrl)
    //   .then(response => response.json())
    //   .then((json) => {
        
    //     console.warn(json);
    //     this.setState({
    //       rss: json.items,
    //       isFetching: false,
    //       isLoading: false
    //     });
    //   });
    console.warn("loadRss");
    fetch('http://limrademo.com/timesqure/ApiV1/Rss/homeNews', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        cat_id:11,
      }),
    })
    .then(response => response.json())
    .then((json) => {
      console.warn("hello");
      console.warn(json);
      this.setState({
        rss: json.newslist[0],
        isFetching: false,
        isLoading: false 
      });
    })
    .catch(function (error) {
      console.warn(error);
    });



  }

    // setRss(json) {
    //   console.warn(json.items);
    //   this.setState({
    //     rss: json.items,
    //     isFetching: false 
    //   });
    // }

    onRefresh() {
      this.setState({ isFetching: true }, function() { this.loadRss() });
    }

    Details = () =>{
      this.props.navigation.navigate('Details');
    }

    changeLayout(){
      this.setState({
      layoutType: !this.state.layoutType
      });
    }





    // birthday = new Date('August 19, 1975 23:15:30');
    // let date1 = birthday.getDate();
    //<View style={this.state.modalVisible ? styles.modalContainer : styles.container}>

    categorytab(){
      let IconComponent = Icon;
      
        return this.state.headerrss.map((item,key) => {
        return <View key={item.category_id} tabLabel={item.category_name}>
        {
          item.category_name == 'Entertainment'?
           this.state.headerrss.map((itemx,key) => {
             return <Text>{itemx.category_name}</Text>
            })
        :
          <FlatTabLoop category_id={item.category_id}/>
        }
         </View>
        })
    }


  render () {
    let IconComponent = Icon;
    return (
        this.state.isLoading ?
        <View><ActivityIndicator /></View> :

      <View style={styles.container}>
      <Modal
          animationType={"slide"}
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() =>{ }}>
          <View style={{flex: 1,flexDirection: 'column',justifyContent:'flex-end'}}>
            <TouchableOpacity style={{flex: 1,backgroundColor:'black',opacity:this.state.modalVisible?0.5:0}}
                onPress={() => {this.setModalVisible(!this.state.modalVisible);}}>
            </TouchableOpacity>

            <View style={{backgroundColor:'white'}}>
                <View style={{paddingHorizontal:15}}>
                 <Text onPress={this.onShare.bind(this,this.state.modalShare)} style={{textAlignVertical:'center',fontSize:16,paddingVertical:10}}> <Icon name={'md-share'} size={21}/>  Share...  </Text>
                 <Text style={{textAlignVertical:'center',fontSize:16,paddingVertical:10}}> <Icon name={'md-open'} size={21}/>  Browse </Text>
                 <Text style={{textAlignVertical:'center',fontSize:16,paddingVertical:10}}> <Icon name={'md-hand'} size={21}/>  Block </Text>
                 <Text style={{textAlignVertical:'center',fontSize:16,paddingVertical:10}}> <Icon name={'md-remove-circle-outline'} size={21}/>  Hide this news</Text>
                 <Text style={{textAlignVertical:'center',fontSize:16,paddingVertical:10}}> <Icon name={'md-thumbs-up'} size={21}/>  Show less of such content</Text>
                 <Text style={{textAlignVertical:'center',fontSize:16,paddingVertical:10}}> <Icon name={'md-thumbs-down'} size={21}/>  Show more of such content</Text>

                 <TouchableHighlight
                  onPress={() => {
                    this.setModalVisible(!this.state.modalVisible);
                  }}>
                  <Text>Hide Modal</Text>
                
                </TouchableHighlight>
              </View>
            </View>
          </View>
        </Modal>

        <FlatList
        data={ this.state.rss }
        onRefresh={() => this.onRefresh()}
        refreshing={this.state.isFetching}
        renderItem={({item}) => 
        
            <View style={styles.container}>

            <TouchableOpacity style={{paddingBottom:15,paddingHorizontal:15}} onPress={this.Details}>

            

             {this.state.layoutType ?
             <View  style={{ flexDirection:'column'}} >
             <Text  style={{ fontSize: 15,color: 'black',fontWeight: 'bold',paddingBottom:10 }}>{item.title}</Text>
             <Image style={{ width: "100%",height: 180, borderRadius: 5 }} source={{uri: item.url}}   />
             
             <View  style={{ flexDirection:"row",alignItems:'baseline',paddingTop:5 }}>
              <Text style={{ flex:1,fontSize:11 }}>{item.pubDate}</Text>
              <TouchableOpacity onPress={this.link.bind(this,item)}>
              <Icon style={{ paddingRight:20 }}  name={'logo-whatsapp'} size={25} color={'#25d366'} />
              </TouchableOpacity>
              <TouchableOpacity onPress={this.loadModal.bind(this,item)}>
              <Icon style={{ paddingRight:10 }}  name={'md-more'}       size={25} color={'grey'} />
              </TouchableOpacity>
             </View>
             </View> :


             <View style={{flexDirection:"row",alignItems:'flex-end' }}>

             <View   style={{ flex:1,flexDirection:"column" }}>
               <Text style={{ flex:1,fontSize: 15,color: 'black',fontWeight: 'bold' }}>{item.title}</Text>
               <Text style={{ fontSize: 11, marginTop: 15}}>{item.pubDate}</Text>
             </View>  
             
              <Icon  style={{ paddingRight:20 }}  name={'logo-whatsapp'} size={25} color={'#25d366'} />
              <Icon  style={{ paddingRight:10 }}  name={'md-more'}       size={25} color={'grey'} />
              <Image style={{ height:100,width:100,borderRadius:5 }} source={{uri: item.url}}   /> 
              </View>}



             </TouchableOpacity>    
             
            </View>
        
          }
          keyExtractor={(item, index) => index.toString()}
        />
         
        {/*
          onPress={this.GetItem.bind(this, item.title)}
         <Image source = {{ uri: item.image }} style={styles.imageView} />
          {this.state.rss.map(item =>
          <View key={item.guid}>
            <Text style={styles.textBold}>{item.title}</Text>
            <Text>{item.content}</Text>
          </View>
        )} */}


      <ScrollableTabView
        style={{marginTop: 5, }}
        initialPage={0}
        renderTabBar={() => <ScrollableTabBar />}>

        {this.categorytab()}

      </ScrollableTabView>

      </View>
    )
  }


}


export class FlatTabLoop extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      rss: [],
      isLoading: true,
      headerrss:[],
      selected:'',
      channel_id: 46,
      isFetching: false,
    };
    //this.setRss = this.setRss.bind(this);
  }

componentDidMount(){
 this.loadHeaderRss();

}

onRefresh() {
  this.setState({ isFetching: true }, function() { this.loadRss() });
}


loadHeaderRss(){
  fetch('http://limrademo.com/timesqure/ApiV1/Rss/categoriesList')
  .then(response => response.json())
  .then((json) => {
    
    console.warn(json);
    this.setState({
      headerrss: json.categoriesList[0],
      isFetching: false,
      isLoading:false 
    });
  });
}

  render () {
    let IconComponent = Icon;

    return (
       this.state.isLoading ?
      <View style = { styles.MainContainer }>
      <ActivityIndicator size="large" color="grey" />
      </View>:
      <FlatList
      data={ this.state.rss }
      onRefresh={() => this.onRefresh()}
      refreshing={this.state.isFetching}
      renderItem={({item}) => 

          <View style={styles.container}>

          <TouchableOpacity style={{paddingBottom:15}} onPress={this.Details}>
          <View style={styles.subcontainer} >
          <Text style={styles.textView}>{item.title}</Text>
          <Image style={styles.imageView} source={{uri: item.url}}   />
          
          <View style={{flexDirection:"row", }}>
            <Text style={{ fontSize: 11,textAlign: 'left', flex:1, alignSelf: 'flex-start',  marginTop: 15}}>{item.pubDate}</Text>
            <IconComponent style={{textAlign: 'right',alignSelf: 'flex-end', marginRight:20, marginTop:5}}  name={'logo-whatsapp'} size={25} color={'#25d366'} />
            <IconComponent style={{textAlign: 'right',alignSelf: 'flex-end',  marginTop:5,transform: [{ rotate: '90deg'}] }}  name={'ios-more'} size={25} color={'grey'} />
          </View>
          </View>
          </TouchableOpacity>    
          
          </View>

        }
        keyExtractor={(item, index) => index.toString()}
      />
    )
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    marginTop:10,
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    marginTop:10,
    backgroundColor:'black',
    opacity: 0.5
  },
  subcontainer:{
    paddingHorizontal: 15,
  },
  imageView:{
    height: 170,
    width: '100%',
    marginTop: 15,
    borderRadius: 5,
  },
  textView: {
    fontSize: 15,
    color: 'black',
    fontWeight: 'bold'
  },
  contentContainer: {
    paddingVertical: 20
  },
  textBold:{
    fontWeight: 'bold'
  }
});
export default Latest;
