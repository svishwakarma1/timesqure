import React, { Component } from 'react';
import {
  View,
  Text,
  ActivityIndicator, 
  StyleSheet,NetInfo,FlatList,Image,ImageBackground,Platform,ScrollView,TouchableHighlight,TouchableOpacity
} from 'react-native';

import Icon  from 'react-native-vector-icons/Ionicons';
import ScrollableTabView, { ScrollableTabBar } from 'react-native-scrollable-tab-view';
import axios from 'axios';

import { BASE_URL } from '../services/Api_constant';
//import { Container} from 'native-base';
//import  Container from './Test/Container.js';
//import ChannelHeader from './Components/ChannelHeader.js';
const data = new Array(5).fill(0);


 export default class Latest extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      rss: [],
      isLoading: true,
      headerrss:[],
      selected:'',
      channel_id: 46,
      category_id: 10,
      isFetching: true,
    };
    //this.setRss = this.setRss.bind(this);
  }

  static navigationOptions =
  {
    headerTransparent: true,
  };

  componentDidMount() {
   this.loadHeaderRss();
  }



  Details = () =>{
    this.props.navigation.navigate('Details');
  }

  loadHeaderRss(){
    fetch(BASE_URL+'Rss/categoriesList')
    .then(response => response.json())
    .then((json) => {
      console.warn('ola');
      console.warn(json.categoriesList);
      this.setState({
        headerrss: json.categoriesList,
        isFetching: false 
      });
    });
  }

  



  followingPage(){
   this.props.navigation.navigate('followingPage');
  }

  /**
   *  Problem : Required Scrollable iterative (loop) based Tab.
   *  <View> tag was not allowing to scroll the react-native-scrollview-tab as it freezes the Tab.
   *  After Researching a lot there were third party plugins but for performance and flexibility reasons decided to simultaneously develope.
   *  so finished, 90% of React Native Custom Development using ScrollView and Flatlist almost achieved it but the animation and indicator part was left.  
   *  For Time Being Ali sir suggested to use react-native-scrollview-tab and native base <Container> it fixed the Problem.
   *  As the Internet Community says the more plugin the more heavy apps will be which ultimately affects the performance.
   *  But the question is Why nativebase replaced? <View> tag with <Container> tag and the difference between them and viceversa.
   *  Error was reporting something related to Adjacent JSX Fragment. Seeking something related to <View> tag. After Analysing, Studying nativebase container method when implementing it again freezed.
   *  After Research tried this <React.Fragment> tag based on anlaysing and studying a plugin module called react-native-paper.
   *  They used so this solved the problem <React.Fragment> .
   *  https://reactjs.org/blog/2017/11/28/react-v16.2.0-fragment-support.html
   *  This above article is very important also it has information about return[] (array) and return()
   */

  /**
   * Api's given by Harpreet
   * http://limrademo.com/timesqure/timesqure_api_ver_01/RssSample/SampleApi/oneIndia
   * http://limrademo.com/timesqure/timesqure_api_ver_01/RssSample/channeldetails/46
   * http://limrademo.com/timesqure/timesqure_api_ver_01/RssSample/news
   * channel_id:46
   * category_id:10
   * http://limrademo.com/timesqure/timesqure_api_ver_01/Follow/followingNews
   * "channel_id": 89,
   * "user_id": 12
   */


  categorytab(){
    let IconComponent = Icon;
    
    return this.state.headerrss.map((item,key) => {
      let tabName = item.name;

      return <View key={item.id} tabLabel={tabName.toUpperCase()}>
      {
        item.name == 'Entertainment'?
         this.state.headerrss.map((itemx,key) => {
           return <Text>{itemx.name}</Text>
          })
      :
        <FlatTabLoop category_id={item.category_id}/>
      }
       </View>
      })
  }

  render() {
    let IconComponent = Icon;
    const i = this.props.i;

    return (


      <ScrollableTabView
        style={{marginTop: 5, }}
        initialPage={0}
        renderTabBar={() => <ScrollableTabBar scrollOffset={5} style={{height:30,}} textStyle={{fontSize:13}} tabStyle={{height:29}} underlineStyle={{backgroundColor:'#f22613',height:2}} backgroundColor={'#ebebeb'} activeTextColor={'#f22613'} inactiveTextColor={'grey'} />}>

        {this.categorytab()}

      </ScrollableTabView>
    );
  }
}


export class FlatTabLoop extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      rss: [],
      isLoading: true,
      headerrss:[],
      selected:'',
      channel_id: 46,
      isFetching: false,
    };
    //this.setRss = this.setRss.bind(this);
  }

componentDidMount(){
 this.loadRss();

}

onRefresh() {
  this.setState({ isFetching: true }, function() { this.loadRss() });
}

loadRss(){


  fetch(BASE_URL+'Rss/homeNews', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      cat_id: 11,
    }),
  })
  .then(response => response.json())
  .then((json) => {
    console.warn(json.newslist);
    this.setState({
      rss: json.newslist,
      isFetching: false,
      isLoading: false 
    });
  })
  .catch(function (error) {
    console.warn(error);
  });
}

  render () {
    let IconComponent = Icon;

    return (
       this.state.isLoading ?
      <View style = { styles.MainContainer }>
      <ActivityIndicator size="large" color="grey" />
      </View>:
      <FlatList
      data={ this.state.rss }
      onRefresh={() => this.onRefresh()}
      refreshing={this.state.isFetching}
      renderItem={({item}) => 

          <View style={styles.container}>

          <TouchableOpacity style={{paddingBottom:15}} onPress={this.Details}>
          <View style={styles.subcontainer} >
          <Text style={styles.textView}>{item.title}</Text>
          <Image style={styles.imageView} source={{uri: item.image}}   />
          
          <View style={{flexDirection:"row", }}>
            <Text style={{ fontSize: 11,textAlign: 'left', flex:1, alignSelf: 'flex-start',  marginTop: 15}}>{item.pubDate}</Text>
            <IconComponent style={{textAlign: 'right',alignSelf: 'flex-end', marginRight:20, marginTop:5}}  name={'logo-whatsapp'} size={25} color={'#25d366'} />
            <IconComponent style={{textAlign: 'right',alignSelf: 'flex-end',  marginTop:5,transform: [{ rotate: '90deg'}] }}  name={'ios-more'} size={25} color={'grey'} />
          </View>
          </View>
          </TouchableOpacity>    
          
          </View>

        }
        keyExtractor={(item, index) => index.toString()}
      />
    )
  }
}






const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    marginTop:10

    
  },
  subcontainer:{
    paddingHorizontal: 15,
  },
  imageView:{
    height: 170,
    width: '100%',
    marginTop: 15,
    borderRadius: 5,
  },
  textView: {
    fontSize: 15,
    color: 'black',
    fontWeight: 'bold'
  },
  contentContainer: {
    paddingVertical: 20
  },
  textBold:{
    fontWeight: 'bold'
  }
});

