import React, { Component } from 'react';
import {
  View,
  Text,
} from 'react-native';

 class World extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>World!</Text>
      </View>
    );
  }
}

export default World;