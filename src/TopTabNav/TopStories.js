import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,StyleSheet
} from 'react-native';

import Icon  from 'react-native-vector-icons/Ionicons';


 class TopStories extends React.Component {
  render() {
    return (
      <View style={styles.container}>

        
        <View style={{paddingHorizontal:15}}>

        <TouchableOpacity style={{paddingBottom:15}} onPress={this.Details}>

            <View   style={{ flexDirection:'column' }} >
             <Text  style={{ fontSize: 15,color: 'black',fontWeight: 'bold' }}>item.title</Text>
             <Image style={{ width: "100%",height: "65%", borderRadius: 5 }} source={require('../img/static/news2.jpg')}   />
             
             <View  style={{ flexDirection:"row",alignItems:'baseline',paddingTop:5 }}>
              <Text style={{ flex:1,fontSize:11 }}>item.pubDate</Text>
              <Icon style={{ paddingRight:20 }}  name={'logo-whatsapp'} size={25} color={'#25d366'} />
              <Icon style={{ paddingRight:10 }}  name={'md-more'}       size={25} color={'grey'} />
             </View>

             </View>

       </TouchableOpacity>  
       <TouchableOpacity style={{paddingBottom:15}} onPress={this.Details}>

          <View style={{flexDirection:"row",alignItems:'flex-end' }}>

             <View   style={{ flex:1,flexDirection:"column" }}>
               <Text style={{ flex:1,fontSize: 15,color: 'black',fontWeight: 'bold' }}>Title</Text>
               <Text style={{ fontSize: 11, marginTop: 15}}>item.pubDate</Text>
             </View>  
             
              <Icon  style={{ paddingRight:20 }}  name={'logo-whatsapp'} size={25} color={'#25d366'} />
              <Icon  style={{ paddingRight:10 }}  name={'md-more'}       size={25} color={'grey'} />
              <Image style={{ height:100,width:100,borderRadius:5 }} source={require('../img/static/news2.jpg')}   /> 
          </View>

        </TouchableOpacity>    
        </View>
 
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    marginTop:10

    
  },
  listsubcontainer:{
    paddingHorizontal: 15,
  },
  listImageView:{
    height: 100,
    width:  100,
    borderRadius: 5,
  },
  textView: {
    fontSize: 15,
    color: 'black',
    fontWeight: 'bold'
  },
  contentContainer: {
    paddingVertical: 20
  },
  textBold:{
    fontWeight: 'bold'
  }
});

export default TopStories;