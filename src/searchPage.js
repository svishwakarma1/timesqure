import React, { Component } from 'react';
import {
  View,
  Text,TextInput,FlatList,TouchableOpacity
} from 'react-native';
import Icon  from 'react-native-vector-icons/Ionicons';


const data = new Array(5).fill(0);

export class SearchPageInput extends React.Component {

  render() {
    return (
        <TextInput placeholder={'Search News, Videos & Memes'} style={{width:'95%',padding:0,height:30,borderBottomWidth:1,borderBottomColor:'#bdc3c7'}} />
    );
  }
}

export default class searchPage extends React.Component {

    static navigationOptions =
    {
     headerTitle:  <SearchPageInput />,
    };

  render() {
    return (
      <View style={{}}>
        <Text style={{padding:20,fontSize:19}}>Trending</Text>
        <FlatList
            data={ data }
            renderItem={({item}) => 

          <View>

          <TouchableOpacity style={{paddingBottom:15,flexDirection:'row'}} onPress={this.Details}>
          
            <Icon style={{paddingHorizontal:15,paddingVertical:10}}  name={'md-trending-up'} size={21} color={'#25d366'} />
            <Text style={{paddingVertical:5,fontSize:19}}>Trending</Text>
         
          </TouchableOpacity>    
          
          </View>

        }
        keyExtractor={(item, index) => index.toString()}
      />
      </View>
    );
  }
}
