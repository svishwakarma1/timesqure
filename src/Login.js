import React, { Component } from 'react';
import {
  View,
  Text,AsyncStorage,Image,StyleSheet, KeyboardAvoidingView,ImageBackground,TextInput,TouchableOpacity
} from 'react-native';

import { BASE_URL } from './services/Api_constant';

export default class Login extends React.Component {
  static navigationOptions =
  {
     drawerLabel: null,
     header : null,
  };
  
  constructor(props){
     super(props);
     //const userToken = AsyncStorage.getItem('userToken');
     this.state ={
       email: "",
       password: "",
       errors: "",
       detail:{},
       user_id:'',
       token:''
     }
 
   }

  componentDidMount(){
    this._getStoreUserDetail()
  }
   
  // Fetch the token from storage then navigate to our appropriate place
  gotoNextActivity = () =>
  {
     //if(){
        this.props.navigation.navigate('App');
     //}
  }


  _storeUserDetail = async () => {
    try {
      await AsyncStorage.setItem('user_id',this.state.user_id);
      this.props.navigation.navigate('Home')
    } catch (error) {
      alert('issue while store');
    };
  }

  _getStoreUserDetail = async () => {
    try {
      const value = await AsyncStorage.getItem('user_id');
      if (value !== null) {
        // We have data!!
        // alert(value);
      }
    } catch (error) {
      // Error retrieving data
    }
  }

  SignIn = () =>{
    // console.warn(" "+this.state.email+" "+this.state.password);


   // const {name}  = this.state ;
   // const {mobile} = this.state ;
   // const {email} = this.state ;
   // const {password} = this.state ;
   // const {confirmpassword} = this.state ;

   //if( this.state.password === this.state.confirmpassword )
   //{   
    if (this.state.email === undefined || this.state.password  === undefined )  {
      
      this.setState({
        errors: 'Required'
      });

    } else if (this.state.email.trim() === '' || this.state.password.trim() === '') {
      this.setState({
        errors: 'Must not be blank'
      });
      
    } else if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.state.email)){
      fetch(BASE_URL + 'Rss/do_login', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email: this.state.email,
          password: this.state.password
         })
        
      }).then((response) => response.json())
      .then((responseJson) => {
      
       // Showing response message coming from server after inserting records.
       // alert(JSON.stringify(responseJson))
       // this.setState({
       //   success: responseJson,
       //   errors: responseJson.message
       // })
       if ( responseJson.status ){
        this.setState({user_id:responseJson.detail.id,token:responseJson.detail.token},()=>this._storeUserDetail())
       }else{
        alert(responseJson.message)
       }
       // console.warn(this.state.success);
      
       }).catch((error) => {
        this.setState({
          errors: error
          
        })
         //console.warn(this.state.error);
       });
             

    }else {
      this.setState({
        errors: 'Invalid Email Address or Password'
      });  
    }


    //
   
    }
 


  render() {
    return (
      <View style={styles.container}>
      <ImageBackground style={styles.bg}  source={require('./img/splash_bg.jpg')}>
      <Image source={require('./img/logo.png')}/>
      
      <KeyboardAvoidingView behavior="padding " enabled>

      <Text style={styles.title}>SIGN <Text style={{color:"red"}}>IN</Text></Text>      
      <Text style={{color:'red',textAlign:'center'}}>{this.state.errors}</Text>

      <View style={styles.w100container}>
       <View style={styles.inputStyle}><Image style={styles.w10} source={require('./img/signup/mail.png')}/><TextInput onChangeText={email => this.setState({email})}
style={styles.inputText} placeholder={'Email'} /></View>
       <View style={styles.inputStyle}><Image style={styles.w10} source={require('./img/signup/key.png')}/><TextInput onChangeText={password => this.setState({password})}
 style={styles.inputText} secureTextEntry={true} placeholder={'Password'} /></View>
       <TouchableOpacity onPress={this.SignIn} style={styles.submitbtn} >
          <Text style={styles.submit}>SIGN IN</Text>
       </TouchableOpacity>
       <TouchableOpacity onPress={this.gotoNextActivity} style={styles.submitbtn} >
          <Text style={styles.submit}>FORGOT PASSWORD ?</Text>
       </TouchableOpacity>
       </View>

       </KeyboardAvoidingView>
      </ImageBackground>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {flex: 1,flexDirection:'column',justifyContent:'center',alignItems:'center'},
  bg: {height:'100%',width:'100%',position:'absolute',resizeMode:'contain',justifyContent:'center',alignItems:'center',},
  title: {fontFamily: 'Oswald-Regular',fontSize:24,color:'black',textAlign:'center',marginVertical:10},
  w100container: {width:'100%',justifyContent:'center',alignItems:'center'},
  inputStyle: {flexDirection:'row',alignItems:'center',borderWidth:1,borderRadius:5,borderColor:'#7f8c8d',backgroundColor:'white',marginVertical:8,paddingLeft:5},
  inputText: {height:50,width: "80%",fontSize:18},
  w10: {width:"10%"},
  submitbtn: {width:'90%',flexDirection:'row',borderRadius:5,marginTop:15,paddingVertical:10,backgroundColor:'red'},
  submit: {width: "100%",fontSize:19,color:'white',textAlign:'center'},
 
 });
 