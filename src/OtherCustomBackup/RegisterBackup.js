import React, { Component } from 'react';
import {
  View,
  Text,AsyncStorage,Image,ImageBackground,Button,TextInput
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

 export default class Register extends React.Component {
  static navigationOptions =
  {
     drawerLabel: null,
     header : null,
  };
  
  constructor(props){
     super(props);
     //const userToken = AsyncStorage.getItem('userToken');
     //this.gotoNextActivity();
   }

   

   
  // Fetch the token from storage then navigate to our appropriate place
  gotoNextActivity = () =>
  {

    //console.warn(userToken);s

     //this.props.navigation.navigate('StackViaTab');
     //this.props.navigation.navigate('Category');

    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    this.props.navigation.navigate('Login');
  }

  // componentWillMount(){
  //   setTimeout(()=> {
  //     this.props.navigation.navigate('App')
  //   },1000);
  // }
  render() {
    return (
      <View style={{ flex: 1}}>
      <ImageBackground style={{flex:1,paddingLeft:10,paddingRight:10,}} source={require('./img/splash_bg.jpg')}>
      <Text style={{fontFamily: 'Oswald-Regular',fontSize:24,color:'black',textAlign:'center',marginVertical:10}}>Register</Text>
      <Text style={{fontFamily: 'Oswald-Regular',fontSize:21,color:'#34495e',textAlign:'center'}}>Create your new account. It's Free </Text>
       <TextInput style={{width: "100%",backgroundColor:'white',marginVertical:5,fontFamily: 'Oswald-Regular',fontWeight:"200",fontSize:18,borderWidth:1,borderRadius:5,borderColor:'#7f8c8d'}} placeholder={'Full Name'} />
       <TextInput style={{width: "100%",backgroundColor:'white',marginVertical:5,fontFamily: 'Oswald-Regular',fontWeight:"200",fontSize:18,borderWidth:1,borderRadius:5,borderColor:'#7f8c8d'}} placeholder={'Mobile No'} />
       <TextInput style={{width: "100%",backgroundColor:'white',marginVertical:5,fontFamily: 'Oswald-Regular',fontWeight:"200",fontSize:18,borderWidth:1,borderRadius:5,borderColor:'#7f8c8d'}} placeholder={'Email'} />
       <TextInput style={{width: "100%",backgroundColor:'white',marginVertical:5,fontFamily: 'Oswald-Regular',fontWeight:"200",fontSize:18,borderWidth:1,borderRadius:5,borderColor:'#7f8c8d'}} secureTextEntry={true} placeholder={'Password'} />
       <TextInput style={{width: "100%",backgroundColor:'white',marginVertical:5,fontFamily: 'Oswald-Regular',fontWeight:"200",fontSize:18,borderWidth:1,borderRadius:5,borderColor:'#7f8c8d'}} secureTextEntry={true} placeholder={'Confirm Password'} />
       <TouchableOpacity onPress={this.gotoNextActivity} style={{marginTop:15,paddingVertical:10,backgroundColor:'#2ecc71'}} >
         <Text style={{fontSize:19,color:'white',textAlign:'center',fontFamily: 'Oswald-Regular'}}>SUBMIT</Text>
       </TouchableOpacity>
       </ImageBackground>
      </View>
    );
  }
}


