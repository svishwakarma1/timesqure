import React, { Component } from 'react';
import {
  View,
  Text,
  Dimensions,ViewPagerAndroid,
  ScrollView,FlatList,TouchableOpacity,StyleSheet
} from 'react-native';

 const data = new Array(5).fill(0);
 let scrollXPos = 0;

 class Videos extends React.Component {
   constructor(props){
     super(props);
     this.state = {
       data:data,
       selected: 0,
       dragBeg:0,
       fselected: null,
       screenHeight: Dimensions.get('window').height,      
       screenWidth: Dimensions.get('window').width,
     }
   }
   renderItem = ({ index }) => {
    return (

      <View>
        <TouchableOpacity style={this.state.selected === index ?  
      styles.btselected : styles.btnotselected}
                      onPress={this.scrollTo.bind(this,index)}>
            <Text style={this.state.selected === index ?  
      styles.selected : styles.notselected}>Scroll to {index}</Text> 
         </TouchableOpacity>
      </View>
    );
  };
  scrollTo = (key) => {
    //console.warn(this.state.screenWidth);
    //console.warn(key);
      scrollXPos = this.state.screenWidth * key;
      this.scroller.scrollTo({x: scrollXPos, y: 0});
       this.setState({
        selected: key
       });
      console.warn(this.state.selected);
  };

  fscrollTo = (index) => {
    //working
    this.setState({
      selected: index,
      fselected:index
     });
     
     console.warn(this.state.selected);
     console.warn(this.state.fselected);

    //Data type: This is how it looks "scrollToIndex((params: object));"
    
    this.fscroller.scrollToIndex({animated: true, index: index});
    
    this.Buttonfscroller.scrollToIndex({animated: true, index: index});

  }

  ScrollBeg(){
    console.warn(this.state.dragBeg);
    this.setState({
      dragBeg: this.state.dragBeg + 1
    })

  }



  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        {/* <FlatList
          data={data}
          renderItem={this.renderItem}
          contentContainerStyle={{ padding: 10 }}
        /> */}
        <Text>Well Hello Internet</Text>
        {/* <ScrollView
         showsHorizontalScrollIndicator={true}
         pagingEnabled={true}
         ref={(scroller) => {this.scroller = scroller}}
         horizontal={true}
         >
            {
              this.state.data.map((item,key)=>{
                return(
                <View>
                  <TouchableOpacity style={{paddingVertical:20,width:130,borderBottomWidth:2,borderBottomColor:'green'}}
                      onPress={this.scrollTo.bind(this,key)}>
                     <Text>Scroll to {key}</Text> 
                 </TouchableOpacity>
              </View>)
              })
            }
        </ScrollView> */}

        <FlatList
          data={data}
          extraData={this.state}
          horizontal={true}
          contentContainerStyle={{ padding: 10 }}
          ref={(Buttonfscroller) => {this.Buttonfscroller = Buttonfscroller}}
          renderItem={({index})=> 
          <View>
            <TouchableOpacity style={this.state.selected === index ?  
          styles.btselected : styles.btnotselected}
                          onPress={this.fscrollTo.bind(this,index)}>
                <Text style={this.state.selected === index ?  
          styles.selected : styles.notselected}>Scroll to {index}</Text> 
             </TouchableOpacity>
          </View>
          }
        />

        <FlatList
          data={data}
          extraData={this.state.fselected}
          horizontal={true}
          pagingEnabled={true}
          ref={(fscroller) => {this.fscroller = fscroller}}
          renderItem={({index}) => 
          
          <View style={{ height: 200, width: this.state.screenWidth,borderWidth:1 }}>
          <Text style={{ textAlign: 'center' }} onPress={this.fscrollTo.bind(this,index)} >Item {index}</Text>
          </View>
          
          }
        />

        {/*<ScrollView
         showsVerticalScrollIndicator={true}
         pagingEnabled={true}
         ref={(scroller) => {this.scroller = scroller}}
         horizontal={true}
         >
          {  
            this.state.data.map((item,key)=>{
             return( 
             <View style={{ height: 200, width: this.state.screenWidth,borderWidth:1 }}>
                <Text style={{ textAlign: 'center' }}>Item {key}</Text>
              </View>
              );
            })
          }
         </ScrollView> */}
        
      </View>

    );
  }
}

const styles = StyleSheet.create({
  selected: {
    color:'red',
  },
  notselected: {
  },
  btselected: {
    paddingVertical:20,
    width:130,
    borderBottomWidth:2,
    borderBottomColor:'red'
  },
  btnotselected: {
    paddingVertical:20,
    width:130,
    borderBottomWidth:2,
    borderBottomColor:'white'
  }
})

export default Videos;