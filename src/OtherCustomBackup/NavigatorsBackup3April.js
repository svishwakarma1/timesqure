/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, {Component} from 'react';
import { FlatList,Dimensions, Platform, ActivityIndicator, StyleSheet, Text, View  } from 'react-native';
import Icon  from 'react-native-vector-icons/Ionicons';

//Navigators
import { createMaterialTopTabNavigator , createBottomTabNavigator, createDrawerNavigator, createStackNavigator, createAppContainer } from 'react-navigation';

//Top Tab Navigators
import Latest from './TopTabNav/Latest.js';
import cTest from './TopTabNav/Movies.js';
import TopStories from './TopTabNav/TopStories.js';
import Trending from './TopTabNav/Trending.js';
import World from './TopTabNav/World.js';

//Drawer
import Plain from './Plain.js';

//Bottom Tab Navigators
import Home from './BottomTabNav/Home.js';
import Photos from './BottomTabNav/Photos.js';
import Profile from './BottomTabNav/Profile.js';
import TVShow from './BottomTabNav/TVShow.js';
import Videos from './BottomTabNav/Videos.js';




  // initialLayout it is used to avoid one frame delay
  const initialLayout = {
    height: 0,
    width: Dimensions.get('window').width,
  };

  const Test = {
    Latest: Latest,
    TopStories: TopStories,
    Movies: cTest,
    Trending: Trending,
    World: World,
  };



    const TopTabNavigator = createMaterialTopTabNavigator(Test,
    {
      tabBarVisible: false,
      initialLayout: initialLayout,
      tabBarOptions: {
        activeTintColor: '#f22613',
        inactiveTintColor: 'grey',
        scrollEnabled : true,
        indicatorStyle : {
          backgroundColor: '#f22613',
        },
        tabStyle: { 
          height: 30,
          width: 80,
          margin: 0,
          padding: 0,
        },
        labelStyle:{
          margin: 0,
          padding: 0,
        },
        style: {
          backgroundColor: '#EBEBEB',
          fontSize:11,
        }
      },
    });
  
  
   export const BottomTabNavigator = createBottomTabNavigator({
    VIDEOS: Videos,
    TVSHOW: TVShow,
    HOME: TopTabNavigator,
    PHOTOS: Photos,
    PROFILE: Profile,
  },
  {  
    initialRouteName: 'PHOTOS',
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        
        const { routeName } = navigation.state;
        let IconComponent = Icon;
        let iconName;
        if (routeName === 'VIDEOS') {
          return <IconComponent  name={'logo-youtube'} size={25} color={focused ? '#f22613' :'#fff'} />;
          //iconName = `ios-information-circle${focused ? '' : '-outline'}`;
          // Sometimes we want to add badges to some icons. 
          // You can check the implementation below.
          //IconComponent = HomeIconWithBadge; 
        } else if (routeName === 'TVSHOW') {
          return <IconComponent  name={'md-tv'} size={25} color={focused ? '#f22613' :'#fff'} />;
  
        } else if (routeName === 'HOME') {
          return <IconComponent  name={'md-home'} size={25} color={focused ? '#f22613' :'#fff'} />;
  
        } else if (routeName === 'PHOTOS') {
          return <IconComponent  name={'md-star-outline'} size={25} color={focused ? '#f22613' :'#fff'} />;
  
        } else if (routeName === 'PROFILE') {
          return <IconComponent  name={'md-notifications'} size={25} color={focused ? '#f22613' :'#fff'} />;
  
        }
      },
    }),
    tabBarOptions: {
      activeTintColor: '#f22613',
      inactiveTintColor: 'white',
      style: {
        backgroundColor: 'black'
      }
    },
  
  });
  
  //export const AppTopTabContainer = createAppContainer(TopTabNavigator);
  //export const AppBottomTabContainer = createAppContainer(BottomTabNavigator);

