  <ScrollView contentContainerStyle={styles.contentContainer}>

         <View style={styles.subcontainer} >
        
          <Text style={styles.textView}>Pulwama attack: Imran Khan asks India to 'give peace a chance' after PM Modi's dare</Text>
        
         <View style={{flexDirection:"row", }}>
           <Text onPress={this.followingPage} style={{ fontSize: 13,color: 'black',textAlign: 'left', flex:1, alignSelf: 'flex-start',marginTop: 5}}>One India {"\n"}<Text style={{fontSize: 9, color: 'grey'}}> 816k Followers </Text></Text>

           <TouchableOpacity onPress={this.toggleFollow} style={{padding:8}}>
           <Text style={{fontSize: 12,borderWidth:1,paddingVertical:2,paddingHorizontal:5,borderColor: '#1DA1F2',borderRadius: 5 ,color: '#1DA1F2'}}>
           <IconComponent  name={'ios-star-outline'} size={14} color={'#1DA1F2'} />
           {' '}Follow</Text>
           </TouchableOpacity>
         </View>
          
         </View>

          <Image style={styles.imageView} source={require('./img/static/news1.jpg')}   />
          
          <View style={styles.subcontainer} >
           <Text style={{ fontSize: 11,textAlign: 'left', flex:1, alignSelf: 'flex-start',paddingVertical: 10}}>23 Feb 2019 10:31 PM</Text>
           <Text style={{ fontSize: 14,color:'black',letterSpacing:0.2}}>
           <Text style={{fontWeight:'bold'}}>Islamabad/New Delhi</Text>
           : Just a day after Prime Minister Narendra Modi challenged Pakistan Prime Minister to “stand by” his words; Imran Khan in a reply evoked John Lennon asking Modi to “give peace a chance”. Khan assured Modi that he intends to stand by his words and reiterated that Islamabad will “immediately act” if New Delhi provides “actionable evidence” on Pulwama attack.
           {'\n\n'}On Saturday, Modi said Imran Khan told him that he is the son of a ‘Pathan’ (a member of Pashto-speaking people originally from North-West Pakistan known for their deep sense of honour), and would keep good on his promises when the former wished him upon being elected as Pakistan’s premier. Now is the time to act with honour, “if he indeed was a Pathan’s son” Modi had said during a public meeting in Rajasthan’s Tonk.          
           {'\n\n'}At Tonk, Modi said during the congratulatory call, he asked Khan that together they should work to fight poverty and illiteracy.  
           {'\n\n'}"PM Imran Khan stands by his words that if India gives us actionable intelligence, we will immediately act," a statement issued by Pakistan’s Prime Minister’s Office (PMO’s), as per a PTI report. “PM Modi should give peace a chance,” the statement said. 
           </Text>

           <Image style={styles.imageView} source={require('./img/static/ads1.jpg')}   />

           <Text style={{ fontSize: 14,color:'black',letterSpacing:0.2}}>
           {'\n\n'}A 19-year-old Jaish-e-Mohammad (JeM) terrorist drove an explosive-laden SUV into a bus plying on with the 78-vehicle Central Reserve Police Force (CRPF) convoy on Jammu-Srinagar highway in Pulwama on February 14, killing 40 CRPF jawans in the bus. 
           {'\n\n'}After the attack, India's Ministry of External Affairs (MEA) blamed Pakistan for harbouring and nurturing JeM and other terrorist organisations that target India with impunity. 
           </Text>

          </View>

        </ScrollView> 
        
        <View style={{flexDirection:'column',alignItems:'flex-end',borderTopWidth:1,borderTopColor:'#d9e1e3',backgroundColor:'#ecf0f1',paddingHorizontal:15}}>
            
            <View style={{height:60,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>

            <TextInput style={{height:40,flex:7,borderWidth:1,borderColor:'#d9e1e3',borderRadius:30,backgroundColor:'white',padding:5}} placeholder={' Add a comment'}></TextInput>
              
              <TouchableOpacity style={{flex:1,paddingVertical:5,paddingHorizontal:15}}>
              <Icon name={'md-chatboxes'} size={21} color={'#2980b9'} />
              <Text style={{fontSize:12,textAlign:'center'}}>1</Text>
              </TouchableOpacity>

              <TouchableOpacity style={{flex:1,paddingVertical:5,paddingHorizontal:15}}>
              <Icon name={'md-thumbs-up'} size={21} color={'#c0392b'} />
              <Text style={{fontSize:12,textAlign:'center'}}>1</Text>
              </TouchableOpacity>

              <TouchableOpacity onPress={this.link} style={{flex:1,paddingVertical:5,paddingHorizontal:15}}>
              <Icon name={'logo-whatsapp'} size={21} color={'#25d366'} />
              <Text style={{fontSize:12,textAlign:'center'}}>1</Text>

              </TouchableOpacity>

            </View>

          </View>

          <Toast visible={this.state.visible} message="Example" />
          </React.Fragment>