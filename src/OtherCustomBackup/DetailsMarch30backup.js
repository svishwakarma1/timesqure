import React, { Component } from 'react';
import {
  View,
  Text,
  ActivityIndicator, 
  StyleSheet,FlatList,TextInput,KeyboardAvoidingView,ToastAndroid,Image,Share,Linking,Platform,ScrollView,TouchableHighlight,TouchableOpacity,SafeAreaView
} from 'react-native';

import {Container,Footer,Header,Left,Right,Button, Content, Body} from "native-base";

import Icon  from 'react-native-vector-icons/Ionicons';

// a component that calls the imperative ToastAndroid API
const Toast = (props) => {
  if (props.visible) {
    ToastAndroid.showWithGravity(
      props.message,
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
      25,
      50,
    );
    return null;
  }
  return null;
};

const data = new Array(5).fill(0);


 class Details extends React.Component {

  static navigationOptions ={
    header:null
  }
  
  constructor(props) {
    super(props);

    this.state = {
      rss: [],
      followingMessage: "",
      followingStatus: 0,
      visible: false,
      isFetching: false,
    };
    this.setRss = this.setRss.bind(this);
  }

  followingPage = () => {
    this.props.navigation.navigate('followingPage');
  }
  componentDidMount(){
    //this.loadRss();
  }

  handleButtonPress = () => {
    this.setState(
      {
        visible: true,
      },
      () => {
        this.hideToast();
      },
    );
  };

  hideToast = () => {
    this.setState({
      visible: false,
    });
  };

  loadRss(){
    const parseUrl = 'https://api.rss2json.com/v1/api.json?rss_url=';
    const rssUrl = 'https://timesofindia.indiatimes.com/rssfeeds/1945062111.cms';
    fetch(parseUrl + rssUrl)
    .then(response => response.json())
    .then((json) => {
      if (json.status === 'ok') {
        this.setRss(json);
        console.warn(json);
      }else {
        console.log("failed");
      }
    });
  }

  toggleFollow(){

    fetch('http://limrademo.com/timesqure/timesqure_api_ver_01/Follow/followingNews', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        channel_id: 46,
        user_id: 12,
      }),
    })
    .then(response => response.json())
    .then((json) => {
      //console.warn(json.Message);
      this.setState({
        followingMessage: json.Message,
        visible: !this.state.visible,
        followingStatus: json.Status 
      });
      
    })
    .catch(function (error) {
      console.warn(error);
    });

  }

  setRss(json) {
    console.warn(json.items);
    this.setState({
      rss: json.items,
      isFetching: false 
    });
  }

  link(){
  // Whatsapp Opening external link. Done!
  const url= "whatsapp://send?text=Hello%2C%20World!";
  Linking.canOpenURL(url)
  .then((supported) => {
    if (!supported) {
      console.log("Can't handle url: " + url);
      console.warn("Can't handle url: " + url);

    } else {
      return Linking.openURL(url);
    }
  })
  .catch((err) => {
  console.error('An error occurred', err); 
  console.warn('An error occurred', err)});
  }

  async onShare(){
    //Sharing Content with all apps Done!
    try {
      const result = await Share.share({
        message:
          'React Native | A framework for building native apps using React',
      })

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };


  render() {
    let IconComponent = Icon;
    const { navigate } = this.props.navigation;

    // const rssList = this.state.rss.map((item) => {
    //   return (
    //     <View>
    //       <Text style={styles.textView}>{item.title}</Text>
    //       <View style={{flexDirection:"row", }}>
    //        <Text onPress={this.followingPage} style={{ fontSize: 13,color: 'black',textAlign: 'left', flex:1, alignSelf: 'flex-start',marginTop: 5}}>Times of India {"\n"}<Text style={{fontSize: 9, color: 'grey'}}> 816k Followers </Text></Text>

    //        <TouchableHighlight style={{padding:8}}>
    //        <Text style={{fontSize: 12,borderWidth:1,paddingVertical:2,paddingHorizontal:5,borderColor: '#1DA1F2',borderRadius: 5 ,color: '#1DA1F2'}}>
    //        <IconComponent  name={'ios-star-outline'} size={14} color={'#1DA1F2'} />
    //        {' '}Follow</Text>
    //        </TouchableHighlight>
    //      </View>
    //      <Image style={styles.imageView} source={{uri:item.thumbnail}}   />
    //      <Text>{item.description}</Text>

    //     </View>
    //   )
    // })

    return (
      <View style={{flex:1}}>

        {/* <ScrollView>
        {rssList}
        </ScrollView> */}

      <ScrollView  horizontal pagingEnableds>


        {data.map((item,key)=>{

         return <React.Fragment key={key}> 

         <View style={{flex:1,flexDirection:'row',borderWidth:1}}>

         </View>


        <Header androidStatusBarColor={'grey'} style={{backgroundColor:'white',paddingLeft:15,paddingRight:15}}>
          <Left><Text><IconComponent name={'md-arrow-back'} size={25} color={'grey'}/></Text></Left>
          <Right><Text><IconComponent name={'md-more'} size={25} color={'grey'}/></Text></Right>
        </Header>


        <ScrollView contentContainerStyle={styles.contentContainer}>

         <View style={styles.subcontainer} >
        
          <Text style={styles.textView}>Pulwama attack: Imran Khan asks India to 'give peace a chance' after PM Modi's dare</Text>
        
         <View style={{flexDirection:"row", }}>
           <Text onPress={this.followingPage} style={{ fontSize: 13,color: 'black',textAlign: 'left', flex:1, alignSelf: 'flex-start',marginTop: 5}}>One India {"\n"}<Text style={{fontSize: 9, color: 'grey'}}> 816k Followers </Text></Text>

           <TouchableOpacity onPress={this.toggleFollow} style={{padding:8}}>
           <Text style={{fontSize: 12,borderWidth:1,paddingVertical:2,paddingHorizontal:5,borderColor: '#1DA1F2',borderRadius: 5 ,color: '#1DA1F2'}}>
           <IconComponent  name={'ios-star-outline'} size={14} color={'#1DA1F2'} />
           {' '}Follow</Text>
           </TouchableOpacity>
         </View>
          
         </View>

          <Image style={styles.imageView} source={require('./img/static/news1.jpg')}   />
          
          <View style={styles.subcontainer} >
           <Text style={{ fontSize: 11,textAlign: 'left', flex:1, alignSelf: 'flex-start',paddingVertical: 10}}>23 Feb 2019 10:31 PM</Text>
           <Text style={{ fontSize: 14,color:'black',letterSpacing:0.2}}>
           <Text style={{fontWeight:'bold'}}>Islamabad/New Delhi</Text>
           : Just a day after Prime Minister Narendra Modi challenged Pakistan Prime Minister to “stand by” his words; Imran Khan in a reply evoked John Lennon asking Modi to “give peace a chance”. Khan assured Modi that he intends to stand by his words and reiterated that Islamabad will “immediately act” if New Delhi provides “actionable evidence” on Pulwama attack.
           {'\n\n'}On Saturday, Modi said Imran Khan told him that he is the son of a ‘Pathan’ (a member of Pashto-speaking people originally from North-West Pakistan known for their deep sense of honour), and would keep good on his promises when the former wished him upon being elected as Pakistan’s premier. Now is the time to act with honour, “if he indeed was a Pathan’s son” Modi had said during a public meeting in Rajasthan’s Tonk.          
           {'\n\n'}At Tonk, Modi said during the congratulatory call, he asked Khan that together they should work to fight poverty and illiteracy.  
           {'\n\n'}"PM Imran Khan stands by his words that if India gives us actionable intelligence, we will immediately act," a statement issued by Pakistan’s Prime Minister’s Office (PMO’s), as per a PTI report. “PM Modi should give peace a chance,” the statement said. 
           </Text>

           <Image style={styles.imageView} source={require('./img/static/ads1.jpg')}   />

           <Text style={{ fontSize: 14,color:'black',letterSpacing:0.2}}>
           {'\n\n'}A 19-year-old Jaish-e-Mohammad (JeM) terrorist drove an explosive-laden SUV into a bus plying on with the 78-vehicle Central Reserve Police Force (CRPF) convoy on Jammu-Srinagar highway in Pulwama on February 14, killing 40 CRPF jawans in the bus. 
           {'\n\n'}After the attack, India's Ministry of External Affairs (MEA) blamed Pakistan for harbouring and nurturing JeM and other terrorist organisations that target India with impunity. 
           </Text>

          </View>

        </ScrollView> 
        
        <View style={{flexDirection:'column',alignItems:'flex-end',borderTopWidth:1,borderTopColor:'#d9e1e3',backgroundColor:'#ecf0f1',paddingHorizontal:15}}>
            
            <View style={{height:60,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>

            <TextInput style={{height:40,flex:7,borderWidth:1,borderColor:'#d9e1e3',borderRadius:30,backgroundColor:'white',padding:5}} placeholder={' Add a comment'}></TextInput>
              
              <TouchableOpacity style={{flex:1,paddingVertical:5,paddingHorizontal:15}}>
              <Icon name={'md-chatboxes'} size={21} color={'#2980b9'} />
              <Text style={{fontSize:12,textAlign:'center'}}>1</Text>
              </TouchableOpacity>

              <TouchableOpacity style={{flex:1,paddingVertical:5,paddingHorizontal:15}}>
              <Icon name={'md-thumbs-up'} size={21} color={'#c0392b'} />
              <Text style={{fontSize:12,textAlign:'center'}}>1</Text>
              </TouchableOpacity>

              <TouchableOpacity onPress={this.link} style={{flex:1,paddingVertical:5,paddingHorizontal:15}}>
              <Icon name={'logo-whatsapp'} size={21} color={'#25d366'} />
              <Text style={{fontSize:12,textAlign:'center'}}>1</Text>

              </TouchableOpacity>

            </View>

          </View>

          <Toast visible={this.state.visible} message="Example" />
          </React.Fragment>
          })}
          </ScrollView>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    
  },
  subcontainer:{
    paddingHorizontal:15
  },
  imageView:{
    height: 200,
    width: '100%',
    marginTop: 15,
  },
  textView: {
    fontSize: 19,
    color: 'black',
    fontWeight: 'bold'
  },
  contentContainer: {
    paddingVertical: 20
  },
  textBold:{
    fontWeight: 'bold'
  }
});
export default Details;


/**
 *  <SafeAreaView style={{flex:1}}>

          <View style={{height:250,bottom:10,flexDirection:'row',alignItems:'center',borderTopWidth:1,borderTopColor:'#d9e1e3',backgroundColor:'#ecf0f1',paddingHorizontal:15,}}>

                <TextInput style={{borderWidth:1,borderColor:'#d9e1e3',borderRadius:30,backgroundColor:'white',width:"58%",padding:5}} placeholder={' Add a comment'}></TextInput>
               
                <TouchableOpacity style={{paddingVertical:5,paddingHorizontal:15}}>
                <Icon name={'md-chatboxes'} size={21} color={'#2980b9'} />
                <Text style={{fontSize:12,textAlign:'center'}}>1</Text>
                </TouchableOpacity>

                <TouchableOpacity style={{paddingVertical:5,paddingHorizontal:15}}>
                <Icon name={'md-thumbs-up'} size={21} color={'#c0392b'} />
                <Text style={{fontSize:12,textAlign:'center'}}>1</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={this.link} style={{paddingVertical:5,paddingHorizontal:15}}>
                <Icon name={'logo-whatsapp'} size={21} color={'#25d366'} />
                <Text style={{fontSize:12,textAlign:'center'}}>1</Text>
                </TouchableOpacity>

          </View>
          <View style={{ height: 70 }} />
          </SafeAreaView>
 */
