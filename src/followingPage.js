import React, { Component } from 'react';
import {
  View,
  Text,
  ActivityIndicator, 
  StyleSheet,NetInfo,FlatList,Image,ImageBackground,Platform,ScrollView,TouchableHighlight,TouchableOpacity
} from 'react-native';

import Icon  from 'react-native-vector-icons/Ionicons';
import ScrollableTabView, { ScrollableTabBar } from 'react-native-scrollable-tab-view';
import axios from 'axios';

import { BASE_URL } from './services/Api_constant';

//import { Container} from 'native-base';
//import  Container from './Test/Container.js';
//import ChannelHeader from './Components/ChannelHeader.js';
const data = new Array(5).fill(0);


/** Very Important Findings on Objects.
 *  for(..in)loop highly customizable its alternate is the Object Api in MDN.
 *  Suppose data is some thing like 
 *   
 *  const Test = [{"1":"Cyrus","2":"Genghis Khan","3":"Alexander"}];
 *    const jsonValues = Object.values(Test);
 *    const jsonKeys   = Object.keys(Test);
 */


 class followingPage extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      rss: [],
      isLoading: true,
      headerrss:[],
      selected:'',
      channel_id: this.props.navigation.getParam('channel_id'),
      category_id: this.props.navigation.getParam('category_id'),
      isFetching: true,
    };
    //this.setRss = this.setRss.bind(this);
  }

  static navigationOptions =
  {
    headerTransparent: true,
  };

  componentDidMount() {
   this.loadHeaderRss();
  }



  Details = () =>{
    this.props.navigation.navigate('Details');
  }


  loadHeaderRss(){
      fetch(BASE_URL+'Rss/channeldetails', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          channel_id: this.state.channel_id,
        }),
      })
      .then(response => response.json())
      .then((json) => {
        console.warn(json);
        this.setState({
          headerrss: json.categories,
          isFetching: false,
          isLoading: false 
        });
      })
      .catch((error) =>{
        console.warn(error);
      });
    }
    

  



  followingPage(){
   this.props.navigation.navigate('followingPage');
   
  }
  /**
   *  Problem : Required Scrollable iterative (loop) based Tab.
   *  <View> tag was not allowing to scroll the react-native-scrollview-tab as it freezes the Tab.
   *  After Researching a lot there were third party plugins but for performance and flexibility reasons decided to simultaneously develope.
   *  so finished, 90% of React Native Custom Development using ScrollView and Flatlist almost achieved it but the animation and indicator part was left.  
   *  For Time Being Ali sir suggested to use react-native-scrollview-tab and native base <Container> it fixed the Problem.
   *  As the Internet Community says the more plugin the more heavy apps will be which ultimately affects the performance.
   *  But the question is Why nativebase replaced? <View> tag with <Container> tag and the difference between them and viceversa.
   *  Error was reporting something related to Adjacent JSX Fragment. Seeking something related to <View> tag. After Analysing, Studying nativebase container method when implementing it again freezed.
   *  After Research tried this <React.Fragment> tag based on anlaysing and studying a plugin module called react-native-paper.
   *  They used so this solved the problem <React.Fragment> .
   *  https://reactjs.org/blog/2017/11/28/react-v16.2.0-fragment-support.html
   *  This above article is very important also it has information about return[] (array) and return()
   */

  /**
   * Api's given by Harpreet
   * http://limrademo.com/timesqure/timesqure_api_ver_01/RssSample/SampleApi/oneIndia
   * http://limrademo.com/timesqure/timesqure_api_ver_01/RssSample/channeldetails/46
   * http://limrademo.com/timesqure/timesqure_api_ver_01/RssSample/news
   * channel_id:46
   * category_id:10
   * http://limrademo.com/timesqure/timesqure_api_ver_01/Follow/followingNews
   * "channel_id": 89,
   * "user_id": 12
   */


  categorytab(){
    let IconComponent = Icon;
    
    return this.state.headerrss.map((item,key) => {
      return <View key={item.category_id} tabLabel={item.category_name}>
      {
        item.category_name == 'Entertainment'?
         this.state.headerrss.map((itemx,key) => {
           return <Text>{itemx.category_name}</Text>
          })
      :
        <FlatTabLoop category_id={item.category_id} channel_id={this.state.channel_id}/>
      }
       </View>
      })
  }

  render() {
    let IconComponent = Icon;
    const i = this.props.i;

    return (
      <React.Fragment>
      <ImageBackground source={require('./img/static/newsbg1.png')} style={{height: 180}}>
      <Image source={require('./img/static/hticon.png')} style={{height: 70, width: 70,marginVertical: 50, alignSelf:'center',}}/>
      </ImageBackground>
      <View style={{flexDirection:"row",margin:15 }}>
         <Text onPress={this.followingPage} style={{ fontSize: 18,color: 'black',textAlign: 'left', flex:1, alignSelf: 'flex-start',marginTop: 5}}>One India {"\n"}<Text style={{fontSize: 12, color: 'grey'}}> 816k Followers | 4.5k Stories  </Text></Text>

         <TouchableHighlight style={{padding:8}}>
         <Text style={{fontSize: 14,borderWidth:1,paddingVertical:2,paddingHorizontal:5,borderColor: '#1DA1F2',borderRadius: 5 ,color: '#1DA1F2'}}>
         <Icon  name={'ios-star-outline'} size={14} color={'#1DA1F2'} />
         {' '}Follow</Text>
         </TouchableHighlight>
       </View>


      <ScrollableTabView
        style={{marginTop: 5, }}
        initialPage={0}
        renderTabBar={() => <ScrollableTabBar />}>

        {this.categorytab()}

      </ScrollableTabView>
      </React.Fragment>
    );
  }
}


{/* 
  <View style={{ }}>
        <ImageBackground source={require('./img/static/newsbg1.png')} style={{height: 180}}>
        <Image source={require('./img/static/hticon.png')} style={{height: 70, width: 70,marginVertical: 50, alignSelf:'center',}}/>
        </ImageBackground>
        <View style={{flexDirection:"row",margin:15 }}>
           <Text onPress={this.followingPage} style={{ fontSize: 18,color: 'black',textAlign: 'left', flex:1, alignSelf: 'flex-start',marginTop: 5}}>Times of India {"\n"}<Text style={{fontSize: 12, color: 'grey'}}> 816k Followers | 4.5k Stories  </Text></Text>

           <TouchableHighlight style={{padding:8}}>
           <Text style={{fontSize: 14,borderWidth:1,paddingVertical:2,paddingHorizontal:5,borderColor: '#1DA1F2',borderRadius: 5 ,color: '#1DA1F2'}}>
           <Icon  name={'ios-star-outline'} size={14} color={'#1DA1F2'} />
           {' '}Follow</Text>
           </TouchableHighlight>
         </View>
  </View> 
*/}

export class FlatTabLoop extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      rss: [],
      isLoading: true,
      headerrss:[],
      selected:'',
      channel_id: 51,
      isFetching: false,
      Unavailable: 'Sorry,for the Inconvenience News Feed Unavailable!'
    };
    //this.setRss = this.setRss.bind(this);
  }

componentDidMount(){
 this.loadRss();

}

onRefresh() {
  this.setState({ isFetching: true }, function() { this.loadRss() });
}

loadRss(){
    fetch(BASE_URL+'Rss/news', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        channel_id: this.props.channel_id,
        category_id: this.props.category_id,
      }),
    })
    .then(response => response.json())
    .then((json) => {
      console.warn(json);
      this.setState({
        rss: json.Channel,
        isFetching: false,
        isLoading : false
      });
    })
    .catch(function (error) {
      console.warn(error);
    });
}

  render () {
    let IconComponent = Icon;

    return (
       this.state.isLoading ?
      <View style = { styles.MainContainer }>
      <ActivityIndicator size="large" color="grey" />
      </View>:
      this.state.rss != null ?

      <FlatList
      data={ this.state.rss }
      onRefresh={() => this.onRefresh()}
      refreshing={this.state.isFetching}
      renderItem={({item}) => 

          <View style={styles.container}>

          <TouchableOpacity style={{paddingBottom:15}} onPress={this.Details}>
          <View style={styles.subcontainer} >
          <Text style={styles.textView}>{item.title}</Text>
          <Image style={styles.imageView} source={{uri: item.image}}   />
          
          <View style={{flexDirection:"row", }}>
            <Text style={{ fontSize: 11,textAlign: 'left', flex:1, alignSelf: 'flex-start',  marginTop: 15}}>{item.pubDate}</Text>
            <IconComponent style={{textAlign: 'right',alignSelf: 'flex-end', marginRight:20, marginTop:5}}  name={'logo-whatsapp'} size={25} color={'#25d366'} />
            <IconComponent style={{textAlign: 'right',alignSelf: 'flex-end',  marginTop:5,transform: [{ rotate: '90deg'}] }}  name={'ios-more'} size={25} color={'grey'} />
          </View>
          </View>
          </TouchableOpacity>    
          
          </View>

        }
        keyExtractor={(item, index) => index.toString()}
      />:<Text style={styles.Unavailable}>{this.state.Unavailable}</Text> 
    )
  }
}






const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    marginTop:10

    
  },
  subcontainer:{
    paddingHorizontal: 15,
  },
  imageView:{
    height: 170,
    width: '100%',
    marginTop: 15,
    borderRadius: 5,
  },
  textView: {
    fontSize: 15,
    color: 'black',
    fontWeight: 'bold'
  },
  contentContainer: {
    paddingVertical: 20
  },
  textBold:{
    fontWeight: 'bold'
  },
  Unavailable:{fontSize:18,textAlign:'center',paddingTop:25}
});

export default followingPage;