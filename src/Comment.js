import React, { Component } from 'react'
import {
  View,
  Text,Image,
  StyleSheet,
  AsyncStorage,
  TouchableOpacity,TextInput
} from 'react-native'
import Icon  from 'react-native-vector-icons/Ionicons';
import { FlatList } from 'react-native-gesture-handler';

import { BASE_URL } from './services/Api_constant';



export default class Comment extends Component {
  static navigationOptions = {
    headerTitle: <Text style={{fontSize:18,color:'black'}}>Comments</Text>,
    headerRight: <Text style={{paddingRight:15}}><Icon name={'ios-information-circle-outline'} size={30}/></Text>
  }

  constructor(props){
    super(props);
    this.state = {
      loadComments: [],
      addComment: "",
      count: 0,
      user_id: null,
      isLoading: false,
      news_id:null
    }
  }

  componentDidMount(){
    // alert(JSON.stringify(this.props.navigation.state.params.item))
   this._getStoreUserDetail()
   
   this.setState({news_id:this.props.navigation.state.params.item.news_id},()=>this.loadComments())
  }

  _getStoreUserDetail = async () => {
    try {
      const value = await AsyncStorage.getItem('user_id');
      if (value !== null) {
        this.setState({user_id:value})
      }
    } catch (error) {
      alert('have issue while fetching user detail');
    }
  }

  componentDidUpdate(prevProps,prevState){
    if(this.state.count != prevState.count){
      this.loadComments();
    }
  }

  loadComments = () => {
    fetch(BASE_URL+'Rss/newsCommentList', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        news_id: this.state.news_id,
      }),
    })
    .then(response => response.json())
    .then((json) => {
      // console.warn(json.Comment_list);
      this.setState({
        loadComments: json.Comment_list,
        isLoading: false
      });
    })
    .catch(function (error) {
      console.warn(error);
    });
  }

  addComment = () => {
    const {addComment} = this.state;
    // console.warn(addComment);

    if(this.state.user_id == null){
      alert('Please Login');
    }else if(this.state.news_id == null){
      alert('Selected new detail not found');
    }else{
      fetch(BASE_URL+'Rss/newsComment', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          "news_id": this.state.news_id,
          "user_id": this.state.user_id,
          "comment": addComment,
        }),
      })
      .then(response => response.json())
      .then((json) => {
        console.warn(json);
        this.setState({
          count: this.state.count+1,
          addComment: ""
        })
      })
      .catch(function (error) {
        console.warn(error);
      });
    }
  }
        // <Text style={{textAlign: this.state.user_id == item.user_id ? 'right' : 'left'}}>{item.comment}</Text>

  render () {
    return (
      <View style={styles.container}>


    <FlatList
    data={this.state.loadComments}
    keyExtractor={(item, index) => item.id}
    renderItem={({item}) => 
     
    <View style={{flex:1,flexDirection:'row',alignItems:'center',margin:5}}>

       <View style={{flex:0.2,alignItems:'center',justifyContent:'center'}}>
        <Image style={{height:60,width:60,resizeMode:'center'}} source={{uri: 'https://www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png'}}/>
       </View>
       
       <View style={{flex:0.8}}>
        <Text style={{paddingVertical:5,fontWeight:'bold',textAlign: 'left'}}>User {item.user_name}</Text>
        <Text style={{textAlign: 'left'}}>{item.comment}</Text>

        <View style={{flex:1,flexDirection:'row',justifyContent:'space-between',alignItems:'center',paddingTop:10}}>
       
         <View style={{flex:1,flexDirection:'row',alignItems:'center'}}>
         <Text style={{fontSize:13}}>22m</Text>
         <Icon style={{paddingHorizontal:10,color:'#c0392b'}} name={'md-thumbs-up'} size={15} />
         </View>

         <Icon style={{paddingHorizontal:10}} name={'md-flag'} size={18} />
        </View>
      </View>
    
    </View>
      }
    />
    
     
     
      <View style={{flexDirection:'row',padding:10,alignItems:'flex-end',borderTopWidth:1,borderTopColor:'#d9e1e3',backgroundColor:'#ecf0f1'}}>

      <TextInput style={{backgroundColor:"white",height:40,flex:7,borderWidth:1,borderColor:'#d9e1e3',borderRadius:30,backgroundColor:'white',padding:5}} placeholder={' Add a comment'} 
        onChangeText={(addComment) => this.setState({addComment})}     
        value={this.state.addComment}/>            
      <TouchableOpacity onPress={() => this.addComment()} style={{flex:1,flexDirection:'row',justifyContent:'flex-end',alignItems:'center',paddingVertical:5,paddingHorizontal:5}}>
      <Text style={{transform: [{ rotate: '90deg' }] }}><Icon name={'md-navigate'} size={30} color={'#2980b9'} /></Text>
      </TouchableOpacity>

      </View>

      </View>
      )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'

  },
  text: {
    color: 'white',
    fontSize: 40,
    fontWeight: 'bold',
  },
  button: {
    position: 'absolute',
    top: 50,
    left: 0,
    width: 150,
    height: 50,
    backgroundColor: '#f39c12',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    color: 'white'
  }
})