import React, { Component } from 'react';
import {
  View,
  Text,
  ActivityIndicator, 
  StyleSheet,NetInfo,FlatList,Image,ImageBackground,Platform,ScrollView,TouchableHighlight,TouchableOpacity
} from 'react-native';

import Icon  from 'react-native-vector-icons/Ionicons';

 class Loop extends React.Component {

    constructor(props) {
        super(props);
    
        this.state = {
          rss: [],
          isLoading: true,
          headerrss:[],
          selected:'',
          channel_id: 46,
          category_id: 10,
          isFetching: false,
        };
        //this.setRss = this.setRss.bind(this);
      }

    componentDidMount(){
     this.loadRss();

    }

    loadRss(){
        fetch('http://limrademo.com/timesqure/timesqure_api_ver_01/RssSample/news', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            channel_id: this.state.channel_id,
            category_id: this.state.category_id,
          }),
        })
        .then(response => response.json())
        .then((json) => {
          console.warn(json.list[0].categoriesnews);
          this.setState({
            rss: json.list[0].categoriesnews,
            isFetching: false 
          });
        })
        .catch(function (error) {
          console.warn(error);
        });
    }


  render() {
    return (
        <FlatList
        data={ this.state.rss }
        onRefresh={() => this.onRefresh()}
        refreshing={this.state.isFetching}
        renderItem={({item}) => 
        
            <View style={styles.container}>

            <TouchableOpacity style={{paddingBottom:15}} onPress={this.Details}>
            <View style={styles.subcontainer} >
            <Text style={styles.textView}>{item.title}</Text>
            <Image style={styles.imageView} source={{uri: item.url}}   />
            
            <View style={{flexDirection:"row", }}>
              <Text style={{ fontSize: 11,textAlign: 'left', flex:1, alignSelf: 'flex-start',  marginTop: 15}}>{item.pubDate}</Text>
              <IconComponent style={{textAlign: 'right',alignSelf: 'flex-end', marginRight:20, marginTop:5}}  name={'logo-whatsapp'} size={25} color={'#25d366'} />
              <IconComponent style={{textAlign: 'right',alignSelf: 'flex-end',  marginTop:5,transform: [{ rotate: '90deg'}] }}  name={'ios-more'} size={25} color={'grey'} />
            </View>
            </View>
            </TouchableOpacity>    
            
            </View>
        
          }
          keyExtractor={(item, index) => index.toString()}
        />
    );
  }
}


export default Loop;