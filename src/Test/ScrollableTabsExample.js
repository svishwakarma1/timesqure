import React from 'react';
import {
  Text,
  View,
} from 'react-native';

import ScrollableTabView, { ScrollableTabBar } from 'react-native-scrollable-tab-view';

//const data = new Array(5).fill(0);


 class ScrollableTabsExample extends React.Component {
  render() {
    return (
        <ScrollableTabView
          style={{marginTop: 20, }}
          initialPage={0}
          renderTabBar={() => <ScrollableTabBar />}>
              
              <Text tabLabel={"Header 1"}>My Header 1</Text>
              <Text tabLabel={"Header 2"}>My Header 2 </Text>              
              <Text tabLabel={"Header 3"}>My Header 3</Text>
              <Text tabLabel={"Header 4"}>My Header 4</Text>

        </ScrollableTabView>
    );
  }
}


export default ScrollableTabsExample;