import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,Dimensions,StyleSheet,TextInput,
  ViewPagerAndroid
} from 'react-native';
import Icon  from 'react-native-vector-icons/Ionicons';

const data = new Array(5).fill(0);
let scrollXPos = 0;


  

 class Photos extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      data:data,
      selected: 0,
      fselected: null,
      screenHeight: Dimensions.get('window').height,      
      screenWidth: Dimensions.get('window').width,
    }}
    scrollTo = (key) => {
      //console.warn(this.state.screenWidth);
      //console.warn(key);
        scrollXPos = this.state.screenWidth * key;
        this.scroller.scrollTo({x: scrollXPos, y: 0});
         this.setState({
          selected: key
         });
        console.warn(this.state.selected);
    };
  
    fscrollTo = (index) => {
      this.setState({
        selected: index,
        fselected:index
       });
       console.warn(this.state.selected);
       console.warn(this.state.fselected);
  
      //Data type: This is how it looks "scrollToIndex((params: object));"
  
      this.fscroller.scrollToIndex({animated: true, index: index});
      this.Buttonfscroller.scrollToIndex({animated: true, index: index});
    }
  
   render() {
     
    return (
      <View style={{flex:1}}>
              
        <ViewPagerAndroid
         style={styles.viewPager}
         initialPage={0}>


         <View style={styles.pageStyle} key="1">
           <Text onPress={()=>this.props.navigation.navigate('Test')}>Test page</Text>
         </View>
         <View style={styles.pageStyle} key="2">
           <Text>Second page</Text>
         </View>
         <View style={styles.pageStyle} key="3">
           <Text>Third page</Text>
         </View>
         <View style={styles.pageStyle} key="2">
           <Text>Fourth page</Text>
         </View>
        
        </ViewPagerAndroid>
        </View>
    );
  }
}

var styles = StyleSheet.create({
  viewPager: {
    flex: 1
  },
  pageStyle: {
    alignItems: 'center',
    padding: 20,
  },

})

export default Photos;
          /*<ViewPagerAndroid
          style={styles.viewPager}
          initialPage={0}>

            <FlatList
              data={data}
              extraData={this.state}
              horizontal={true}
              contentContainerStyle={{ padding: 10 }}
              ref={(Buttonfscroller) => {this.Buttonfscroller = Buttonfscroller}}
              renderItem={({index})=> 
              <View style={styles.pageStyle}>
                <TouchableOpacity style={this.state.selected === index ?  
              styles.btselected : styles.btnotselected}
                              onPress={this.fscrollTo.bind(this,index)}>
                    <Text style={this.state.selected === index ?  
              styles.selected : styles.notselected}>Scroll to {index}</Text> 
                </TouchableOpacity>
              
                  <View >
                  <Text> Hello </Text>
                  </View>
            

              </View>
              }
            
            />
            </ViewPagerAndroid>
            */

  /* 
  
  This the comment like message whatsapp section
  
  <View style={{flexDirection:'column',alignItems:'flex-end',borderWidth:1}}>
  
  <View style={{height:60,borderWidth:1,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>

  <TextInput style={{height:40,flex:7,borderWidth:1,borderColor:'#d9e1e3',borderRadius:30,backgroundColor:'white',padding:5}} placeholder={' Add a comment'}></TextInput>
    
    <TouchableOpacity style={{flex:1,paddingVertical:5,paddingHorizontal:15}}>
    <Icon name={'md-chatboxes'} size={21} color={'#2980b9'} />
    <Text style={{fontSize:12,textAlign:'center'}}>1</Text>
    </TouchableOpacity>

    <TouchableOpacity style={{flex:1,paddingVertical:5,paddingHorizontal:15}}>
    <Icon name={'md-thumbs-up'} size={21} color={'#c0392b'} />
    <Text style={{fontSize:12,textAlign:'center'}}>1</Text>
    </TouchableOpacity>

    <TouchableOpacity onPress={this.link} style={{flex:1,paddingVertical:5,paddingHorizontal:15}}>
    <Icon name={'logo-whatsapp'} size={21} color={'#25d366'} />
    <Text style={{fontSize:12,textAlign:'center'}}>1</Text>

    </TouchableOpacity>

  </View>

</View> */
