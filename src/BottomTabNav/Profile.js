import React, { Component } from 'react';
import {
  View,
  Text,TouchableOpacity,StyleSheet
} from 'react-native';

 class Profile extends React.Component {
  render() {
    const { navigate } = this.props.navigation;

    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Profile!</Text>
        <TouchableOpacity
        style={styles.button}
        onPress={() => navigate('Simple')}
      >
        <Text>Simple example</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.button}
        onPress={() => navigate('Scrollable')}
      >
        <Text>Scrollable tabs example</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.button}
        onPress={() => navigate('Overlay')}
      >
        <Text>Overlay example</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.button}
        onPress={() => navigate('Facebook')}
      >
        <Text>Facebook tabs example</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.button}
        onPress={() => navigate('Dynamic')}
      >
        <Text>Dynamic tabs example</Text>
      </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 30,
    alignItems: 'center',
  },
  button: {
    padding: 10,
  },
});
export default Profile;