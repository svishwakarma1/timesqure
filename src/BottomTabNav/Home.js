import React, { Component } from 'react';
import {
  View,Text,Image,
  StyleSheet,ImageBackground, ActivityIndicator,Modal,Linking,Share,
  NetInfo,FlatList,Platform,ScrollView,TouchableHighlight,TouchableOpacity,AppState
} from 'react-native';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import Icon  from 'react-native-vector-icons/Ionicons';
import ScrollableTabView, { ScrollableTabBar } from 'react-native-scrollable-tab-view';
import DeviceInfo from 'react-native-device-info';


import { BASE_URL } from '../services/Api_constant';

//import { Container} from 'native-base';
//import  Container from './Test/Container.js';
//import ChannelHeader from './Components/ChannelHeader.js';
const data = new Array(5).fill(0);

/**
 * Time Being Image Issues 
 * using image base Url
 * 
 */


 export default class Home extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      rss: [],
      isLoading: true,
      headerrss:[],
      selected:'',
      channel_id: 46,
      category_id: 1,
      isFetching: true,
      deviceID:0,
      userID:'',
      appState: AppState.currentState
    };
    //this.setRss = this.setRss.bind(this);
  }

  static navigationOptions =
  {
    headerTransparent: true,
  };

  componentDidMount() {
    
    this.getdeviceId();

    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.loadHeaderRss();
      }
    );
  }



  componentWillUnmount() {
    this.willFocusSubscription.remove();
  }


  getdeviceId = () => {
    //Getting the Unique Id from here
    var id = DeviceInfo.getUniqueID();
    this.setState({ deviceID: id },()=>this._getStoreData());

  };


  _getStoreData = async () => {
    try {
      const value = await AsyncStorage.multiGet(["deviceID", "userID"])
      // console.log(value[0][1]);
      // console.log(value[1][1]);
      if(value !== null) {
        this.setState({deviceID:value[0][1],userID:value[1][1]})
      }
    } catch(e) {
      console.log('failed to get data from async');
    }
  }

  loadHeaderRss(){
    this.setState({isFetching:false,isLoading:true})
    const url = BASE_URL + 'Rss/categoriesList';

    fetch(url)
    .then((response) => response.json())
    .then((json) => {
      // alert(JSON.stringify(json.data))
      this.setState({
        headerrss: json.data,
        isFetching: false,
        isLoading: false 
      });
    })
    .catch((error) => {
      alert(JSON.stringify(error));
    });
  }

  



  followingPage(){
   this.props.navigation.navigate('followingPage');
  }

  /**
   *  Problem : Required Scrollable iterative (loop) based Tab.
   *  <View> tag was not allowing to scroll the react-native-scrollview-tab as it freezes the Tab.
   *  After Researching a lot there were third party plugins but for performance and flexibility reasons decided to simultaneously develope.
   *  so finished, 90% of React Native Custom Development using ScrollView and Flatlist almost achieved it but the animation and indicator part was left.  
   *  For Time Being Ali sir suggested to use react-native-scrollview-tab and native base <Container> it fixed the Problem.
   *  As the Internet Community says the more plugin the more heavy apps will be which ultimately affects the performance.
   *  But the question is Why nativebase replaced? <View> tag with <Container> tag and the difference between them and viceversa.
   *  Error was reporting something related to Adjacent JSX Fragment. Seeking something related to <View> tag. After Analysing, Studying nativebase container method when implementing it again freezed.
   *  After Research tried this <React.Fragment> tag based on anlaysing and studying a plugin module called react-native-paper.
   *  They used so this solved the problem <React.Fragment> .
   *  https://reactjs.org/blog/2017/11/28/react-v16.2.0-fragment-support.html
   *  This above article is very important also it has information about return[] (array) and return()
   */

  /**
   * Api's given by Harpreet
   * http://limrademo.com/timesqure/timesqure_api_ver_01/RssSample/SampleApi/oneIndia
   * http://limrademo.com/timesqure/timesqure_api_ver_01/RssSample/channeldetails/46
   * http://limrademo.com/timesqure/timesqure_api_ver_01/RssSample/news
   * channel_id:46
   * category_id:10
   * http://limrademo.com/timesqure/timesqure_api_ver_01/Follow/followingNews
   * "channel_id": 89,
   * "user_id": 12
   */


  categorytab(){
    let IconComponent = Icon;
    let {navigation} = this.props.navigation.navigate;
    return this.state.headerrss.map((item,key) => {
      let tabName = item.name;

      return <View key={item.id} tabLabel={tabName}>
          <FlatTabLoop category_id={item.id} nav={this.props.navigation} />
       </View>
      })

      {/* {
        item.name == 'Entertainment'?
         this.state.headerrss.map((itemx,key) => {
           return <Text>{itemx.name}</Text>
          })
          :
          <FlatTabLoop category_id={item.category_id} nav={this.props.navigation} />
       } */}

    }
    
    render() {
      let IconComponent = Icon;
      const i = this.props.i;
    return (
      this.state.isLoading ?
      <View style={styles.MainContainer}>
      <ActivityIndicator size="large" />
      </View>
      :<ScrollableTabView
        style={{ }}
        initialPage={0}
        renderTabBar={() => <ScrollableTabBar scrollOffset={5} style={{height:30,}} textStyle={{fontSize:13}} tabStyle={{height:29}} underlineStyle={{backgroundColor:'#f22613',height:2}} backgroundColor={'#ebebeb'} activeTextColor={'#f22613'} inactiveTextColor={'grey'} />}>

        {this.categorytab()}

      </ScrollableTabView>
    );
  }
}


export class FlatTabLoop extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      rss: [],
      isLoading: true,
      headerrss:[],
      selected:'',
      channel_id: 46,
      modalVisible: false,
      modalShare: "",
      isFetching: false,
    };
    //this.setRss = this.setRss.bind(this);
  }

  componentDidMount(){
  this.loadRss();

  }

  onRefresh() {
    this.setState({ isFetching: true }, function() { this.loadRss() });
  }

  loadRss(){
    fetch( BASE_URL + 'Rss/homeNews', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        category_id: this.props.category_id,
      }),
    })
    .then(response => response.json())
    .then((json) => {
      // console.warn(json.newslist);
      this.setState({
        rss: json.newslist,
        isFetching: false,
        isLoading: false 
      });
    })
    .catch((error) => {
      alert(JSON.stringify(error))
    });
  }

  Details = (item,index) =>{
    // this.props.nav.navigate('Details',{category_id: item.category_id}) 
    this.props.nav.navigate('Details',{category_id: item.category_id,details:item,selectedIndex:index});
  }

  whatsappLink(item){
    // Whatsapp Opening external link. Done!
    const newsTitle = item.title;
    // encodeURI () function returns the Whatsapp supported encode format of link which also contains image sends link as well images
    //const link = "https://youtu.be/"+item.link;
    const newsLink = encodeURI(item.link);
    
    const appSource = "Source%3A+TimeSqure%0D%0A%0D%0ADownload+Now%0D%0Ahttps%3A%2F%2Fwww.limratechnosys.com%2F";

    const url= "whatsapp://send?text="+newsTitle+"%0D%0A"+newsLink+"%0D%0A"+appSource ;
    Linking.canOpenURL(url)
    .then((supported) => {
      if (!supported) {
        console.log("Can't handle url: " + url);
        console.warn("Can't handle url: " + url);
  
      } else {
        return Linking.openURL(url);
      }
    })
    .catch((err) => {
    console.error('An error occurred', err); 
    console.warn('An error occurred', err)});
  }

  loadModal(item){
    //Here item is taken as way of optimisation of Modals. Since modal is not looped so the value is passed in this.state.modalShare: item
    this.setState({
      modalShare: item
    })
    this.setModalVisible(true);
  } 
  
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
    // console.warn(visible);
  }

  async onShare(){
    //Sharing Content with all apps Done!
  // console.warn(this.state.modalShare);
  // Whatsapp Opening external link. Done!
  const newsTitle = this.state.modalShare.title;
  // OnShare method does not require encodeURI () function 
  const newsLink = this.state.modalShare.link;
  const appSource = "Source:"+" TimeSqure"+"\n\n"+"Download Now "+"https://www.limratechnosys.com/";

  const url= newsTitle +"\n" + newsLink +"\n"+appSource ;

    try {
      const result = await Share.share({
        message: url,
      })

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };




  render () {
    let IconComponent = Icon;

    return (
       this.state.isLoading ?
      <View style={styles.MainContainer}>
      <ActivityIndicator size="large" />
      </View>:
      <View>
      <View style={styles.container}>     
      <Modal
      transparent={true}
      visible={this.state.modalVisible}
      onRequestClose={() =>{ }}>
      <View style={{flex: 1,flexDirection: 'column',justifyContent:'flex-end'}}>
        <TouchableOpacity style={{flex: 1,backgroundColor:'black',opacity:this.state.modalVisible?0.3:0}}
            onPress={() => {this.setModalVisible(!this.state.modalVisible);}}>
        </TouchableOpacity>

        <View style={{backgroundColor:'white'}}>
            <View style={{paddingHorizontal:15}}>
            <Text onPress={()=>this.onShare()} style={{textAlignVertical:'center',fontSize:16,paddingVertical:10}}> <Icon name={'md-share'} size={21}/>  Share...  </Text>
            <Text style={{textAlignVertical:'center',fontSize:16,paddingVertical:10}}> <Icon name={'md-open'} size={21}/>  Browse </Text>
            <Text style={{textAlignVertical:'center',fontSize:16,paddingVertical:10}}> <Icon name={'md-hand'} size={21}/>  Block </Text>
            <Text style={{textAlignVertical:'center',fontSize:16,paddingVertical:10}}> <Icon name={'md-remove-circle-outline'} size={21}/>  Hide this news</Text>
            <Text style={{textAlignVertical:'center',fontSize:16,paddingVertical:10}}> <Icon name={'md-thumbs-up'} size={21}/>  Show less of such content</Text>
            <Text style={{textAlignVertical:'center',fontSize:16,paddingVertical:10}}> <Icon name={'md-thumbs-down'} size={21}/>  Show more of such content</Text>
          </View>
        </View>
      </View>
    </Modal>
   
    </View>

  
      <FlatList
      data={ this.state.rss }
      onRefresh={() => this.onRefresh()}
      refreshing={this.state.isFetching}
      renderItem={({item,index}) => 

          <View style={styles.container}>

          <TouchableOpacity style={{paddingBottom:15}} onPress={()=>this.Details(item,index)}>
          <View style={styles.subcontainer} >
          <Text style={styles.textView}>{item.title}</Text>
          <Image style={styles.imageView} source={{uri: item.image}}   />
          
          <View style={{flexDirection:"row", }}>
            <Text style={{ fontSize: 11,textAlign: 'left', flex:1, alignSelf: 'flex-start',  marginTop: 15}}>{item.date}</Text>
            
            <TouchableOpacity onPress={()=>this.whatsappLink(item)} style={{padding:5,textAlign: 'right',alignSelf: 'flex-end'}}>
              <Text>
               <IconComponent   name={'logo-whatsapp'} size={25} color={'#25d366'} />
              </Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={()=>this.loadModal(item)}  style={{padding:5,textAlign: 'right',alignSelf: 'flex-end',transform: [{ rotate: '90deg'}] }}>
              <Text>
                <IconComponent  name={'ios-more'} size={25} color={'grey'} />
              </Text>
            </TouchableOpacity>

          </View>
          </View>
          </TouchableOpacity>    
          
          </View>

        }
        keyExtractor={(item, index) => index.toString()}
      />
      </View>
    )
  }
}






const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',


    
  },
  subcontainer:{
    paddingHorizontal: 15,
  },
  imageView:{
    height: 170,
    width: '100%',
    marginTop: 15,
    borderRadius: 5,
  },
  textView: {
    fontSize: 15,
    color: 'black',
    fontWeight: 'bold'
  },
  contentContainer: {
    paddingVertical: 20
  },
  textBold:{
    fontWeight: 'bold'
  },
  MainContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});



// import React, { Component } from 'react';
// import {
//   View,
//   Text,
//   ActivityIndicator, 
//   StyleSheet,
// } from 'react-native';
// import * as rssParser from 'react-native-rss-parser';



//  class Home extends React.Component {
//   constructor(props){
//     super(props);
//     this.state ={ isLoading: true}
//   }

//   componentDidMount(){
//     return fetch('http://www.nasa.gov/rss/dyn/breaking_news.rss')
//       .then((response) => response.text())
//       .then((responseData) => rssParser.parse(responseData))
//       .then((rss) => {
//         console.warn(rss);
//         console.warn(rss.title);
//         console.warn(rss.items.length);

//         this.setState({
//           isLoading: false,
//           dataSource: rss,
//         });

//       })
//       .catch((error) =>{
//         console.error(error);
//       });
//   }

  
//   render() {
//     if(this.state.isLoading){
//       return(
//         <View style={{flex: 1, padding: 20}}>
//           <ActivityIndicator/>
//         </View>
//       )
//     }
//     return(
//       <View style={{flex: 1, paddingTop:20}}>
//        <Text>Hello and Welcome</Text>
//       </View>
//     );
//   }

// }


// export default Home;