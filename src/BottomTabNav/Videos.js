import React, { Component } from 'react';
import {
  View,
  Text,
  ActivityIndicator, Linking,Share,Modal,
  StyleSheet,NetInfo,FlatList,Image,ImageBackground,Platform,ScrollView,TouchableHighlight,TouchableOpacity
} from 'react-native';

import Icon  from 'react-native-vector-icons/Ionicons';
import ScrollableTabView, { ScrollableTabBar } from 'react-native-scrollable-tab-view';
import axios from 'axios';

import { BASE_URL } from '../services/Api_constant';

//import { Container} from 'native-base';
//import  Container from './Test/Container.js';
//import ChannelHeader from './Components/ChannelHeader.js';
const data = new Array(5).fill(0);
const YoutubeApiKey = "AIzaSyCwPyeDgIp4jIeHecewfPDBW5NBdBoVRW4";



/**
 * Time Being Image Issues 
 * using image base Url
 * 
 */


 export default class Videos extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      rss: [],
      isLoading: true,
      headerrss:[],
      selected:'',
      channel_id: 46,
      category_id: 10,
      isFetching: true,
    };
    //this.setRss = this.setRss.bind(this);
  }

  static navigationOptions =
  {
    headerTransparent: true,
  };

  componentDidMount() {
    // alert('reached')
   this.loadHeaderRss();
  }



  Details = () =>{
    this.props.navigation.navigate('Details');
  }

  loadHeaderRss(){
    fetch(BASE_URL+'Video/categoriesList')
    .then(response => response.json())
    .then((json) => {
      // console.warn(json.categoriesList);
      this.setState({
        headerrss: json.data,
        isFetching: false,
        isLoading: false 
      });
    })
    .catch(function (error) {
      console.warn(error);
    });
  }

  



  followingPage(){
   this.props.navigation.navigate('followingPage');
  }




  categorytab(){
    let IconComponent = Icon;
    let {navigation} = this.props.navigation.navigate;
    return this.state.headerrss.map((item,key) => {
      let tabName = item.name;

      return <View key={item.id} tabLabel={tabName.toUpperCase()}>
      <FlatTabLoop category_id={item.id} category_name={item.name} nav={this.props.navigation} />
       </View>
      })
        {/* {
          item.name == 'Entertainment'?
           this.state.headerrss.map((itemx,key) => {
             return <Text>{itemx.name}</Text>
            })
        :
          <FlatTabLoop category_id={item.id} nav={this.props.navigation} />
        } */}
  }

  render() {
    let IconComponent = Icon;
    const i = this.props.i;

    return (

      this.state.isLoading ?

      <View style={styles.MainContainer}>
      <ActivityIndicator size="large" />
      </View>
      :<ScrollableTabView
        style={{ }}
        initialPage={0}
        renderTabBar={() => <ScrollableTabBar scrollOffset={5} style={{height:30,}} textStyle={{fontSize:13}} tabStyle={{height:29}} underlineStyle={{backgroundColor:'#f22613',height:2}} backgroundColor={'#ebebeb'} activeTextColor={'#f22613'} inactiveTextColor={'grey'} />}>

        {this.categorytab()}

      </ScrollableTabView>
    );
  }
}


export class FlatTabLoop extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      modalVisible: false,
      modalShare: "",
      rss: [],
      isLoading: true,
      headerrss:[],
      selected:'',
      channel_id: 46,
      isFetching: false,
      selectedVideo: ""
    };
    //this.setRss = this.setRss.bind(this);
  }

componentDidMount(){
 this.loadRss();

}

onRefresh() {
  this.setState({ isFetching: true }, function() { this.loadRss() });
}

loadRss(){


  fetch(BASE_URL+'Video/homeNewsVideos', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      "user_id": 9,
      "category_id":this.props.category_id
    }),
  })
  .then(response => response.json())
  .then((json) => {
    console.warn(json.newslist);
    this.setState({
      rss: json.newslist,
      isFetching: false,
      isLoading: false 
    });
  })
  .catch((error) => {
    console.warn(error);
  });
}

  Details = () =>{
    
    this.props.nav.navigate('Details') 
  }

  selectedVideo = (link) =>{
    this.props.nav.navigate('VideoCategoryDetails',{category_id: this.props.category_id,category_name: this.props.category_name,VideoId:link})  
  //  this.setState({
  //    selectedVideo: item.news_id,
  //  })
  //  console.warn(this.state.status);
   //console.warn(this.state.selectedVideo+item.news_id);
  }

  whatsappLink(item){
    // Whatsapp Opening external link. Done!
    const newsTitle = item.title;
    // encodeURI () function returns the Whatsapp supported encode format of link which also contains image sends link as well images
    const link = "https://youtu.be/"+item.link;
    const newsLink = encodeURI(link);
    
    const appSource = "Source%3A+TimeSqure%0D%0A%0D%0ADownload+Now%0D%0Ahttps%3A%2F%2Fwww.limratechnosys.com%2F";

    const url= "whatsapp://send?text="+newsTitle+"%0D%0A"+newsLink+"%0D%0A"+appSource ;
    Linking.canOpenURL(url)
    .then((supported) => {
      if (!supported) {
        console.log("Can't handle url: " + url);
        console.warn("Can't handle url: " + url);
  
      } else {
        return Linking.openURL(url);
      }
    })
    .catch((err) => {
    console.error('An error occurred', err); 
    console.warn('An error occurred', err)});
  }

  loadModal(item){
    //Here item is taken as way of optimisation of Modals. Since modal is not looped so the value is passed in this.state.modalShare: item
    this.setState({
      modalShare: item
    })
    this.setModalVisible(true);
  } 
  
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
    console.warn(visible);
  }

  async onShare(){
    //Sharing Content with all apps Done!
  console.warn(this.state.modalShare);
  // Whatsapp Opening external link. Done!
  const newsTitle = this.state.modalShare.title;
  // OnShare method does not require encodeURI () function 
  const newsLink = "https://youtu.be/"+this.state.modalShare.link;
  const appSource = "Source:"+" TimeSqure"+"\n\n"+"Download Now "+"https://www.limratechnosys.com/";

  const url= newsTitle +"\n" + newsLink +"\n"+appSource ;

    try {
      const result = await Share.share({
        message: url,
      })

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  render () {
    let IconComponent = Icon;
    return (
       this.state.isLoading ?
      <View style={styles.MainContainer}>
      <ActivityIndicator size="large" />
      </View>:

      <View>
           <View style={styles.container}>     
              <Modal
              transparent={true}
              visible={this.state.modalVisible}
              onRequestClose={() =>{ }}>
              <View style={{flex: 1,flexDirection: 'column',justifyContent:'flex-end'}}>
                <TouchableOpacity style={{flex: 1,backgroundColor:'black',opacity:this.state.modalVisible?0.3:0}}
                    onPress={() => {this.setModalVisible(!this.state.modalVisible);}}>
                </TouchableOpacity>

                <View style={{backgroundColor:'white'}}>
                    <View style={{paddingHorizontal:15}}>
                    <Text onPress={()=>this.onShare()} style={{textAlignVertical:'center',fontSize:16,paddingVertical:10}}> <Icon name={'md-share'} size={21}/>  Share...  </Text>
                    <Text style={{textAlignVertical:'center',fontSize:16,paddingVertical:10}}> <Icon name={'md-open'} size={21}/>  Browse </Text>
                    <Text style={{textAlignVertical:'center',fontSize:16,paddingVertical:10}}> <Icon name={'md-hand'} size={21}/>  Block </Text>
                    <Text style={{textAlignVertical:'center',fontSize:16,paddingVertical:10}}> <Icon name={'md-remove-circle-outline'} size={21}/>  Hide this news</Text>
                    <Text style={{textAlignVertical:'center',fontSize:16,paddingVertical:10}}> <Icon name={'md-thumbs-up'} size={21}/>  Show less of such content</Text>
                    <Text style={{textAlignVertical:'center',fontSize:16,paddingVertical:10}}> <Icon name={'md-thumbs-down'} size={21}/>  Show more of such content</Text>
                  </View>
                </View>
              </View>
            </Modal>
        
          </View>
      <FlatList
      data={ this.state.rss }
      extraData={this.state.selectedVideo}
      onRefresh={() => this.onRefresh()}
      refreshing={this.state.isFetching}
      renderItem={({item}) => 

          <View style={styles.container}>

          <TouchableOpacity style={{borderColor:this.state.selectedVideo === item.news_id?'red':'black',paddingBottom:15}} onPress={()=>this.selectedVideo(item.link)}>
          <View style={styles.subcontainer} >
          <Text style={styles.textView}>{item.title}</Text>
          <Image style={styles.imageView} source={{uri:"https://i.ytimg.com/vi/"+item.link+"/0.jpg"}} />
          {/* <YouTube
          apiKey={YoutubeApiKey}
          videoId={item.link} // The YouTube video ID
          play={false} // control playback of video with true/false
          fullscreen={false} // control whether the video should play in fullscreen or inline
          loop={true} // control whether the video should loop when ended
          resumePlayAndroid={false} // tab switching crashes without this
          onReady={e => this.setState({ isReady: true })}
          onChangeState={e => this.setState({ status: e.state })}
          onChangeQuality={e => this.setState({ quality: e.quality })}
          onError={e => console.warn(e.error)}
          style={{ width: 300, height: 200 }}
        /> */}
          <View style={{flexDirection:"row", }}>
            <Text style={{ fontSize: 11,textAlign: 'left', flex:1, alignSelf: 'flex-start',  marginTop: 15}}>{item.date}</Text>
            
            <TouchableOpacity onPress={()=>this.whatsappLink(item)} style={{padding:5,textAlign: 'right',alignSelf: 'flex-end'}}>
              <Text>
               <IconComponent   name={'logo-whatsapp'} size={25} color={'#25d366'} />
              </Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={()=>this.loadModal(item)} style={{padding:5,textAlign: 'right',alignSelf: 'flex-end',transform: [{ rotate: '90deg'}] }}>
              <Text>
                <IconComponent  name={'ios-more'} size={25} color={'grey'} />
              </Text>
            </TouchableOpacity>

          </View>
          </View>
          </TouchableOpacity>    
          
          </View>

        }
        keyExtractor={(item, index) => index.toString()}
      />
      </View>
    )
  }
}




/***
 * 
 * 
 *   
          <YouTube
              apiKey={YoutubeApiKey}
              videoId={"OvvjMloXPy4"}      // The YouTube video ID
              play={this.state.selectedVideo ===  item.news_id ? true : false}              // control playback of video with true/false
              fullscreen={false}        // control whether the video should play in fullscreen or inline
              loop={false}             // control whether the video should loop when ended
            
              onReady={(e) => this.Ready(e,item)}
              onChangeState={(e) => this.ChangeState((e,item))}
              // onChangeQuality={e => this.setState({ quality: e.quality })}
               onError={e => this.setState({ error: e.error })}
              style={{ alignSelf: 'stretch', height: 300 }}
            />
 */

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',

    
  },
  subcontainer:{
    paddingHorizontal: 15,
  },
  imageView:{
    height: 170,
    width: '100%',
    marginTop: 15,
    borderRadius: 5,
  },
  textView: {
    fontSize: 15,
    color: 'black',
    fontWeight: 'bold'
  },
  contentContainer: {
    paddingVertical: 20
  },
  textBold:{
    fontWeight: 'bold'
  },
  MainContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

