import React, { Component } from 'react'
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,FlatList,Image,ActivityIndicator,AsyncStorage
} from 'react-native'
import Icon  from 'react-native-vector-icons/Ionicons';

import { BASE_URL } from '../services/Api_constant';

export default class Notifications extends Component {

  constructor(props) {
    super(props);

    this.state = {
      modalVisible: false,
      modalShare: "",
      rss: [],
      isLoading: true,
      headerrss:[],
      selected:'',
      channel_id: 46,
      isFetching: false,
      selectedVideo: "",
      userID:null
    };
    //this.setRss = this.setRss.bind(this);
  }

  componentDidMount(){
    this._getStoreUserDetail();
   }

   _getStoreUserDetail = async () => {
    try {
      const value = await AsyncStorage.getItem('user_id');
      if (value !== null) {
        this.setState({userID:value},()=>this.loadRss())
      }else{
        this.stopLoading(false)
      }
    } catch (error) {
      aler('have issue while fetching user detail');
    }
  }

    stopLoading = (state) => {
      this.setState({isFetching: state,isLoading: state});
    }

   loadRss(){

    this.stopLoading(true)

    fetch(BASE_URL+'Rss/notification', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "user_id": this.state.userID,
      }),
    })
    .then(response => response.json())
    .then((json) => {
      // alert(JSON.stringify(json));

      if(json.status){
        this.setState({rss: json.data});
      }
      this.stopLoading(false);

    })
    .catch((error) => {
      console.warn(error);
    });
  }
  


  render () {
    let IconComponent = Icon;

    return (
      <View style={styles.container}>
        {this.state.isLoading?
        <ActivityIndicator />

          :<FlatList
            data={ this.state.rss }
            extraData={this.state.selectedVideo}
            renderItem={({item}) => 
                <TouchableOpacity style={{borderColor:this.state.selectedVideo === item.news_id?'red':'black'}}>
                  <View style={styles.subcontainer} >
                    <Image style={styles.imageView} source={{uri:item.image}} />          
                    <Text style={styles.textView} numberOfLines={3}>{item.title}</Text>
                  </View>
                </TouchableOpacity>
              }
              keyExtractor={(item, index) => index.toString()}
            />
            }

      </View>
      )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
    subcontainer:{
      flex:1,
      flexDirection:'row',
      justifyContent: 'center',
      borderBottomWidth:1,
      borderBottomColor:'grey',
      paddingHorizontal: 10,
      paddingVertical:5

    },
    imageView:{
      flex:0.3,
      height: 60,
      width:'100%',
      borderRadius: 5,
      margin:5,
      resizeMode: 'contain'

    },
    textView: {
      flex: 0.7,
      textAlignVertical:'center',
      fontSize: 15,
    },
    contentContainer: {
      paddingVertical: 20
    },
    textBold:{
      fontWeight: 'bold'
    },
    MainContainer: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
  });
  