import React, { Component } from 'react';
import { Dimensions, View } from 'react-native';

import YouTube from 'react-native-youtube'


const YoutubeApiKey = "AIzaSyCwPyeDgIp4jIeHecewfPDBW5NBdBoVRW4";

const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;


export default class Youtubevideo extends Component {

    render() {
        const item = this.props.item
        //const playon = this.props.playon
        return (
            <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
            <YouTube
              apiKey={YoutubeApiKey}
              videoId="527ZQgRx8W8" // The YouTube video ID
              play={false} // control playback of video with true/false
              fullscreen={false} // control whether the video should play in fullscreen or inline
              loop={true} // control whether the video should loop when ended
              resumePlayAndroid={false} // tab switching crashes without this
              onReady={e => this.setState({ isReady: true })}
              onChangeState={e => this.setState({ status: e.state })}
              onChangeQuality={e => this.setState({ quality: e.quality })}
              onError={e => console.warn(e.error)}
              style={{ width: 300, height: 200 }}
            />
          </View>
        );
    }
};

/*<YouTube
    apiKey={YoutubeApiKey}
    videoId={item.link} // The YouTube video ID
    play={true} // control playback of video with true/false
    fullscreen={false} // control whether the video should play in fullscreen or inline
    loop={false} // control whether the video should loop when ended
    resumePlayAndroid={false} // tab switching crashes without this
    onReady={e => this.setState({ isReady: true })}
    onChangeState={e => this.setState({ status: e.state })}
    onChangeQuality={e => this.setState({ quality: e.quality })}
    onError={e => console.warn(e.error)}
    style={{flex:0.8,height:200,marginTop:10}}
/>*/
module.export = Youtubevideo;