import React, { Component } from 'react'
import {
  View,Image,ImageBackground,
  Text,FlatList,ActivityIndicator,ScrollView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Icon  from 'react-native-vector-icons/Ionicons';
import DeviceInfo from 'react-native-device-info';
import AsyncStorage from '@react-native-community/async-storage';

import { BASE_URL } from '../services/Api_constant';

const deviceId = DeviceInfo.getDeviceId();
const getDeviceName = DeviceInfo.getDeviceName();
//const uniqueId = DeviceInfo.uniqueId();
const getModel = DeviceInfo.getModel();

export default class Follow extends Component {

  constructor(props) {
    super(props);

    this.state = {
      followingRssCategory: [],
      followingRssChannel: [],
      selectedCategory: null,
      selectedChannel: null,
      isLoading: true,
      userID:null
    };
  }

  componentDidMount() {
    this._getStoreUserDetail();
   }

  _getStoreUserDetail = async () => {
    try {
      const value = await AsyncStorage.getItem('user_id');
      if (value !== null) {
        this.setState({userID:value})
      }

      this.followingRssCategory();
      this.followingRssChannel();
    } catch (error) {
      aler('have issue while fetching user detail');
    }
  }
  
  followingRssCategory = () => {
    fetch(BASE_URL+'Rss/categoryFollowList', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        user_id: this.state.userID,
        language:2
      }),
    })
    .then(response => response.json())
    .then((json) => {
      // console.warn(json);
      if(json.status){
        this.setState({followingRssCategory: json.data});
      }
        
      this.setState({isFetching: false,isLoading: false })
    })
    .catch(function (error) {
      console.warn(error);
    });
  };

  followingRssChannel = () => {
      fetch(BASE_URL+'Rss/channelFollowList', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          user_id: this.state.userID,
          language:2
        }),
      })
      .then(response => response.json())
      .then((json) => {
        // alert(JSON.stringify(json))
        // console.warn(json);
        if(json.status){
          this.setState({followingRssChannel: json.data});
        }

        this.setState({isFetching: false,isLoading: false })
      })
      .catch(function (error) {
        alert('reached')
      })
  };


  following = (item) =>{
    this.setState({
      selected: item.following
    })
  }

  toggleFollowCategory = (item) => {

    this.setState({
      selectedCategory: item.name+item.following,
      selectedStatus: item.following,
      isLoading: false 
    });

      if(this.state.userID!==null){
        fetch(BASE_URL+'Rss/categoryFollow', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            user_id: this.state.userID,
            categories_id: item.id,
          }),
        })
        .then(response => response.json())
        .then((json) => {
          console.warn(json);

        })
        .catch(function (error) {
          console.warn(error);
        });
      }else{
        alert('Please login');
      }
    }

  toggleFollowChannel = (item) => {

      this.setState({
        selectedChannel: item.channel_name+item.following,
        selectedStatus: item.following,
        isLoading: false 
      });
    
    if(this.state.userID!==null){
      fetch(BASE_URL+'Rss/channelFollow', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            user_id: this.state.userID,
            channel_id: item.channel_id,
          }),
        })
        .then(response => response.json())
        .then((json) => {
          console.warn(json);
        })
        .catch(function (error) {
          console.warn(error);
        });
      }else{
        alert('Pleas login')
      }
  }
  

    componentDidUpdate(prevProps, prevState, snapshot){
      if (this.state.selectedChannel !== prevState.selectedChannel ) {
          //console.warn("componentDidUpdate "+this.state.selectedChannel+"prevState "+prevState.selectedChannel);
          //console.warn("if prevState "+prevState.selectedChannel+" selectedChannel "+this.state.selectedChannel+" prevState "+prevState.selectedStatus+" selectedStatus "+this.state.selectedStatus);
        this.followingRssChannel();
      }

      if (this.state.selectedCategory !== prevState.selectedCategory){
        this.followingRssCategory();
      }
    
    }
  



  render () {
    let IconComponent = Icon;

    return (
      <ScrollView>
      {this.state.isLoading?
        <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
        <ActivityIndicator />
        </View>:

      
      <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
      
      <Text style={styles.text}>Category</Text>

      <FlatList
      horizontal
      data={ this.state.followingRssCategory }
      keyExtractor={(item,index)=>index.toString()}
      extraData={this.state}
      renderItem={({item}) => 
        <View style={{flex:1,width:'25%',borderWidth:1,margin:5,borderRadius:5,borderColor:'grey',backgroundColor:'white',alignItems:'center',justifyContent:'center'}}>        
        <TouchableOpacity style={{}} onPress={() => this.following(item)}>
          {/* <ImageBackground style={{width:100,height:100,resizeMode:'contain',justifyContent:'center',alignItems:'center'}} source={{uri: item.image}}>
             <Text style={{backgroundColor:'white',textAlign:'center',textAlignVertical:'center'}}>{item.name}</Text>
          </ImageBackground> */}
          <Image style={{width:100,height:100,resizeMode:'contain',justifyContent:'center',alignItems:'center'}} source={{uri: item.image}} />
          <Text style={{textAlign:'center',textAlignVertical:'center'}}>{item.name}</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>this.toggleFollowCategory(item)} style={{padding:8}}>
         <Text style={{fontSize: 12,borderWidth:1,paddingVertical:2,paddingHorizontal:5,borderColor: '#1DA1F2',borderRadius: 5 ,color: '#1DA1F2'}}>
         <IconComponent name={item.following === 'Y' ? 'ios-star':'ios-star-outline'} size={14} color={'#1DA1F2'} />
         {item.following === 'Y' ? '':' Follow'}</Text>
         </TouchableOpacity>
        {/* <TouchableOpacity style={{borderWidth:1,borderColor:'blue',borderRadius:5}} onPress={() => this.props.navigation.navigate('DrawerOpen')}>
          <Text style={{}}>{this.state.selected === item.following?'Following':'Follow'}</Text>
        </TouchableOpacity> */}
        </View>
        }
        />

        <Text style={styles.text}>Channel</Text>

        <FlatList
        keyExtractor={(item,index)=>index.toString()}
        horizontal
        data={ this.state.followingRssChannel }
        extraData={this.state}
        renderItem={({item}) => 
          <View style={{flex:1,borderWidth:1,margin:5,borderRadius:5,borderColor:'grey',backgroundColor:'white',alignItems:'center',justifyContent:'center'}}>        
          <TouchableOpacity style={{}} onPress={() => this.following(item)}>
            {/* <ImageBackground style={{width:100,height:100,resizeMode:'contain',justifyContent:'center',alignItems:'center'}} source={{uri: item.image}}>
               <Text style={{backgroundColor:'white',textAlign:'center',textAlignVertical:'center'}}>{item.channel_name}</Text>
            </ImageBackground> */}
            <Image style={{width:100,height:100,resizeMode:'contain',justifyContent:'center',alignItems:'center'}} source={{uri: item.image}} />
            <Text style={{textAlign:'center',textAlignVertical:'center'}}>{item.channel_name}</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>this.toggleFollowChannel(item)} style={{padding:8}}>
           <Text style={{fontSize: 12,borderWidth:1,paddingVertical:2,paddingHorizontal:5,borderColor: '#1DA1F2',borderRadius: 5 ,color: '#1DA1F2'}}>
           <IconComponent name={item.following === 'Y' ? 'ios-star':'ios-star-outline'} size={14} color={'#1DA1F2'} />
           {item.following=='N'?'Follow':''}</Text>
           </TouchableOpacity>
          {/* <TouchableOpacity style={{borderWidth:1,borderColor:'blue',borderRadius:5}} onPress={() => this.props.navigation.navigate('DrawerOpen')}>
            <Text style={{}}>{this.state.selected === item.following?'Following':'Follow'}</Text>
          </TouchableOpacity> */}
          </View>
          }
          />
          
      </View>
    }
    </ScrollView>
      )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  text: {
    color: 'black',
    fontSize: 21,
    marginLeft: 15,
    alignSelf: 'flex-start',
    
  },
  button: {
    position: 'absolute',
    top: 50,
    left: 0,
    width: 150,
    height: 50,
    backgroundColor: '#f39c12',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    color: 'white'
  }
})