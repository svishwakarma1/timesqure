import React, { Component } from 'react';
import {
  View,Text,Image,
  ActivityIndicator,Dimensions,BackHandler,
  StyleSheet,FlatList,TextInput,Modal,KeyboardAvoidingView,ToastAndroid,Share,Linking,Platform,ScrollView,TouchableHighlight,TouchableOpacity,SafeAreaView
} from 'react-native';
import _ from "lodash";
import {Container,Footer,Header,Left,Right,Button, Content, Body} from "native-base";
import Icon  from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-community/async-storage';

import { BASE_URL } from './services/Api_constant';

const test=[{"channel_name":"Times of India"},{"channel_name":"Zee News"}];
const testWorkable=[{"45":"Times of India","51":"Zee News"}];



//item.channel_name == test
var {height, width} = Dimensions.get('window');
// a component that calls the imperative ToastAndroid API
const Toast = (props) => {
  if (props.visible) {
    ToastAndroid.showWithGravity(
      props.message,
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
      25,
      50,
    );
    return null;
  }
  return null;
};

const data = new Array(5).fill(0);


 class Details extends React.Component {

  static navigationOptions = {
    header:null,
    drawerLockMode: 'locked-closed'
  }    
  
  constructor(props) {
    super(props);

    this.state = {
      category_id: this.props.navigation.getParam('category_id',11),
      rss: [],
      updateRss:[],
      newsLiked:null,
      comment: false,
      selectedChannel: null,
      selectedStatus: null,
      previousSelectedStatus: null,
      followingMessage: " ",
      followingStatus: " ",
      follow: "Follow",
      following: "Following",
      modalVisible: false,
      modalShare: "",
      status: "",
      layoutType: true,
      isFetching: false,
      isLoading: true,
      backgroundColor: '',
      userID:'',
      deviceID:0
    };
    this.setRss = this.setRss.bind(this);
  }

  followingPage = (category_id,channel_id) => {
    let catid = category_id;
    let chanid = channel_id;
    
    this.props.navigation.navigate('followingPage',{'category_id': catid,'channel_id': chanid});
  }

  componentDidMount(){
    // alert('reached')
    // BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    // alert(JSON.stringify(this.props.navigation))
    this._getStoreUserDetail()
  }

  _getStoreUserDetail = async () => {
    try {
      const value = await AsyncStorage.getItem('user_id');
      if (value !== null) {
        this.setState({userID:value},()=>this.loadRss())
      }else{
        this.loadRss();
      }
    } catch (error) {
      aler('have issue while fetching user detail');
    }
  }

  componentWillUnmount() {
    //Test Basis
    // BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }
  componentDidUpdate(prevProps, prevState, snapshot){
    if (this.state.selectedChannel !== prevState.selectedChannel ) {
        //console.warn("componentDidUpdate "+this.state.selectedChannel+"prevState "+prevState.selectedChannel);
        //console.warn("if prevState "+prevState.selectedChannel+" selectedChannel "+this.state.selectedChannel+" prevState "+prevState.selectedStatus+" selectedStatus "+this.state.selectedStatus);
      this.loadRss();
    }
    else if (this.state.newsLiked !== prevState.newsLiked){
      this.loadRss();
    }
  
  }

  handleBackPress = () => {
    console.warn('BackPress');
    this.setState({
      comment: false
    })
    this.props.navigation.goBack(); // works best when the goBack is async
    return true;
  }

  

  handleButtonPress = () => {
    this.setState(
      {
        visible: true,
      },
      () => {
        this.hideToast();
      },
    );
  };



  hideToast = () => {
    this.setState({
      visible: false,
    });
  };

  loadRss = _.debounce(() => {
    //this.loadUpdateRss();
    fetch(BASE_URL+'Rss/homeNewsDetails', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        category_id: this.state.category_id,
        user_id: this.state.userID,
        //channel_id: 51,
      }),
    })
    .then(response => response.json())
    .then((json) => {
      // alert(JSON.sstringify(json.newslist));
      this.setState({
        rss: json.newslist,
        updateRss: json.newslist,
        isFetching: false,
        isLoading: false 
      });
    })
    .catch(function (error) {
      alert(JSON.stringify(error));
    });
  },250);

  toggleFollow = (item) => {

    this.setState({
      selectedChannel: item.channel_name+item.following,
      selectedStatus: item.following,
      isLoading: false 
    });

    // console.warn("channelName "+this.state.selectedChannel);

  
      fetch(BASE_URL+'Rss/channelFollow', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          user_id: this.state.userID,
          device_id:this.state.device_id,
          channel_id: item.channel_id,
        }),
      })
      .then(response => response.json())
      .then((json) => {
        // console.warn(json);

      })
      .catch(function (error) {
        console.warn(error);
      });
    }
   
  toggleLike = (item) => {

    this.setState({
      newsLiked: item.news_id+item.channel_name+item.like,
      isLoading: false 
    });


    //To Follow and Unfollow Channel
    fetch(BASE_URL+'Rss/newsLike', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        news_id: item.news_id,
        user_id: this.state.userID,
      }),
    })
    .then(response => response.json())
    .then((json) => {
      const fStatus = json.Status ; 
      // console.warn(json);
      
    })
    .catch(function (error) {
      console.warn(error);
    });

  }

  navCommentPage = (item) => {
    this.props.navigation.navigate('Comment',{item});
    //To Follow and Unfollow Channel	
    // fetch('http://limrademo.com/timesqure/ApiV1/Rss/channelFollow', {
    //   method: 'POST',
    //   headers: {
    //     'Accept': 'application/json',
    //     'Content-Type': 'application/json',
    //   },
    //   body: JSON.stringify({
    //     channel_id: 49,
    //     user_id: 8,
    //   }),
    // })
    // .then(response => response.json())
    // .then((json) => {
    //   const fStatus = json.Status ; 
    //   console.warn(fStatus);
    //   this.setState({
    //     visible: !this.state.visible,
    //   });
      
    // })
    // .catch(function (error) {
    //   console.warn(error);
    // });

  }

  setRss(json) {
    console.warn(json.items);
    this.setState({
      rss: json.items,
      isFetching: false 
    });
  }

  whatsappLink(item){
    // Whatsapp Opening external link. Done!
    const newsTitle = item.title;
    // encodeURI () function returns the Whatsapp supported encode format of link which also contains image sends link as well images
    const newsLink = encodeURI(item.link);
    const appSource = "Source%3A+TimeSqure%0D%0A%0D%0ADownload+Now%0D%0Ahttps%3A%2F%2Fwww.limratechnosys.com%2F";

    const url= "whatsapp://send?text="+newsTitle+"%0D%0A"+newsLink+"%0D%0A"+appSource ;
    Linking.canOpenURL(url)
    .then((supported) => {
      if (!supported) {
        console.log("Can't handle url: " + url);
        console.warn("Can't handle url: " + url);
  
      } else {
        return Linking.openURL(url);
      }
    })
    .catch((err) => {
    console.error('An error occurred', err); 
    console.warn('An error occurred', err)});
    }

  loadModal(item){
      //Here item is taken as way of optimisation of Modals. Since modal is not looped so the value is passed in this.state.modalShare: item
      this.setState({
        modalShare: item
      })
      this.setModalVisible(true);
  } 
    
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
    console.warn(visible);
  }

  async onShare(){
    //Sharing Content with all apps Done!
  console.warn(this.state.modalShare);
  // Whatsapp Opening external link. Done!
  const newsTitle = this.state.modalShare.title;
  // OnShare method does not require encodeURI () function 
  const newsLink = this.state.modalShare.link;
  const appSource = "Source:"+" TimeSqure"+"\n\n"+"Download Now "+"https://www.limratechnosys.com/";

  const url= newsTitle +"\n" + newsLink +"\n"+appSource ;

    try {
      const result = await Share.share({
        message: url,
      })

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };
  
  onFocus(){
    //console.warn(' onFocus');
    this.setState({
        backgroundColor: 'green',
        comment: true
    })
  }

  onBlur(){
    //console.warn(' onBlur');
    this.setState({
      backgroundColor: '#ededed',
      comment: false
    })
  }
  
  ctest = () =>{
    //Its Working properly <TextInput onFocus={this.ctest}
    console.warn(' onFocus ctest');

    this.setState({
      comment: !this.state.comment
    })
  }

  render() {
    // alert(this.state.category_id)
    let IconComponent = Icon;
    const { navigate } = this.props.navigation;

    
    return (
      this.state.isLoading ?
        <View style={styles.container}><ActivityIndicator size={'large'} /></View> :

      <View style={{flex:1}}>
        {/* <ScrollView>
        {rssList}
        </ScrollView> */}
            <View style={styles.container}>     
              <Modal
              transparent={true}
              visible={this.state.modalVisible}
              onRequestClose={() =>{ }}>
              <View style={{flex: 1,flexDirection: 'column',justifyContent:'flex-end'}}>
                <TouchableOpacity style={{flex: 1,backgroundColor:'black',opacity:this.state.modalVisible?0.3:0}}
                    onPress={() => {this.setModalVisible(!this.state.modalVisible);}}>
                </TouchableOpacity>

                <View style={{backgroundColor:'white'}}>
                    <View style={{paddingHorizontal:15}}>
                    <Text onPress={()=>this.onShare()} style={{textAlignVertical:'center',fontSize:16,paddingVertical:10}}> <Icon name={'md-share'} size={21}/>  Share...  </Text>
                    <Text style={{textAlignVertical:'center',fontSize:16,paddingVertical:10}}> <Icon name={'md-open'} size={21}/>  Browse </Text>
                    <Text style={{textAlignVertical:'center',fontSize:16,paddingVertical:10}}> <Icon name={'md-hand'} size={21}/>  Block </Text>
                    <Text style={{textAlignVertical:'center',fontSize:16,paddingVertical:10}}> <Icon name={'md-remove-circle-outline'} size={21}/>  Hide this news</Text>
                    <Text style={{textAlignVertical:'center',fontSize:16,paddingVertical:10}}> <Icon name={'md-thumbs-up'} size={21}/>  Show less of such content</Text>
                    <Text style={{textAlignVertical:'center',fontSize:16,paddingVertical:10}}> <Icon name={'md-thumbs-down'} size={21}/>  Show more of such content</Text>
                  </View>
                </View>
              </View>
            </Modal>
        
          </View>

      <ScrollView horizontal={true} scrollEnabled={true} pagingEnabled={true} >
      
        <FlatList
        data={this.state.rss}
        horizontal={true}
        keyExtractor={(item, index) => index.toString()}
        initialScrollIndex={this.props.navigation.getParam('selectedIndex')}
        renderItem={({item}) => 
      

          <View style={{width:width}}>

          <View style={{flexDirection:'row',justifyContent:"space-between",alignItems:"center",borderBottomWidth:1,borderBottomColor:'#d9e1e3',height:60,backgroundColor:'white'}}>
            <TouchableOpacity style={{paddingVertical:10,paddingHorizontal:15}} onPress={() => this.props.navigation.goBack()}><Text ><IconComponent name={'md-arrow-back'} size={25} color={'grey'}/></Text></TouchableOpacity>
            <TouchableOpacity style={{paddingVertical:10,paddingHorizontal:15}} onPress={() => this.loadModal(item)}><Text ><IconComponent name={'md-more'} size={25} color={'grey'}/></Text></TouchableOpacity>
          </View>

         

         <ScrollView > 
         <View style={styles.subcontainer} >
        
        <Text style={styles.textView}>{item.title}</Text>
      
       <View style={{flexDirection:"row", }}>
         <Text onPress={this.followingPage.bind(this,item.category_id,item.channel_id)} style={{ fontSize: 13,color: 'black',textAlign: 'left', flex:1, alignSelf: 'flex-start',marginTop: 5}}>{item.channel_name}{"\n"}<Text style={{fontSize: 9, color: 'grey'}}> 816k Followers {this.state.followingMessage}</Text></Text>

         <TouchableOpacity onPress={()=>this.toggleFollow(item)} style={{padding:8}}>
         <Text style={{fontSize: 12,borderWidth:1,paddingVertical:2,paddingHorizontal:5,borderColor: '#1DA1F2',borderRadius: 5 ,color: '#1DA1F2'}}>
         <IconComponent name={item.following === 1 ? 'ios-star':'ios-star-outline'} size={14} color={'#1DA1F2'} />
         {item.following === 1 ? '':' Follow'}</Text>
         </TouchableOpacity>
       </View>

       </View>

        <Image style={styles.imageView} source={{uri:item.image}}   />
        
        <View style={styles.subcontainer} >
         <Text style={{ fontSize: 11,textAlign: 'left', flex:1, alignSelf: 'flex-start',paddingBottom: 10}}>{item.date}</Text>
         <Text style={{ fontSize: 16,color:'black'}}>
         {item.short_desc}
         </Text>

         <Image style={styles.imageView} source={require('./img/static/ads1.jpg')}   />

         <Text style={{ fontSize: 16,color:'black'}}>
         {item.long_desc}
         </Text>

        </View>
         
        </ScrollView>
                <View style={{flexDirection:'column',alignItems:'flex-end',borderTopWidth:1,borderTopColor:'#d9e1e3',backgroundColor:'#ecf0f1',paddingHorizontal:15}}>
            
                <View style={{height:60,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
    
                <TextInput 
                   onPress={()=>this.navCommentPage(item)}    
                   onBlur={() => this.onBlur()}
                   onFocus={() => this.onFocus()}
                   style={{height:40,flex:7,borderWidth:1,borderColor:'#d9e1e3',borderRadius:30,backgroundColor:'white',padding:5}} placeholder={' Add a comment'}>
                </TextInput>
                  
                  {this.state.comment ?
                  
                  <TouchableOpacity onPress={() => this.comment(item)} style={{flex:1,flexDirection:'row',justifyContent:'flex-end',alignItems:'center',paddingVertical:5,paddingHorizontal:5}}>
                  <Text style={{transform: [{ rotate: '90deg' }] }}><Icon name={'md-navigate'} size={30} color={'#2980b9'} /></Text>
                  </TouchableOpacity>:

                  <View style={{flex:5,flexDirection:'row',justifyContent:'space-around'}}>
                  <TouchableOpacity onPress={()=>this.navCommentPage(item)} style={{paddingVertical:5,}}>
                  <Icon name={'md-chatboxes'} size={21} color={'#2980b9'} />
                  <Text style={{fontSize:12,textAlign:'center'}}>{item.comment_count}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={()=>this.toggleLike(item)} style={{paddingVertical:5,}}>
                  <Icon name={item.like === 1 ? 'md-thumbs-up' : 'md-thumbs-down'} size={21} color={'#c0392b'} />
                  <Text style={{fontSize:12,textAlign:'center'}}>{item.like_count}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={()=>this.whatsappLink(item)} style={{paddingVertical:5,}}>
                  <Icon name={'logo-whatsapp'} size={21} color={'#25d366'} />
                  <Text style={{fontSize:12,textAlign:'center'}}>{item.share_count}</Text>
                  </TouchableOpacity> 
                  </View>
                  }
    
                </View>
    
              </View>
          
          
          
          </View>
                
          }
          />

          </ScrollView>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems:'center'
  },
  subcontainer:{
    paddingHorizontal:15
  },
  imageView:{
    height: 200,
    width: '100%',
    marginTop: 15,
    marginBottom: 15,
  },
  textView: {
    fontSize: 19,
    color: 'black',
    fontWeight: 'bold'
  },
  contentContainer: {
    paddingVertical: 20
  },
  textBold:{
    fontWeight: 'bold'
  }
});
export default Details;


/**
 *  <SafeAreaView style={{flex:1}}>

          <View style={{height:250,bottom:10,flexDirection:'row',alignItems:'center',borderTopWidth:1,borderTopColor:'#d9e1e3',backgroundColor:'#ecf0f1',paddingHorizontal:15,}}>

                <TextInput style={{borderWidth:1,borderColor:'#d9e1e3',borderRadius:30,backgroundColor:'white',width:"58%",padding:5}} placeholder={' Add a comment'}></TextInput>
               
                <TouchableOpacity style={{paddingVertical:5,paddingHorizontal:15}}>
                <Icon name={'md-chatboxes'} size={21} color={'#2980b9'} />
                <Text style={{fontSize:12,textAlign:'center'}}>1</Text>
                </TouchableOpacity>

                <TouchableOpacity style={{paddingVertical:5,paddingHorizontal:15}}>
                <Icon name={'md-thumbs-up'} size={21} color={'#c0392b'} />
                <Text style={{fontSize:12,textAlign:'center'}}>1</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={this.link} style={{paddingVertical:5,paddingHorizontal:15}}>
                <Icon name={'logo-whatsapp'} size={21} color={'#25d366'} />
                <Text style={{fontSize:12,textAlign:'center'}}>1</Text>
                </TouchableOpacity>

          </View>
          <View style={{ height: 70 }} />
          </SafeAreaView>
 */




 /***
  * Array 1 = 10 items.
  * Array 2 = 3 items.
  * 
  * var array1 = ['a', 'b', 'c' , 'd', 'e', 'f'];
    var array2 = ['a', 'e', 'c'];

    array2.forEach(function(x) {
                            
      var found = array1.find(function(element) {
        return element === x;
      });

      console.log(found);
    });
  * 
  * 
  * 
  * 
  */