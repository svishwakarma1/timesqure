import React from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground
} from 'react-native';


export default function ChannelHeader({
  onPressFollow,
  onPressChannel,
  imgBgSrc,
  imgSrc, 
  imgBgStyle = styles.imgBg,
  imgStyle = styles.img,
}) {

  return (
    // <TouchableOpacity style={buttonStyle} onPress={onPress}>
    //   <Text style={textColor}>{label.toUpperCase()}</Text>
    // </TouchableOpacity>

   <View>
    <ImageBackground source={imgBgSrc} style={imgBgStyle}>
    <Image source={imgSrc} style={imgStyle}/>
    </ImageBackground>
    <View style={{flexDirection:"row",margin:15 }}>
       <Text onPress={onPressChannel} style={{ fontSize: 18,color: 'black',textAlign: 'left', flex:1, alignSelf: 'flex-start',marginTop: 5}}>Times of India {"\n"}<Text style={{fontSize: 12, color: 'grey'}}> 816k Followers | 4.5k Stories  </Text></Text>

       <TouchableOpacity onPress={onPressFollow} style={{padding:8}}>
       <Text style={{fontSize: 14,borderWidth:1,paddingVertical:2,paddingHorizontal:5,borderColor: '#1DA1F2',borderRadius: 5 ,color: '#1DA1F2'}}>
       <Icon  name={'ios-star-outline'} size={14} color={'#1DA1F2'} />
       {' '}Follow</Text>
       </TouchableOpacity>
     </View>
   </View>
  );
}

ChannelHeader.propTypes = {
    onPressChannel: React.PropTypes.func.isRequired,
    onPressFollow: React.PropTypes.func.isRequired,
    imgBgSrc: React.PropTypes.string,
    imgBgStyle: React.PropTypes.any,
    imgSrc: React.PropTypes.string,
    imgStyle: React.PropTypes.any,
};

// Button.propTypes = {
//   onPress: React.PropTypes.func.isRequired,
//   label: React.PropTypes.string,
//   buttonStyle: React.PropTypes.any,
//   textColor: React.PropTypes.any,
// };


 const styles = StyleSheet.create({
    imgBg:{
     height: 180
    },
    img:{
     height: 70, 
     width: 70,
     marginVertical: 50, 
     alignSelf:'center'
    }
//   button: {
//     width: 150,
//     height: 75,
//     backgroundColor: 'ivory',
//     borderRadius: 5,
//     justifyContent: 'center',
//     alignItems: 'center',
//     marginVertical: 15,
//   },
//   text: {
//     color: 'red',
//     fontSize: 16,
//   },
 });