import React, { Component } from 'react';
import {
  View,
  Text,AsyncStorage,Image,ImageBackground
} from 'react-native';

 class Splash extends React.Component {
  static navigationOptions =
  {
     drawerLabel: null,
     header : null,
  };
  
  constructor(props){
     super(props);
     //const userToken = AsyncStorage.getItem('userToken');
     this.gotoNextActivity();
   }

   

   
  // Fetch the token from storage then navigate to our appropriate place
  gotoNextActivity = async () =>
  {
    const userToken = await AsyncStorage.getItem('userToken');

    //console.warn(userToken);s

     //this.props.navigation.navigate('StackViaTab');
     //this.props.navigation.navigate('Category');

    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    //this.props.navigation.navigate(userToken ? 'MyDrawerNavigator' : 'Details');
    this.props.navigation.navigate('Home');
  }

  // componentWillMount(){
  //   setTimeout(()=> {
  //     this.props.navigation.navigate('App')
  //   },1000);
  // }
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
       <ImageBackground style={{width:'100%',height:'100%',justifyContent: 'center', alignItems: 'center'}} source={require('./img/splash_bg.jpg')}>
        <Image style={{width:'80%'}} source={require('./img/icon.jpg')}/>
       </ImageBackground> 
      </View>
    );
  }
}


export default Splash;